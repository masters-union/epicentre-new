export const environment = {
  production: true,
  // baseURL: "https://api-fms-be.mastersunion.in/api/",
  // orgURL: "https://api-fms-be.mastersunion.in/org/"
  // baseURL: "https://test-fms.herokuapp.com/api/",
  // orgURL: "https://test-fms.herokuapp.com/org/"

  //  baseURL: "https://epicapi.mastersunion.org/api/",
  //  orgURL: "https://epicapi.mastersunion.org/api/org",
  // without ssl
  baseURL: "https://epicapi.mastersunion.org/api/org",
  orgId: '50f2f27b-3cf1-4802-aef3-d422eee85be7',
  // baseURL: "https://test-epicapi.herokuapp.com/api/",
  // orgURL: "https://test-epicapi.herokuapp.com/api/org",
  // lpuUrl: "http://lpu-world-record.mastersunion.org/", 
};
