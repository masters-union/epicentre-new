import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, Form } from '@angular/forms';
import { CookieServiceProvider } from '../../common/service/cookie.service';
import { StudentService } from '../service/student.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { DomSanitizer } from "@angular/platform-browser";
// import { AuthService } from 'angularx-social-login';
import { client } from 'src/app/common/service/config';
import * as moment from 'moment';
declare var $: any;

@Component({
  selector: 'app-student-resume',
  templateUrl: './student-resume.component.html',
  styleUrls: ['./student-resume.component.css']
})
export class StudentResumeComponent implements OnInit {
  loading: boolean = false;
  saveloading: boolean = false;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  student_id: String = "";
  student_token: String = '';
  student_email: String = '';
  organizationId: any;
  profileData: any;
  lastName: any;
  firstName: any;
  studentProfileId: any;
  profilPicUrl: any = "";
  resumeData: any;
  skillsForm: FormGroup;
  campusOrgForm: FormGroup;
  achievementsForm: FormGroup;
  aboutForm: FormGroup;
  projectsForm: FormGroup;
  educationForm: FormGroup;
  slug: any = "";
  workExperienceForm: FormGroup;
  showOverview: boolean = true;
  showSkill: boolean = true;
  showEducation: boolean = true;
  showAchievement: boolean = true;
  showWorkExperience: boolean = true;
  showProject: boolean = true;
  showCampusOrg: boolean = true;
  stuDetails: any;
  formD: FormData;
  fileName: string;
  resumeUrl: any;
  resumeFile : String;
  changePasswordForm: FormGroup;
  oldPasswordWrong: boolean;
  confirmPasswordError: boolean;
  submited: boolean;
  oldpasswordError: boolean;
  passwordError: boolean;
  passwordMatch: boolean;
  studentName: string;
  skillsArray: any = [];
  campusOrgArray: any = [];

  constructor( private _ss: StudentService, private cs: CookieServiceProvider, private fb: FormBuilder, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router,  private sanitizer:DomSanitizer) { }

  ngOnInit() {
    // window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.student_token = this.cs.getItem('token');
    this.organizationId = this.cs.getItem('orgId');
    // this.route.params.subscribe((params: Params) => {
    //   // this.firstName = params['fname'];
    //   // this.lastName = params['lname'];
    //   this.slug = params['slug'];
    // })
    // this.slug = this.firstName.toLowerCase() + "-" + this.lastName.toLowerCase()
    
    // this.resumeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://epicentre.s3.ap-south-1.amazonaws.com/assets/'+this.slug+'.pdf#toolbar=0')
    this.getStudentDetails();
    // this.getStudentResumeData();
    this.initSkills();
    this.initAchievements();
    this.initOverview();
    this.initProjects();
    this.initEducation();
    this.initWorkExperience();
    this.initCampusOrgComingFrom();
    this.submited = false;
    this.passwordMatch = true
    this.initForm();
    
  }
  addSkilltag(){
    
    $('.for-skill.for-selectized .selectize-control .selectize-input input').attr('placeholder', 'Add Skills');
  }
  addBiotag(){
    
    $('.for-bio.for-selectized .selectize-control .selectize-input input').attr('placeholder', 'Add Bio');
  }
  initOverview() {
    this.aboutForm = this.fb.group({
      description : ['', Validators.required]
    })
  }

  initSkills () {
    this.skillsForm = this.fb.group({
      skills : this.fb.array([this.fb.control(null, Validators.required)])
    })
    
  }

  initAchievements () {
    this.achievementsForm = this.fb.group({
      achievements : this.fb.array([this.fb.control(null, Validators.required)])
    })
    
  }

  initProjects() {
    this.projectsForm = this.fb.group({
      projects: this.fb.array([this.fb.group({
            title: ['', Validators.required],
            description: ['', Validators.required]
          })
        ])
    })
  }

  initEducation() {
    this.educationForm = this.fb.group({
        education: this.fb.array([this.fb.group({
          duration: ['', Validators.required],
          courseName: ['', Validators.required],
          organization: ['', Validators.required]
        })
      ])
    })
  }

  initWorkExperience() {
    this.workExperienceForm = this.fb.group({
      workExperience: this.fb.array([this.fb.group({
        duration: ['', Validators.required],
        organizationName: ['', Validators.required],
        profile: ['', Validators.required],
        responsibilities : this.fb.array([this.fb.control(null, Validators.required)]),
        designation: this.fb.array([this.fb.group({
            responsibilities : this.fb.array([this.fb.control(null, Validators.required)]),
            title: ['', Validators.required],
          })
        ])
      })
    ])
    })
  }

  initCampusOrgComingFrom () {
    this.campusOrgForm = this.fb.group({
      campusOrgComingFrom : this.fb.array([this.fb.control(null, Validators.required)])
    })
    
  }

 getStudentDetails(){
  this.loading= true;
  this._ss.getStudentProfileDetails(this.student_id).subscribe((data: any) => {
    if (data.IsSuccess) {
     this.stuDetails = data.Data
     this.student_email = data.Data.officialEmail
     this.studentName = data.Data.firstName + " " + data.Data.lastName
        if(data.Data.slug) {
          this.slug = data.Data.slug
          this.getStudentResumeData()
        }
        else{
          this.firstName = data.Data.firstName
          this.lastName = data.Data.lastName
          let slugcheck1 = this.firstName.toLowerCase() + "-" + this.lastName.toLowerCase()
          this.slug = slugcheck1
          this.getStudentResumeData()
        }
    }
    else{
      this.loading= false;
    }
    
  })
 }

  getStudentResumeData() {
    this.loading= true;
    //using masterUnionapi
    this._ss.getStudentProfileMasterUnion(this.student_id, this.slug).subscribe((data:any) => {
      this.loading = false
      if(data.IsSuccess){
        this.profileData = data.Data;
        // this.slugCheck2 = data.Data.slug;
        this.skillsArray = this.profileData.skills || []
        this.campusOrgArray = this.profileData.campusOrgComingFrom || []
        this.aboutForm.controls.description.patchValue(this.profileData.description)
        this.skillsForm.setControl('skills',this.fb.array(this.setSKills(this.profileData.skills) || []))
        this.achievementsForm.setControl('achievements',this.fb.array(this.setAchievements(this.profileData.achievements) || []))
        this.projectsForm.setControl('projects', this.fb.array(this.setProjects(this.profileData.projects)));
        this.educationForm.setControl('education', this.fb.array(this.setEducation(this.profileData.education)));
        this.workExperienceForm.setControl('workExperience', this.fb.array(this.setWorkExperience(this.profileData.workExperience)));
        this.campusOrgForm.setControl('campusOrgComingFrom',this.fb.array(this.setCampusOrg(this.profileData.campusOrgComingFrom) || []))
        this.profilPicUrl = this.sanitizer.bypassSecurityTrustUrl(this.profileData.studentProfilePic)
        if(this.profileData.batch == "2022" || (this.profileData.resume_url != "" && this.profileData.resume_url)){
          this.resumeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.profileData.resume_url+ '#toolbar=0')
        }
        else {
          this.resumeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://mu-student-resume.s3.ap-south-1.amazonaws.com/'+this.slug+'.pdf#toolbar=0')
        }
      }
      else {
        this.profileData = {
          "description": "",
          "skills":[],
          "projects": [],
          "education": [],
          "workExperience": [],
          "achievements": [],
          "campusOrgComingFrom": [],
          "slug" : this.slug,
          "isDeleted" : true,
          "studentName" : this.studentName
        }
        this.aboutForm.controls.description.patchValue("")
        this.skillsForm.setControl('skills',this.fb.array([]))
        this.campusOrgForm.setControl('campusOrgComingFrom',this.fb.array([]))
        this.achievementsForm.setControl('achievements',this.fb.array([]))
        this.projectsForm.setControl('projects', this.fb.array([]));
        this.educationForm.setControl('education', this.fb.array([]));
        this.workExperienceForm.setControl('workExperience', this.fb.array([]));
      }
    })
  }

  goToTop() {
    // window.scrollTo({top: 100, behavior: "smooth"})
  }

  aboutUpdate() {
    this.showOverview = false
  }
  skillUpdate() {
    this.showSkill = false
    this.selectizeOnSkill()
  }
  eduUpdate() {
    this.showEducation = false
  }
  achievementUpdate() {
    this.showAchievement = false
  }
  workexperienceUpdate() {
    this.showWorkExperience = false
  }
  projectUpdate() {
    this.showProject = false
  }
  campusOrgUpdate() {
    this.showCampusOrg = false
    this.selectizeOnCampusOrg()
  }

  addSkill(){
   (<FormArray>this.skillsForm.get('skills')).push(new FormControl(null, Validators.required))
  }

  addAchievement(){
    (<FormArray>this.achievementsForm.get('achievements')).push(new FormControl(null,Validators.required))
   }

   addProject(){
    (<FormArray>this.projectsForm.get('projects')).push(this.fb.group({
      title: ['', Validators.required],
      description: ['', Validators.required]
    })
    )
   }

   addEducation(){
    (<FormArray>this.educationForm.get('education')).push(this.fb.group({
      duration: ['', Validators.required],
      courseName: ['', Validators.required],
      organization: ['', Validators.required]
    })
    )
   }

   addCampusOrg(){
    (<FormArray>this.campusOrgForm.get('campusOrgComingFrom')).push(new FormControl(null,Validators.required))
   }

   addWorkExperience(){
    (<FormArray>this.workExperienceForm.get('workExperience')).push(this.fb.group({
            duration: ['', Validators.required],
            organizationName: ['', Validators.required],
            profile: ['', Validators.required],
            responsibilities : this.fb.array([this.fb.control(null, Validators.required)]),
            designation: this.fb.array([this.fb.group({
                responsibilities : this.fb.array([this.fb.control(null, Validators.required)]),
                title: ['', Validators.required],
              })
            ])
      })
    )
   }

   addResponsibilities(i){
    (<FormArray>(<FormArray>this.workExperienceForm.get('workExperience')).at(i).get('responsibilities')).push(this.fb.control(null, Validators.required))
   }

   addDesignation(i){
    (<FormArray>(<FormArray>this.workExperienceForm.get('workExperience')).at(i).get('designation')).push(this.fb.group({
      responsibilities : this.fb.array([this.fb.control(null, Validators.required)]),
      title: ['', Validators.required]
    }))
   }

   addDesignationResp(i,j){
    (<FormArray>(<FormArray>(<FormArray>(<FormArray>this.workExperienceForm.get('workExperience')).at(i).get('designation')).at(j)).get('responsibilities')).push(this.fb.control(null, Validators.required))
   }

  setSKills(arr: any): Array<any>{
    const array = []
    // let c = this.fb.control(null, Validators.required)
    arr.forEach(element => {
      let a = this.fb.control(element)
      array.push(a);
    });
    return array
  }
  

  setCampusOrg(arr: any): Array<any>{
    const array = []
    // let c = this.fb.control(null, Validators.required)
    arr.forEach(element => {
      let a = this.fb.control(element)
      array.push(a);
    });
    return array
  }

  setAchievements(arr: any): Array<any>{
    const array = []
    // let c = this.fb.control(null, Validators.required)
    arr.forEach(element => {
      let a = this.fb.control(element)
      array.push(a);
    });
    return array
  }

  setProjects(arr: any): Array<any>{
    const array = []
    // let c = this.fb.control(null, Validators.required)
    arr.forEach(element => {
      let a = this.fb.group({
        title: element.title,
        description: element.description
      })
      array.push(a);
      
    });
    return array
  }

  setEducation(arr: any): Array<any>{
    const array = []
    // let c = this.fb.control(null, Validators.required)
    arr.forEach(element => {
      let a = this.fb.group({
        duration: element.duration,
        organization: element.organization,
        courseName: element.courseName
      })
      array.push(a);
      
    });
    return array
  }

  setWorkExperience(arr: any): Array<any>{
    const array = []
    // let c = this.fb.control(null, Validators.required)
    arr.forEach(element => {
      let a = this.fb.group({
        duration: element.duration,
        organizationName: element.organizationName,
        profile: element.profile,
        responsibilities: this.fb.array(this.addresp(element.responsibilities)),
        designation: this.fb.array(this.desg(element.designation))
      })
      array.push(a);
      
    });
    return array
  }

  addresp(element){
    let a = []
    element.forEach(resp => {   
      let b = this.fb.control(resp)
      a.push(b)
    })
    return a
  }

  desg(element){
    let arr = []
    element.forEach(design => {
      let c = this.fb.group({
        title: design.title,
        responsibilities: this.fb.array(this.addrespDesignation(design.responsibilities))
      })
      arr.push(c)
    })
    return arr
  }

  addrespDesignation(e){
    let a = []
    e.forEach(resp => {   
      let b = this.fb.control(resp)
      a.push(b)
    })
    return a
  }
  
  removeSkillItem(i) {
    (<FormArray>this.skillsForm.get('skills')).removeAt(i);
  }
  removeAchievementItem(i) {
    (<FormArray>this.achievementsForm.get('achievements')).removeAt(i);
  }

  removeCampusOrgItem(i) {
    (<FormArray>this.campusOrgForm.get('campusOrgComingFrom')).removeAt(i);
  }

  removeProjectItem(i) {
    (<FormArray>this.projectsForm.get('projects')).removeAt(i);
  }

  removeEducationItem(i) {
    (<FormArray>this.educationForm.get('education')).removeAt(i);
  }

  removeWorkExperienceItem(i) {
    (<FormArray>this.workExperienceForm.get('workExperience')).removeAt(i);
  }

  removeResponsibilities(i, j) {
    (<FormArray>(<FormArray>this.workExperienceForm.get('workExperience')).at(i).get('responsibilities')).removeAt(j)
  }

  removeDesignation(i, j) {
    (<FormArray>(<FormArray>this.workExperienceForm.get('workExperience')).at(i).get('designation')).removeAt(j)
  }

  removeDesignationResponsibilities(i, j, k) {
    (<FormArray>(<FormArray>(<FormArray>(<FormArray>this.workExperienceForm.get('workExperience')).at(i).get('designation'))).at(j).get('responsibilities')).removeAt(k)
  }

  selectizeOnSkill(){
        const items = this.skillsArray.map(item => ({item}));
        setTimeout(() => {
          $('#skillSelectize').selectize({
            plugins: ['remove_button'],
            delimiter: ',',
            persist: false,
            create: function(input) {
                return {
                    value: input,
                    text: input
                }
            }
        });
        this.setvalueofskillselectize()
        this.addSkilltag()
        },10)
    
  }


  setvalueofskillselectize(){
    let s = $("#skillSelectize").selectize();
    s[0].selectize.items = this.skillsArray;
  }

   updateSkills(val) {
    
    let obj = this.profileData
    // obj.skills = val.skills
    obj.skills = val
    if (this.skillsForm.valid) {
      this.loading = true;
      this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
        if(data.IsSuccess){
          this.loading = false;
          this.ts.pop("success", "", "Data Successfully Updated");
          this.getStudentResumeData()
          this.showSkill = true
        }
        else {
          this.loading = false;
          this.getStudentResumeData()
          this.showSkill = true
          // this.ts.pop("error", "", "Data not updated, please try again");
        }
      })
    }
    
    else {
      this.loading = false;
      this.ts.pop("error", "", "Empty field cannot be submitted");
    }
    this.goToTop()
   
   }


   updateSkillUsingSelectize() {
    this.addSkilltag()
    let selectS = $("#skillSelectize").selectize();
    let skills = selectS[0].selectize.items
    let isString = true;
    skills.forEach(element => {
      if(!isNaN(element)){
        isString = false
      }
    });
    if(isString){
      this.updateSkills(skills)
    }
    else {
      this.ts.pop("error","", "Please enter alphanumeric values only")
    }
   
   }

   updateAchievements(val) {
    
    let isString = true;
    val.achievements.forEach(element => {
      if(!isNaN(element)){
        isString = false
      }
    });
    if(isString){
      let obj = this.profileData
      obj.achievements = val.achievements
      if (this.achievementsForm.valid) {
        this.loading = true;
        this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
          if(data.IsSuccess){
            this.loading = false;
            this.ts.pop("success", "", "Data Successfully Updated");
            this.showAchievement = true
          }
          else {
            this.loading = false;
            // this.ts.pop("error", "", "Data not updated, please try again");
            this.showAchievement = true
          }
    
        })
      }
      else {
        this.loading = false;
        this.ts.pop("error", "", "Empty field cannot be submitted");
      }
      this.goToTop()
    }
    else {
      this.ts.pop("error","", "Please enter alphanumeric values only")
    }
    
   }

   selectizeOnCampusOrg(){
    setTimeout(() => {
      $('#campusOrgSelectize').selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });
    this.setvalueofcampusorgselectize()
    this.addBiotag()
    },10)
  }


  setvalueofcampusorgselectize(){
    let s = $("#campusOrgSelectize").selectize();
    s[0].selectize.items = this.campusOrgArray;
  }


   updateCampusOrg(val) {
    
    let obj = this.profileData
    // obj.campusOrgComingFrom = val.campusOrgComingFrom
    obj.campusOrgComingFrom = val
    if (this.campusOrgForm.valid) {
      this.loading = true;
      this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
        if(data.IsSuccess){
          this.loading = false;
          this.getStudentResumeData()
          this.ts.pop("success", "", "Data Successfully Updated");
          this.showCampusOrg = true
        }
        else {
          this.getStudentResumeData()
          this.loading = false;
          // this.ts.pop("error", "", "Data not updated, please try again");
          this.showCampusOrg = true
        }
  
      })
    }
    else {
      this.loading = false;
      this.ts.pop("error", "", "Empty field cannot be submitted");
    }
    this.goToTop()
    
   }
   updateCampusOrgUsingSelectize() {
     this.addBiotag()
    let selectS = $("#campusOrgSelectize").selectize();
    let campusoOrg = selectS[0].selectize.items
    let isString = true;
    campusoOrg.forEach(element => {
      if(!isNaN(element)){
        isString = false
      }
    });
    if(isString){
      this.updateCampusOrg(campusoOrg)
    }
    else {
      this.ts.pop("error","", "Please enter alphanumeric values only")
    }
   
   }

   updateOverview(val) {
    
    let obj = this.profileData
    obj.description = val.description
    if (this.aboutForm.valid) {
      this.loading = true;
      this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
        if(data.IsSuccess){
          this.loading = false;
          this.ts.pop("success", "", "Data Successfully Updated");
          this.showOverview = true
        }
        else {
          this.loading = false;
          // this.ts.pop("error", "", "Data not updated, please try again");
          this.showOverview = true
        }
      })
    }
    else {
      this.loading = false;
      this.ts.pop("error", "", "Empty field cannot be submitted");
    }
    this.goToTop()
    
   }

   updateProjects(val) {
    let obj = this.profileData
    obj.projects = val.projects
    if (this.projectsForm.valid) {
      this.loading = true;
      this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
        if(data.IsSuccess){
          this.loading = false;
          this.ts.pop("success", "", "Data Successfully Updated");
          this.showProject = true
        }
        else {
          this.loading = false;
          // this.ts.pop("error", "", "Data not updated, please try again");
          this.showProject = true
        }
      })
    }
    else {
      this.loading = false;
      this.ts.pop("error", "", "Empty field(s) cannot be submitted");
    }
    this.goToTop()
    
   }

   updateEducation(val) {
    
    let obj = this.profileData
    obj.education = val.education
    if (this.educationForm.valid) {
      this.loading = true;
      this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
        if(data.IsSuccess){
          this.loading = false;
          this.ts.pop("success", "", "Data Successfully Updated");
          this.showEducation = true;
        }
        else {
          this.loading = false;
          // this.ts.pop("error", "", "Data not updated, please try again");
          this.showEducation = true
        }
      })
    }
    else {
      this.loading = false;
      this.ts.pop("error", "", "Empty field(s) can not be updated");
    }
    this.goToTop()
    
   }

   updateWorkExperience(val) {
    
    let obj = this.profileData
    obj.workExperience = val.workExperience
    if (this.workExperienceForm.valid) {
      this.loading = true;
      this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
        if(data.IsSuccess){
          this.loading = false;
          this.ts.pop("success", "", "Data Successfully Updated");
          this.showWorkExperience = true;
        }
        else {
          this.loading = false;
          // this.ts.pop("error", "", "Data not updated, please try again");
          this.showWorkExperience = true
        }
      })
    }
    else {
      this.loading = false;
      this.ts.pop("error", "", "Empty field(s) cannot be submitted");
    }
    this.goToTop()
    
   }

   cancelClick(){
    this.getStudentResumeData()
    this.showOverview = true;
    this.showEducation = true;
    this.showWorkExperience = true;
    this.showAchievement = true;
    this.showSkill = true;
    this.showProject = true;
    // this.showCampusOrg = true;
    this.goToTop()
  }

  cancelBioClick(){
    this.getStudentResumeData()
    this.showCampusOrg = true;
    this.goToTop()
  }


  uploadResume(event, firstName, lastName) {
    // this.loading = true;
    // if (event.target.files) {
    //     this.formD = new FormData();
    //     const file: File = event.target.files[0];
    //     this.fileName = file.name
    //     this.formD.append('file', file, file.name)
    //     console.error("this.formD resume ",this.formD)
    //     let data = {
    //       file: this.formD,
    //       slug: firstName.toLowerCase() + "-" + lastName.toLowerCase()
    //     }
    //     this._ss.uploadResume(this.organizationId, this.student_id, this.formD).subscribe((res: any) => {
    //       // console.log("resume upload res....", res)
    //       if (res.IsSuccess) {
    //         this.resumeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(res.Data)
    //         this.loading = false
    //         this.ts.pop("success", "", "Uploaded Successfully");
    //       } else {
    //         // this.loading = false
    //         this.ts.pop("error", "", res.Message)
    //       }
    //     })
    // }

    let temp = "";
    let slug = firstName.toLowerCase() + "-" + lastName.toLowerCase()
    const options = {
      accept: [".pdf"],
      onFileSelected(file) {
        temp = file.filename;
        this.resumeFile = temp;
        let min = file.mimetype.split('application/')[1];
        let exten = file.filename.split('.').pop();
        return { ...file, name: slug + "." + min }
      },
      storeTo: {
        location: "s3",
        path: "/assets/",
        region: "ap-south-1",
        access: "public"
      },
      onFileUploadFinished: file => {
        this.fileName = file.url;
        this.resumeUrl = this.fileName;
        let obj = this.profileData
        obj.resume_url = this.fileName
        this.loading = true;
        this._ss.editStudentProfileData(this.student_id, this.slug, obj).subscribe((data:any) => {
          if(data.IsSuccess){
            this.loading = false;
            this.ts.pop("success", "", "Data Successfully Updated");
            this.getStudentResumeData()
          }
          else {
            this.loading = false;
            this.getStudentResumeData()
            // this.ts.pop("error", "", "Data not updated, please try again");
          }
        })
      }
    };
    client.picker(options).open();

    
  }
 


  initForm() {
    this.changePasswordForm = this.fb.group({
      password: ['', Validators.required],
      confirmPassword: [null, Validators.required],
      oldpassword: ['', Validators.required]
    },
      { validators: this.confirmPasswordF('password', 'confirmPassword') })
  }
  confirmPasswordF(password: string, confirmPassword: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let pass = group.controls[password];
      let cnfpass = group.controls[confirmPassword];
      if (pass.value !== cnfpass.value) {
        this.passwordMatch = false
        return {
          confirmPasswordF: true
        };
      }
      this.passwordMatch = true
      return null;
    }
  }

  submitPassword() {
    this.submited = true;
    this.oldpasswordError = this.changePasswordForm.controls.oldpassword.invalid;
    this.passwordError = this.changePasswordForm.controls.password.invalid;
    this.confirmPasswordError = this.changePasswordForm.controls.confirmPassword.invalid;
    this.oldPasswordWrong = false;
    let data = {
      id: this.student_id,
      password: this.changePasswordForm.value.password,
      email: this.student_email,
      oldpass: this.changePasswordForm.value.oldpassword
    }

    if (!this.passwordError && this.passwordMatch && !this.oldpasswordError) {
      this.saveloading = true;
      // console.log(data);

      this._ss.changePassword(data).subscribe((data: any) => {
        if (data.IsSuccess) {
          document.getElementById('closeModel').click();
          this.saveloading = false;
          this.ts.pop("success", "", "Password changed successfully, Redirecting to login page..");
          setTimeout(() => {
            this.changePasswordForm.reset();
            // this.authService.signOut().then().catch(err => { });
            this.cs.logout()
          }, 3000);
        }
        else {
          this.saveloading = false;
          this.oldPasswordWrong = true;
          if (data.Message) {
            this.ts.pop("error", "", data.Message)
          }
          else {
            this.ts.pop("error", "", "Something went wrong! Please try again")
          }
          this.changePasswordForm.reset();
        }

      }, err => {
        // console.log(err)
        this.saveloading = false;
        this.ts.pop("error", "", "Something went wrong! Please try again")
        this.changePasswordForm.reset();
      })
    }
  }

}
