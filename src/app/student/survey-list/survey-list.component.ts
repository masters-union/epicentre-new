import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { StudentService } from '../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
declare var $: any;

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {
  studentId: string;
  organizationId: string;
  attemptedSurvey = [];
  unattemptedSurvey = [];
  anonymousSurvey = [];
  loading : boolean = false;

  constructor(
    private route: ActivatedRoute, private cs: CookieServiceProvider,
    private _ss: StudentService, private router: Router,
    private ts: ToasterService
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem('id')
    this.organizationId = this.cs.getItem('orgId')

    this.getAllSurveys()
  }


  getAllSurveys() {
    this.loading = true;
    this._ss.getAllSurveyByStuId(this.studentId).subscribe((data:any) => {
      if(data.IsSuccess){
        this.loading = false;
        let array = []
        // if()
        array = data.Data.filter(el => {
          if(el.SurveyMaster)
          return el.SurveyMaster.isPublished
        })
        array.forEach((e)=>{
          e.SurveyMaster.expiryDate = new Date(e.SurveyMaster.expiryDate)
          e.SurveyMaster.expiryDate.setHours(23,59, 0, 0)
        })
        this.attemptedSurvey = array.filter(el => el.isAttempted=="1" && el.SurveyMaster.isAnonymous==false)
        this.attemptedSurvey.sort((a,b)=>  a.SurveyMaster.expiryDate - b.SurveyMaster.expiryDate)
        this.unattemptedSurvey = array.filter(el => el.isAttempted=="0" && el.SurveyMaster.isAnonymous==false && el.SurveyMaster.expiryDate > new Date())
        this.unattemptedSurvey.sort((a,b)=>  a.SurveyMaster.expiryDate - b.SurveyMaster.expiryDate)
        // this.unattemptedSurvey = array.filter(el => el.isAttempted=="0" && el.SurveyMaster.expiryDate > new Date())
        this.anonymousSurvey = array.filter(el=> el.SurveyMaster.isAnonymous==true && el.SurveyMaster.expiryDate > new Date())
        this.anonymousSurvey.sort((a,b)=>  a.SurveyMaster.expiryDate - b.SurveyMaster.expiryDate)

      }
    })
  }

}
