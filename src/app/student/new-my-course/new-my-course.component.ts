import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { StudentService } from '../service/student.service';
declare var $:any;
@Component({
  selector: 'app-new-my-course',
  templateUrl: './new-my-course.component.html',
  styleUrls: ['./new-my-course.component.css'],
  providers: [LoadscriptsService],

})
export class NewMyCourseComponent implements OnInit {

  studentId: string;
  organizationId: string;
  loading: boolean = false;
  courseRoster = [];
  programId: string;
  isShow:boolean= false;
  studentDetails:any;
  isCrossEnable: boolean;
  searchParam: string="";
  term:string = "1";
  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.programId = this.cs.getItem("programBatchId");
    console.log("student id sis ",this.studentId);
    this.getMyCourses();
    console.log(this.isShow);
  }
  toggleAction(){
    this.isShow = !this.isShow;
    console.log(this.isShow)
  }
  onCourseClick(courseId){
    console.log("course id",courseId)
    this.router.navigate(['student/newCourseDetails/',courseId]);
  }
  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
  }
  resetsearch() {
    this.searchParam = "";
    this.getMyCourses();
    this.isCrossEnable = false;
  }

  search() {
    this.getMyCourses();
  }
  // onTermSelect() {
  //   this.term = $("#term").val();
  //   this.getMyCourses();
  // }
  getMyCourses() {
    this.loading = true;
    console.log(this.searchParam);
    this._ss.getMyCourses(this.studentId,this.programId,this.searchParam).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        // console.log(data.Data);
        this.courseRoster = data.Data;
        this.studentDetails = data.studentDetails;
        console.log(this.courseRoster);
        console.log(this.studentDetails)
        // setTimeout(() => {
				// 	this.loadscriptsService.loadStuff();
				// }, 1000);
        // console.log("check data", data.Data);
      }else{
        this.loading = false;
        console.log("Error Occured, Please Check")
      }
    });
  }

}
