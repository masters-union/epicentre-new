import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMyCourseComponent } from './new-my-course.component';

describe('NewMyCourseComponent', () => {
  let component: NewMyCourseComponent;
  let fixture: ComponentFixture<NewMyCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewMyCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewMyCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
