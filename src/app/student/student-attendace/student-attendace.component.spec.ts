import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAttendaceComponent } from './student-attendace.component';

describe('StudentAttendaceComponent', () => {
  let component: StudentAttendaceComponent;
  let fixture: ComponentFixture<StudentAttendaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentAttendaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAttendaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
