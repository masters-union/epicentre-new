import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { StudentService } from '../service/student.service';
declare var $:any;
@Component({
  selector: 'app-student-attendace',
  templateUrl: './student-attendace.component.html',
  styleUrls: ['./student-attendace.component.css'],
  providers: [LoadscriptsService],
})
export class StudentAttendaceComponent implements OnInit {

  studentId: string;
  organizationId: string;
  loading: boolean = false;
  courseRoster = [];
  programId: string;
  isShow:boolean= false;
  studentDetails:any;
  sessions=[];
  missed:any = 0;
  attended:any = 0;
  attendancePercentage:any = 0;
  searchParam: string ="";
  term: string ="1";

  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.programId = this.cs.getItem("programBatchId");
    this.getMyCourses();
  }
  // onTermSelect() {
  //   this.term = $("#term").val();
  //   this.getMyCourses();
  // }
  getMyCourses() {
    this.loading = true;
    console.log("term is ",this.term);
    this._ss.getMyCourses(this.studentId,this.programId,this.searchParam).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        this.courseRoster = data.Data
        this.getStudentCourseAttendance(this.courseRoster[0].CourseRoster.id)
      }else{
        this.loading = false;
        console.log("Error Occured, Please Check")
      }
    });
  }
  getAttendance(id){
    this.getStudentCourseAttendance(id);
  }
  getStudentCourseAttendance(courseId){
    this.loading = true;
    this._ss.getStudentCourseAttendance(this.studentId,courseId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        this.sessions = data.Data.CourseRoster.CourseSessions;
        this.attended = 0;
        this.missed = 0;
        this.sessions.forEach((ele)=>{
          if(ele.SessionAttendance!=null){
            if(ele.SessionAttendance.status == "missed"){
              this.missed += 1; 
            }else if(ele.SessionAttendance.status == "attended"){
              this.attended +=1;
            }
          }
        });
        this.attendancePercentage = ((this.attended/(this.missed + this.attended))*100).toFixed(2);
        if(isNaN(this.attendancePercentage)){
          this.attendancePercentage = 0;
        }
      }else{
        this.loading = false;
        console.log("Error Occured, Please Check")
      }
    });
  }

}
