import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeSlipComponent } from './fee-slip.component';

describe('FeeSlipComponent', () => {
  let component: FeeSlipComponent;
  let fixture: ComponentFixture<FeeSlipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeSlipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeSlipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
