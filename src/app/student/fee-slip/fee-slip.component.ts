import { Component, OnInit } from '@angular/core';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
import { StudentService } from '../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Pipe, PipeTransform } from '@angular/core';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import * as moment from 'moment';
@Pipe({ name: 'ordinal' })

@Component({
  selector: 'app-fee-slip',
  templateUrl: './fee-slip.component.html',
  styleUrls: ['./fee-slip.component.css']
})
export class FeeSlipComponent implements OnInit, PipeTransform {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  hidePdf: boolean;
  student_id: string;
  paramsId: any;
  details: any;
  slipData: any;
  CalTaxAmount = [];
  amountInRupees: string;
  InstInt: string;
  loading: boolean;
  receiptNo: any;
  defaultDate: any = moment('2020-11-28', "YYYY-MM-DD")
  actualFeeAmount: any;
  constructor(private _ss: StudentService, private cookie: CookieServiceProvider, private route: ActivatedRoute, private ts: ToasterService, private router: Router) { }

  ngOnInit() {
    window.console.log = function () { };
    window.scroll(0, 0);
    this.loading = true;
    this.route.params.subscribe((params: Params) => {
      this.paramsId = params['id'];
    })
    this.receiptNo = this.paramsId.substr(this.paramsId.length - 5);
    this.student_id = this.cookie.getItem('id');
    this.getFeeSlip();
  }

  getFeeSlip() {
    this.loading = true;
    this._ss.getFeeSlipByFeeDueId(this.student_id, this.paramsId).subscribe((data: any) => {
      if (data.IsSuccess) {
        console.log(data.Data)
        // this.loading = false
        this.slipData = data.Data
        console.log("this.slipData ", this.slipData)

        // console.log("slipData?.FeeTransaction?.paidOn",this.slipData.FeeTransaction.paidOn.getTime())
        console.log(this.details)
        if (this.slipData.FeeInstallment)
          this.InstInt = this.transform(this.slipData.FeeInstallment.installmentNumber);
        else
          this.InstInt = this.transform(this.slipData.feeDue);
        this.calculate();
      }
    })
  }
  transform(i) {
    var j = i % 10,
      k = i % 100;
    console.log("i ", i)
    console.log("j ", j)
    if (j == 1 && k != 11) {
      return i + "st";
    }
    if (j == 2 && k != 12) {
      return i + "nd";
    }
    if (j == 3 && k != 13) {
      return i + "rd";
    }
    return i + "th";
  }
  calculateTax() {
    let tempInstallment = this.slipData.FeeInstallment.amount;
    this.slipData.Fee.FeeTaxes.forEach(element => {
      let taxcal = (parseFloat(tempInstallment) * parseFloat(element.percent)) / 100;
      this.CalTaxAmount.push(taxcal);
      let totalTaxcal = parseFloat(tempInstallment) - taxcal;
      tempInstallment = totalTaxcal
    });
  }

  calculate() {
    let afterTaxFee = 0;
    let afterAllCal

    let feeAmount = this.slipData.Fee.amount
    let scholarshipAmount = this.slipData.scholarshipAmount
    let discountAmount = this.slipData.discount



    if (!this.slipData.Fee.isPayableInInstallments || this.slipData.isAllInstallmentsPaidOnce) {
      console.log("installments nhi hai ..............")
      /////calculation perform on feeAmount////////////
      let tempFeeAmount = feeAmount
      if (this.slipData.adjustemntAmount && this.slipData.adjustmentEvent) {
        if (this.slipData.adjustmentEvent == 'add') {
          tempFeeAmount = parseFloat(tempFeeAmount) + parseFloat(this.slipData.adjustemntAmount)
          this.actualFeeAmount = tempFeeAmount
          console.log("this.actualFeeAmount ", this.actualFeeAmount)
        } else {
          tempFeeAmount = parseFloat(tempFeeAmount) - parseFloat(this.slipData.adjustemntAmount)
          this.actualFeeAmount = tempFeeAmount
        }
        console.log("this.actualFeeAmount", this.actualFeeAmount)
      }
      if (this.slipData.isScholarshipGiven) {
        tempFeeAmount = parseFloat(tempFeeAmount) - parseFloat(scholarshipAmount)
        console.log("afterScholorshipAmount", tempFeeAmount);
      }
      if (this.slipData.discount) {
        tempFeeAmount = parseFloat(tempFeeAmount) - parseFloat(discountAmount)
        console.log("after discount hai", tempFeeAmount)
      }
      this.slipData.Fee.FeeTaxes.forEach(element => {
        let tax = element.percent
        console.log("tax", tax);
        let taxFee;
        taxFee = (parseFloat(tempFeeAmount) * parseFloat(tax)) / 100;
        taxFee = Math.round(taxFee)
        this.CalTaxAmount.push(taxFee);
        afterTaxFee = afterTaxFee + parseFloat(taxFee)
      });
      afterTaxFee = parseFloat(tempFeeAmount) + afterTaxFee
      console.log("afterTaxFee", afterTaxFee)
      tempFeeAmount = afterTaxFee;

      afterAllCal = afterTaxFee;
      this.downloadPdf();
    }
    else {
      console.log("installments hai ............")
      ////////////calculation perform on fee installment////////////////////
      let feeInstallment = this.slipData.FeeInstallment.amount

      let tempFeeAmount = feeInstallment
      if (this.slipData.adjustemntAmount && this.slipData.adjustmentEvent) {
        if (this.slipData.adjustmentEvent == 'add') {
          tempFeeAmount = parseFloat(tempFeeAmount) + parseFloat(this.slipData.adjustemntAmount)
          this.actualFeeAmount = tempFeeAmount
        } else {
          tempFeeAmount = parseFloat(tempFeeAmount) - parseFloat(this.slipData.adjustemntAmount)
          this.actualFeeAmount = tempFeeAmount
        }
      }
      console.log("this.actualFeeAmount", this.actualFeeAmount)
      if (this.slipData.isScholarshipGiven) {
        tempFeeAmount = parseFloat(tempFeeAmount) - parseFloat(scholarshipAmount)
        console.log("afterScholorshipAmount", tempFeeAmount);
      }


      if (this.slipData.discount) {
        tempFeeAmount = parseFloat(tempFeeAmount) - parseFloat(discountAmount)
        console.log("after discount hai", tempFeeAmount)
      }
      this.slipData.Fee.FeeTaxes.forEach(element => {
        let tax = element.percent
        console.log("tax", tax);
        let taxFee;
        taxFee = (parseFloat(tempFeeAmount) * parseFloat(tax)) / 100;
        taxFee = Math.round(taxFee)
        this.CalTaxAmount.push(taxFee);
        console.log("this.CalTaxAmount@@@@@@@@@", this.CalTaxAmount)
        afterTaxFee = afterTaxFee + parseFloat(taxFee)
      });
      afterTaxFee = parseFloat(tempFeeAmount) + afterTaxFee
      console.log("afterTaxFee", afterTaxFee)
      tempFeeAmount = afterTaxFee;

      afterAllCal = afterTaxFee;
      this.downloadPdf();
    }

    this.amountInWords();
  }
  amountInWords() {
    var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
    var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    // this.slipData.feeDue = 22181
    let num = this.slipData.feeDue
    // if (num % 1 != 0) {
    let splitNum = num.split('.')
    console.log("splitNum", splitNum)
    num = splitNum[0]
    // }
    console.log("num.toString()).length", (num = num.toString()).length)
    if ((num = num.toString()).length > 9) return 'overflow';
    var n = num.split('');
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    console.log("n number ", n);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
    // return str;
    console.log("in words ", str);
    this.amountInRupees = str;

    // this.loading = false;
  }
  downloadPdf() {
    // this.hidePdf = true;
    // this.loading =  true;
    // this.loading = true;
    setTimeout(() => {

      window.screenTop;
      var data = document.getElementById("downloadPdfId").scrollTop;
      document.getElementById("downloadPdfId").style.height = "auto";
      html2canvas(document.querySelector("#downloadPdfId"), { scrollY: -window.scrollY }).then(canvas => {
        var imgWidth = 208;
        var pageHeight = document.getElementById("downloadPdfId").offsetHeight;
        var imgHeight = pageHeight * imgWidth / canvas.width;
        var heightLeft = imgHeight;

        const contentDataURL = canvas.toDataURL('image/png')
        let pdf = new jsPDF('p', 'mm', 'a4');
        var position = 0;
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
        pdf.save('Fee receipt.pdf');
      this.loading = false;
      this.ts.pop("success", "", "pdf Successfully downloaded");
      this.router.navigate(["/student/student-portal"]);
      });

    }, 2000);
  }

  compareDates(feeSlip) {
    return moment(this.defaultDate).isBefore(moment(feeSlip));
  }
}
