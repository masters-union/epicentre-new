import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursePreferenceComponent } from './course-preference.component';

describe('CoursePreferenceComponent', () => {
  let component: CoursePreferenceComponent;
  let fixture: ComponentFixture<CoursePreferenceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursePreferenceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursePreferenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
