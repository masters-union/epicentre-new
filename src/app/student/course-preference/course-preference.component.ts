import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToasterConfig, ToasterService } from "angular2-toaster";
import * as moment from "moment";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from "../service/student.service";

@Component({
  selector: 'app-course-preference',
  templateUrl: './course-preference.component.html',
  styleUrls: ['./course-preference.component.css'],
  providers: [LoadscriptsService],
})
export class CoursePreferenceComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean;
  courseId:any
  userId:string
  organizationId:String
  termValue: number;
  numberOfPreferences:number;
  selectedPreference = [];
  minPreferenceArray = [];
  counter = Array;
  coursesArrayLength:number
  coursesData:any;
  courseList = [];
  expiryDate:string;
  minPreferences : number;
  constructor(
    private studentService: StudentService,
    private cs: CookieServiceProvider,
    private route: ActivatedRoute,
    private ts: ToasterService,
    private router: Router,
    private loadscriptsService: LoadscriptsService,
  ) {}

  ngOnInit() {
    this.organizationId = this.cs.getItem('orgId')
    this.userId = this.cs.getItem('id')
    this.courseId = this.route.snapshot.paramMap.get('id');
    this.loadscriptsService.loadStuff();
    this.getAllCourses();
  }

  getAllCourses() {
    this.loading = true;
    this.studentService.fetchCoursesList(this.courseId,this.userId).subscribe((data:any) => {
      if(data.IsSuccess){
        this.loading = false;
        this.loadscriptsService.loadStuff();
        this.coursesData=data.Data;  
        // console.log(data.Data);
        
        this.coursesArrayLength=data.Data.CoursePrefrenceRoasters.length
        this.setCourses(data.Data.CoursePrefrenceRoasters)
        let date = moment(data.Data.expiryDate);
        this.expiryDate = date.utc().format("Do MMMM (dddd)");
   
        if(JSON.stringify(data.Data.minPreference)==""|| JSON.stringify(data.Data.minPreference)=="null" || isNaN(data.Data.minPreference)){
          this.minPreferences = data.Data.numberOfPrefrences;
        }
        else{
          this.minPreferences = parseInt(data.Data.minPreference);
        }
        this.numberOfPreferences= parseInt(data.Data.numberOfPrefrences);
        console.log(this.numberOfPreferences);
        console.log(this.minPreferences);
        
        
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    })
  }
  setCourses(arr){
    for(let i=0;i<this.coursesArrayLength;i++){
      this.courseList.push(arr[i].CourseRoster);
    }
  }
  onPublish() {
    console.log(this.selectedPreference);
    if (
      new Set(this.selectedPreference.map((x) => x.course)).size !== this.selectedPreference.length){
      this.ts.pop("error", "Please select unique preferences");
    } 
    else {           
          if(this.selectedPreference.length>=this.minPreferences && this.minPreferenceArray.length==this.minPreferences){
          this.loading = true;
          this.studentService
          .submitCoursePreference(this.organizationId, this.userId,this.selectedPreference,this.courseId)
          .subscribe((data: any) => {
            if (data.IsSuccess) {
              this.loading = false;
              this.ts.pop("success","Successfully Submitted");
              setTimeout(() => {
                this.router.navigate(['/student/courseList'])
              }, 1000);
            } else {
              this.loading = false;
              this.ts.pop("error", "Something Went Wrong");
            }
          });
        }
        else{
          this.ts.pop("error","Please fill all the required fields");
        }
      }
    }

  selectOption(value, index) {
    let obj = { preference: index, course: value };
    this.checkData(obj);
  }

  mandatoryFields(value,index){
    let obj = { preference: index, course: value };
    this.mandatoryData(obj);
    this.checkData(obj);
  }

  mandatoryData(obj) {
    if (this.minPreferenceArray.length !== 0) {
      var foundId = this.minPreferenceArray.findIndex(
        (x) => x.preference == obj.preference
      );
      if (foundId == -1) {
        this.minPreferenceArray.push(obj);
      } else {
        this.minPreferenceArray[foundId] = obj;
      }
    } else {
      this.minPreferenceArray.push(obj);
    }
    // console.log("mandatory array",  this.minPreferenceArray)
  }

  checkData(obj) {
    if (this.selectedPreference.length !== 0) {
      var foundId = this.selectedPreference.findIndex(
        (x) => x.preference == obj.preference
      );
      if (foundId == -1) {
        this.selectedPreference.push(obj);
      } else {
        this.selectedPreference[foundId] = obj;
      }
    } else {
      this.selectedPreference.push(obj);
    }
    // console.log("selected array",  this.selectedPreference)

  }
}


