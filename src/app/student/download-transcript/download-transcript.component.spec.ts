import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadTranscriptComponent } from './download-transcript.component';

describe('DownloadTranscriptComponent', () => {
  let component: DownloadTranscriptComponent;
  let fixture: ComponentFixture<DownloadTranscriptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadTranscriptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadTranscriptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
