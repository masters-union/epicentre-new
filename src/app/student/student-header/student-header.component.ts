import { Component, OnInit } from '@angular/core';
// import { AuthService } from 'angularx-social-login';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';
declare var swal;
@Component({
  selector: 'app-student-header',
  templateUrl: './student-header.component.html',
  styleUrls: ['./student-header.component.css']
})
export class StudentHeaderComponent implements OnInit {
  loading: boolean;
  name: any;
  student_id: any;
  organization_id: any;
  notificationSeen = [];
  notificationNotSeen = [];
  isNewNotification: boolean = false;
  showNew: boolean = false;
  details: any;
  firstName: any;
  lastName: any;

  constructor(private cookie: CookieServiceProvider, private _ss: StudentService, private router: Router) { }

  ngOnInit() {
    // window.console.log = function () { };
    let studentName = this.cookie.getItem('email')
    this.name = studentName.charAt(0)
    this.student_id = this.cookie.getItem('id');
    this.organization_id = this.cookie.getItem('orgId');
    this.getAllNotification();
    this.getStudentDetails();
  }

  getAllNotification() {
    this.loading = true;
    this._ss.fetchAllNotification(this.student_id, this.organization_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        let d = data.Data;
        d.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
        let isSeen = d.filter(e => e.isSeen == true);
        let isNotSeen = d.filter(e => e.isSeen == false);
        if (isNotSeen.length > 0) {
          this.isNewNotification = true;
          this.showNew = true;
        }
        this.notificationSeen = isSeen
        this.notificationNotSeen = isNotSeen
      }
    })
  }

  timeDifference(previous) {
    // var current= new Date()
    var current = new Date((new Date(previous)).toUTCString())
    // var ms_Min = 60 * 1000;
    // var ms_Hour = ms_Min * 60;
    // var ms_Day = ms_Hour * 24;
    // var ms_Mon = ms_Day * 30;
    // var ms_Yr = ms_Day * 365;
    // var diff = current.valueOf() - previous1.valueOf();

    // if (diff < ms_Min) { 
    //     return Math.round(diff / 1000) + ' seconds ago'; 
    // } else if (diff < ms_Hour) { 
    //     return Math.round(diff / ms_Min) + ' minutes ago'; 
    // } else if (diff < ms_Day) { 
    //     return Math.round(diff / ms_Hour) + ' hours ago'; 
    // } else if (diff < ms_Mon) { 
    //     return Math.round(diff / ms_Day) + ' day(s) ago'; 
    // } else if (diff < ms_Yr) { 
    //     return Math.round(diff / ms_Mon) + ' month(s) ago'; 
    // } else { 
    //     return Math.round(diff / ms_Yr) + ' year(s) ago'; 
    // }
    return moment(current).fromNow();

  }

  redirectTo(uri: string) {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([uri])
    })
  }

  goto(notificationFor) {
    if (notificationFor == "Fee Due") {
      this.redirectTo('/student/student-portal');
    }
    else if (notificationFor == "Inbox")
      this.redirectTo('/student/inbox');
    else {
      this.redirectTo('/student/enquiry');
    }
  }

  clicked() {
    this.showNew = false;
    let obj = {
      isSeen: true
    }
    if (this.isNewNotification) {
      this._ss.upateSeenNotification(this.student_id, this.organization_id, obj).subscribe((data: any) => {
        if (data.IsSuccess) {
        }
      })
    }
  }

  getStudentDetails() {
    this._ss.getStudentProfileDetails(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.details = data.Data
        this.cookie.setItem(
"batch",this.details.Program.batch, 24 * 3600,
          "/",
          null,
          null
        );      }
    })
  }

  goToResumePage(){
    let slug = this.details.slug
    if(slug) {
      this.router.navigate(['/student/profile', slug]);
    }
    else{
      this.firstName = this.details.firstName
      this.lastName = this.details.lastName
      slug = this.firstName.toLowerCase() + "-" + this.lastName.toLowerCase()
      this.router.navigate(['/student/profile', slug]);
    }
    
  }

  // logOut(){
  //   this.authService.signOut().then().catch(err=>{});
  //   this.cookie.logout()
  // }
  logOut() {
    swal({
      title: "Logout",
      text: "Are you sure you want to logout?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then(result => {
      if (result) {
        // this.authService.signOut().then().catch(err => { });
        this.cookie.logout()
      }
    }, function (dismiss) {
      if (dismiss === 'cancel') { // also handle 'close' or 'timer' if we used those
        // ignore
      } else {
        // throw dismiss;
        // ignore
      }
    });
  }
}
