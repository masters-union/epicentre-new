import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCourseDetailsComponent } from './new-course-details.component';

describe('NewCourseDetailsComponent', () => {
  let component: NewCourseDetailsComponent;
  let fixture: ComponentFixture<NewCourseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCourseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCourseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
