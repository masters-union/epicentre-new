import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { StudentService } from '../service/student.service';

@Component({
  selector: 'app-new-course-details',
  templateUrl: './new-course-details.component.html',
  styleUrls: ['./new-course-details.component.css'],
  providers: [LoadscriptsService],

})
export class NewCourseDetailsComponent implements OnInit {
  studentId: string;
  organizationId: string;
  loading: boolean = false;
  sessions = [];
  programId: string;
  isShow:boolean= false;
  courseId:string;
  courseRoster:any;
  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.programId = this.cs.getItem("programBatchId");
    this.courseId = this.route.snapshot.paramMap.get('courseId');
    this.getParticularCourseDetails();
  }

  getParticularCourseDetails() {
    this.loading = true;
    this._ss.getParticularCourseDetails(this.studentId,this.courseId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        // console.log(data.Data);
        this.sessions = data.Data.CourseRoster.CourseSessions;
        this.courseRoster = data.Data.CourseRoster;
        console.log("course roster",this.courseRoster);
        console.log(this.sessions);
        // setTimeout(() => {
				// 	this.loadscriptsService.loadStuff();
				// }, 1000);
        // console.log("check data", data.Data);
      }else{
        this.loading = false;
        console.log("Error Occured, Please Check")
      }
    });
  }
}
