import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
// import { $ } from 'protractor';
declare var $: any;

import { ToasterService, ToasterConfig } from "angular2-toaster";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { FormsModule } from '@angular/forms';
@Component({
  selector: 'app-student-portal',
  templateUrl: './student-portal.component.html',
  styleUrls: ['./student-portal.component.css']
})
export class StudentPortalComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  student_id: any
  object: {}
  details: any = {}
  hostel: boolean = false
  tutionFee: any;
  foodOpted: boolean = false
  loading: boolean = true
  installmentHide: boolean = false
  hostel1: boolean = false
  ScholarShip: boolean = true
  discountPercentage: boolean = false

  selectedIndex: number = 0
  loadScript: any;
  hidePdf: boolean;
  programmeName: any
  UTR_number: any
  submitEnabled: boolean = false
  id: any
  isBankTransfer: boolean = false;
  isBankTransferClicked = false;
  feeData: any;
  stuData: any;

  constructor(private _ss: StudentService, private cookie: CookieServiceProvider
    , private http: HttpClient, private ts: ToasterService) { }
  ngOnInit() {
    window.console.log = function () { };
    this.hidePdf = false;
    // $("#hidePdf").hide()
    // this.loadScript('../../../assets/downloadRecipt/css')
    this.loading = true
    this.student_id = this.cookie.getItem('id')
    this.object = {
      student_id: this.student_id
    }
    this.getStudentDetails();
    this.javascriptCall()

  }
  javascriptCall() {

    $(document).ready(function () {
      var collaspedAreaHeight = $(".collasped-area").height();
      $(".collasped-area").css({
        height: collaspedAreaHeight + 'px'
      });
      $('.collasped-area').slideUp(600);

      $('.row-toggle-btn').click(function () {

        $(this).parents('tr').next("tr.collasped-area").slideToggle();


      });
    });
  }

  // downloadPdf() {
  //   this.hidePdf = true;
  //   this.loading =  true;
  //   setTimeout(() => {
  //     html2canvas(document.querySelector("#downloadPdfId")).then(canvas => {

  //       var pdf = new jsPDF('p', 'pt', [canvas.width, canvas.height]);

  //       var imgData = canvas.toDataURL("image/jpeg", 1.0);
  //       pdf.addImage(imgData, 0, 0, canvas.width, canvas.height);
  //       pdf.output('datauri');
  //       pdf.save('converteddoc.pdf');

  //     });
  //     this.hidePdf = false;
  //   this.loading = false;
  //   }, 2000);

  // }


  SetselectedIndex(i) {

    this.selectedIndex = i
  }
  getAmountPaid(amount) {
    var total = 0;
    total += amount;

    return total;
  }
  getStudentDetails() {
    // console.log()
    this.loading = true
    this._ss.fetchStudentFeeDetails(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false
        this.details = data.Data
        console.log("this", this.details)
        this.programmeName = this.details.Program.name
        // if(this.details.OrganizationProgram.OrganizationFees[1].OrganizationUserFeePayables.length<=0){
        // }

        this.details.Program.Fees.forEach(fees => {
          var amountPaid: number = 0;
          var totalTax: number = 0;
          var scholarship: number = 0;
          var discount: number = 0;

          fees.StudentFeeDues.forEach(feeDue => {
            if (feeDue.FeeTransaction != null)
              amountPaid = amountPaid + parseInt(feeDue.FeeTransaction.amount)
            if (feeDue.isScholarshipGiven)
              scholarship = scholarship + parseInt(feeDue.scholarshipAmount)
            if (feeDue.discount != "" || feeDue.discount != null) {
              scholarship = scholarship + parseInt(feeDue.discount)
            }

          })
          fees.FeeTaxes.forEach(feeTax => {

            totalTax = totalTax + parseInt(feeTax.percent)
            feeTax.taxAmount = (fees.amount * parseInt(feeTax.percent) / 100)


          })

          fees.amountPaid = amountPaid
          fees.dueAmount = fees.amount - scholarship
          fees.dueAmount = fees.dueAmount + (fees.dueAmount * totalTax / 100)
          fees.dueAmount = fees.dueAmount - amountPaid
        });
        // console.log("this", this.details)

      }
    })
  }
  setIndex(ind) {
    this.selectedIndex = ind
  }

  payFee() {
    let feedata = this.stuData
    this.loading = true
    console.log("fee pay id ", feedata)
    let orderId = feedata.id.substr(feedata.id.length - 6);
    let data = {
      studentEmail: this.details.officialEmail,
      studentName: this.details.firstName + ' ' + this.details.lastName,
      phone: this.details.mobileNumber,
      id: orderId,
      // studentFeeDueId: feedata.id
    }
    this._ss.onlineFeePaymentByStudent(this.student_id, feedata.id, data).subscribe((res: any) => {
      console.log("feepay data is ", res)
      this.loading = false;
      if (res.IsSuccess) {
        if(res.Data1){
          window.open(res.Data1, "_self");

        }
        if (res.Data) {
          if (res.Data.status == "OK") {
            // this.loading = false;
            console.log("inside cashFree data.......... ", res)
            let paymentLink = res.Data.paymentLink;
            // window.open(paymentLink, "_blank");
            window.open(paymentLink, "_self");
          }
          if (res.Data.status == "ERROR") {
            console.log("reason.............", res.Data.reason)
            this.ts.pop("error", "", res.Data.reason);
          }
        } else {
          this.ts.pop("error", "", "Network issue, Please try again");
        }
      } else {
        if (res.Message == "Transaction already done") {
          this.ts.pop("error", "", "Transaction already done");
        }
        else
          this.ts.pop("error", "", "try again");
      }
    })
  }
  onChange(value) {
    if (value != '') {
      this.submitEnabled = true
      this.UTR_number = value
      console.log("val", this.UTR_number)


    }
    else {
      this.submitEnabled = false
    }
  }
  pay(id, data) {
    $("#pay-now").modal('show');
    this.id = id
    this.stuData = data;
    console.log("pay now data ", this.stuData)
    this.isBankTransfer = false;
    this.isBankTransferClicked = false
    this.UTR_number = ""
  }
  submit() {
    let object = {
      UTR_number: this.UTR_number
    }
    this._ss.updateUtrNumber(this.student_id, this.id, object).subscribe((data: any) => {
      if (data.IsSuccess) {
        console.log(data.Data)
        this.ts.pop("success", "Updated Successfully")
        this.getStudentDetails()
        $("#pay-now").modal('hide');
        this.isBankTransfer = false;
        this.isBankTransferClicked = false
      }
      else {
        this.ts.pop("error", "Something Went Wrong")
      }
    })
    console.log("d", this.UTR_number)
  }
  bankTransfer() {
    this.isBankTransfer = true;
    this.isBankTransferClicked = true;
  }
}
