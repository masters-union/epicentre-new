import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CalendarOptions } from '@fullcalendar/angular';
// import dayGridPlugin from '@fullcalendar/daygrid';
// import timeGridPlugin from '@fullcalendar/timegrid';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { StudentService } from '../service/student.service';
import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-new-dashboard',
  templateUrl: './new-dashboard.component.html',
  styleUrls: ['./new-dashboard.component.css'],
  providers: [LoadscriptsService],

})
export class NewDashboardComponent implements OnInit {

  studentId: string;
  organizationId: string;
  loading: boolean = false;
  courseRoster = [];
  programId: string;
  isShow:boolean= false;
  studentDetails:any;
  Data:any;
  completedSession:number = 0;
  upcomingSession:number = 0;
  isBiddingEnabled:boolean = false;
  totalCourses:number = 0;
  allSessions = [];
  allSessions1 = [];
  calendarOptions: CalendarOptions;
  today = new Date();
  initialized = false;
  sessionName : any;
  courseName : any;
  courseCode : any;
  description: any;

  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.programId = this.cs.getItem("programBatchId");
    this.getDashboardDetails();
    this.getAllStudentCourse();
    // this.nextPrevButton();
    // this.monthDayWeekButton();
    
    
  }

  getDashboardDetails() {
    this.loading = true;
    this._ss.getDashboardDetails(this.studentId,this.programId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        console.log(data.Data);
        this.totalCourses = data.prog.length;
        let bidding = data.termDetail;
        // biddingArray.length = 3
        // biddingArray.map((ele)=>{
        //   console.log(ele.isBiddingOpen);
        //   if(ele.isBiddingOpen){
        //     console.log(ele.isBiddingOpen);
        //     this.isBiddingEnabled = true; 
        //   }
        // })
        if(bidding!=null){
          this.isBiddingEnabled = bidding.isBiddingOpen;
        }
        this.Data = data;
        this.completedSession = this.Data.completedSessions;
        this.upcomingSession = this.Data.noofSessions - this.Data.completedSessions;
        // this.courseRoster = data.Data.CourseRoster;
        // console.log("course roster",this.courseRoster);
        // setTimeout(() => {
				// 	this.loadscriptsService.loadStuff();
				// }, 1000);
        // console.log("check data", data.Data);
      }else{
        this.loading = false;
        console.log("Error Occured, Please Check")
      }
    });
  }

  getAllStudentCourse() {
    // this.loading = true;
    this._ss.getCourseByStudentId(this.organizationId, this.studentId).subscribe((res: any) => {
      if (res.IsSuccess) {
        // this.loading = false;
        let arr = res.Data;
        arr.forEach((e) => {
          // console.error(e.CourseRoster)
          if (e.CourseRoster && e.CourseRoster.CourseSessions) {
            e.CourseRoster.CourseSessions.forEach(element => {
              element.courseCode = e.CourseRoster.courseCode;
              element.courseName = e.CourseRoster.courseName;
              this.allSessions.push(element)
            });
          }
        })
        // console.error(this.allSessions)
        this.setEvents()
      }
      else {
        this.loading = false;
      }
    })
  }

  setEvents() {
    this.allSessions.forEach((o) => {
      o.title = o.name;
      o.courseName = o.courseName;
      // o.allDay = false;
      let startDate = new Date(o.date);
      let endDate = new Date(o.date);
      let timeArr1 = o.fromTime.toString().split(":");
      let timeArr2 = o.toTime.toString().split(":");
     
        
      startDate.setHours(+timeArr1[0],+timeArr1[1], 0, 0)
      endDate.setHours(+timeArr2[0],+timeArr2[1], 0, 0)
      o.start = moment(startDate).format();
      o.end = moment(endDate).format();
      // o.color = "#229E6D";

      // if(this.today > new Date(o.date)) {
      //   o.color = "lightgray";
      //   // setTimeout(() => {
      //   //   $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
      //   // }, 100);
      //   // setTimeout(() => {
      //   //   $(".fc-timegrid-event-harness").removeClass("active-bg-green");
      //   // }, 100);
      //   // setTimeout(() => {
      //   //   $(".fc-timegrid-event-harness").addClass("past-bg-pink");
      //   // }, 100);
      // } else {
      //   o.color = "#229E6D";
      //   // setTimeout(() => {
      //   //   $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
      //   // }, 100);
      //   // setTimeout(() => {
      //   //   $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
      //   // }, 100);
      //   // setTimeout(() => {
      //   //   $(".fc-timegrid-event-harness").addClass("active-bg-green");
      //   // }, 100);
        
      // }
    });

    this.allSessions1 = this.allSessions.map(({ title, date, id, start, end, courseCode, description, color, courseName}) => ({ title, date, id, start, end, courseCode, description, color, courseName}))
    this.initializeCalendar(this.allSessions1);
  }

  

  initializeCalendar(e) {
    this.calendarOptions = {
      initialView: 'timeGridWeek',
      // dateClick: this.handleDateClick.bind(this),
      events: e,
      eventClick: this.showModal.bind(this),
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      
      views: {
        timeGridDay: {
          titleFormat: { year: 'numeric', month: '2-digit', day: '2-digit' },
          // columnHeaderFormat: { year: 'numeric', month: 'long', day: '2-digit', weekday:'long' } //Optional - for column headers
        },
        timeGridWeek: {
          // titleFormat: { year: 'numeric', month: 'numeric', day: '2-digit' },
          columnHeaderFormat: { year: 'numeric', month: 'long', day: '2-digit', weekday:'long' } //Optional - for column headers
        },
      },
      
      slotMinTime: "07:00:00",
      slotMaxTime: "20:00:00",
      eventBackgroundColor: "transparent",
      eventBorderColor: "transparent",
      // defaultAllDay: false
    };

    this.initialized = true;

    setTimeout(() => {
      $("table.fc-scrollgrid").parent("div.fc-view").addClass("fc-timegrid");
      $("table.fc-scrollgrid").parent("div.fc-view").addClass("fc-timeGridWeek-view");
    }, 100);
    setTimeout(() => {
      $(".fc-dayGridMonth-button").removeClass("fc-button-active");
    }, 100);
  
    setTimeout(() => {
      $(".fc-header-toolbar.fc-toolbar .fc-toolbar-title").appendTo("fc-header-toolbar.fc-toolbar .fc-toolbar-chunk:nth-child(1) .fc-button-group");
    }, 100);
    setTimeout(() => {
      $("tr.fc-scrollgrid-section .fc-scrollgrid-section-body").addClass("hide");
    }, 100);
    setTimeout(() => {
      $("a.fc-event a.fc-event:hover").attr('cursor', 'pointer');
    }, 100);

  }

  nextPrevButton() {
    setTimeout(() => {
        $('body').on('click', 'button.fc-prev-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").removeClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("past-bg-pink");
                }, 100);
              } else {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("active-bg-green");
                }, 100);
              }
            });
        });
     
        $('body').on('click', 'button.fc-next-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
              }, 100);
              setTimeout(() => {
                $(".fc-timegrid-event-harness").removeClass("active-bg-green");
              }, 100);
              setTimeout(() => {
                $(".fc-timegrid-event-harness").addClass("past-bg-pink");
              }, 100);
              } else {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("active-bg-green");
                }, 100);
              }
            });
        });
    }, 100);
  }

  monthDayWeekButton() {
    setTimeout(() => {
        $('body').on('click', 'button.fc-dayGridDay-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").removeClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("past-bg-pink");
                }, 100);
              } else {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("active-bg-green");
                }, 100);
              }
            });
        });
     
        $('body').on('click', 'button.fc-dayGridMonth-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").removeClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("past-bg-pink");
                }, 100);
                
              } else {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("active-bg-green");
                }, 100);
              }
                
            });
        });
        $('body').on('click', 'button.fc-timeGridWeek-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
              }, 100);
              setTimeout(() => {
                $(".fc-timegrid-event-harness").removeClass("active-bg-green");
              }, 100);
              setTimeout(() => {
                $(".fc-timegrid-event-harness").addClass("past-bg-pink");
              }, 100);
              } else {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").addClass("active-bg-green");
                }, 100);
              }
                
            });
        });
    }, 100);
  }

  showModal(arg) {
    this.sessionName = arg.event.title;
    // this.sessionName = arg.event.extendedProps.name;
    console.log(arg.event.extendedProps.name)
    this.description = arg.event.extendedProps.description;
    this.courseName = arg.event.extendedProps.courseName;
    this.courseCode = arg.event.extendedProps.courseCode;
    this.courseCode = arg.event.extendedProps.courseCode;
    document.getElementById("description").innerHTML= arg.event.extendedProps.description;
    $('#exampleModal').modal('show')
  }


}
