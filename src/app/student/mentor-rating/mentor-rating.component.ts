import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
declare var $: any;
import { StarRatingComponent } from 'ng-starrating';

import { ToasterService, ToasterConfig } from "angular2-toaster";
import { StudentService } from '../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
@Component({
  selector: 'app-mentor-rating',
  templateUrl: './mentor-rating.component.html',
  styleUrls: ['./mentor-rating.component.css']
})
export class MentorRatingComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean;
  studentName: any;
  organizationId: string;
  mentorId: string;
  questionData: any;
  ratingForm: FormGroup;
  studentId: any;
  goals = [];
  subRatingArr: any = [{ goals: this.goals }];

  ratingArr: any = [{
    subRatingArray: this.subRatingArr,
    goals: this.goals
  }];
  goalIndex: number = 0;
  questionIndex: any;
  mentorName: any;
  ratingjQueryQuesInd: number;

  subRatingjQueryQuesInd: number;
  sortBy: string;
  isRatingAvaliable: boolean = false;

  // ratingArr: { comment: any, id: any }[] = [];
  constructor(private route: ActivatedRoute, private cs: CookieServiceProvider,
    private studentService: StudentService, private fb: FormBuilder, private router: Router,
    private ts: ToasterService) { }

  ngOnInit() {
    window.console.log = function () { };
    this.route.params.subscribe((params: Params) => {
      this.mentorName = params['mentorName'];
      this.mentorId = params['id'];
    })
    this.organizationId = this.cs.getItem('orgId')
    this.studentId = this.cs.getItem('id')
    // this.getRatingQuations();
    this.ratingInitForm();
    this.checkIsStudentAlreadyRateMentor()

  }
  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }, quesId, quesIndex) {
    // alert(`Old Value:${$event.oldValue}, 
    //   New Value: ${$event.newValue}, 
    //   Checked Color: ${$event.starRating.checkedcolor}, 
    //   Unchecked Color: ${$event.starRating.uncheckedcolor}`);
    console.log("New Value:", `${$event.newValue}`)
    console.log("quesId......", quesId)
    console.log("i........", quesIndex)
    this.ratingArr[quesIndex].rating = `${$event.newValue}`;
  }
  onSubRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }, subQuesId, quesIndex, subQuesIndex) {
    this.ratingArr[quesIndex].subRatingArray[subQuesIndex].rating = `${$event.newValue}`;
  }
  onRateQus(quesIndex) {
    this.ratingjQueryQuesInd = quesIndex
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function () {
      var onStar = parseInt($(this).data('value'), 10);
      // The star currently mouse on     
      // Now highlight all the stars that's not after the current hovered star 
      $(this).parent().children('li.star').each(function (e) {
        if (e < onStar) {
          $(this).addClass('hover');
        }
        else {
          $(this).removeClass('hover');
        }
      });
    }).on('mouseout', function () {
      $(this).parent().children('li.star').each(function (e) {
        $(this).removeClass('hover');
      });
    });
    /* 2. Action to perform on click */
    // let jQueryInstance = this;
    $('#stars li').on('click', (event) => {
      var $this = $(event.currentTarget);
      var onStar = parseInt($this.data('value'), 10);
      this.ratingArr[this.ratingjQueryQuesInd].rating = onStar

      // The star currently selected   
      var stars = $this.parent().children('li.star');
      for (let i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }
      for (let i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }
    });

  }
  onSubRateQus(quesIndex, subQuesIndex) {
    this.ratingjQueryQuesInd = quesIndex
    this.subRatingjQueryQuesInd = subQuesIndex;
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#subStars li').on('mouseover', function () {
      var onStar = parseInt($(this).data('value'), 10);
      // The star currently mouse on     
      // Now highlight all the stars that's not after the current hovered star 
      $(this).parent().children('li.star').each(function (e) {
        if (e < onStar) {
          $(this).addClass('hover');
        }
        else {
          $(this).removeClass('hover');
        }
      });
    }).on('mouseout', function () {
      $(this).parent().children('li.star').each(function (e) {
        $(this).removeClass('hover');
      });
    });
    /* 2. Action to perform on click */
    // let jQueryInstance = this;
    $('#subStars li').on('click', (event) => {
      var $this = $(event.currentTarget);
      var onStar = parseInt($this.data('value'), 10);
      this.ratingArr[this.ratingjQueryQuesInd].subRatingArray[this.subRatingjQueryQuesInd].rating = onStar

      // The star currently selected   
      var stars = $this.parent().children('li.star');
      for (let i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }
      for (let i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }
    });

  }
  commentSpacePrevant() {
    console.log("event ", event)
    $("textarea").on("keypress", function (e) {
      if (e.which === 32 && !this.value.length)
        e.preventDefault();
    });
  }
  ngAfterViewInit() {

  }
  ratingInitForm() {
    this.ratingForm = this.fb.group({
      ratingArray: this.fb.array([this.ratingArray]),
      // ratingMasterId: "",
      // comment: "",
      // rating: "",
      // organizationId: this.organizationId,
      // studentId: this.studentId,
      // mentorId: this.mentorId,
      // hasGoals : ""
    })
  }
  get ratingArray(): FormGroup {
    return this.fb.group({
      ratingMasterId: "",
      comment: "",
      rating: "",
      organizationId: this.organizationId,
      studentId: this.studentId,
      mentorId: this.mentorId,
      hasGoals: ""
    });
  }
  checkIsStudentAlreadyRateMentor() {
    this.loading = true;
    this.studentService.checkIsStudentAlreadyRateMentor(this.organizationId, this.studentId, this.mentorId).subscribe((res: any) => {
      this.loading = false;
      if (res.IsSuccess) {
        if (res.Data.length > 0) {
          console.log("rating avaliable hai ...... ")
          this.isRatingAvaliable = true;
        } else {
          this.getRatingQuations();
        }
      } else {
        console.log("rating avaliable nhi hai ")
        this.getRatingQuations();
      }
    })
  }
  getRatingQuations() {
    this.loading = true;
    this.studentService.fetchRatingQuestionsForStudent(this.organizationId).subscribe((res: any) => {
      // console.log("res ",res)

      if (res.IsSuccess) {

        this.loading = false;
        console.log("rating quation ", res)
        // this.questionData = res.Data

        // if(this.sortBy == "asc"){
        res.Data.sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime())
        this.questionData = res.Data
        // }
        console.log("this.questionData.....", this.questionData)
        // this.ratingArr.length = this.questionData.length;

        this.questionData.forEach((element, i) => {
          this.ratingArr[i] = {
            comment: "",
            ratingMasterId: this.questionData[i].id,
            rating: "",
            organizationId: this.organizationId,
            studentId: this.studentId,
            mentorId: this.mentorId,
            hasGoals: false,
            subRatingQuesId: "",
            goals: []
          }
          if (element.SubRatings.length > 0) {
            element.SubRatings.forEach((subElement, j) => {
              this.subRatingArr[j] = {
                comment: "",
                ratingMasterId: this.questionData[i].id,
                subRatingQuesId: subElement.id,
                rating: "",
                organizationId: this.organizationId,
                studentId: this.studentId,
                mentorId: this.mentorId,
                hasGoals: false,
                goals: []
              }
            });
            this.ratingArr[i].subRatingArray = this.subRatingArr
            this.subRatingArr = [];
          }
          // console.log("rating arr i ", i)
        });
        console.log("this.ratingArr length", this.ratingArr.length)
        console.log("this.ratingArr", this.ratingArr)
      } else {
        console.log("error ", res);
      }
    })
  }
  addGoal(qusId, qusIndex) {
    console.log("add goal clicked")
    // this.ratingArr[qusIndex].goals[ind+1]={
    //   ratingMasterId: qusId,
    //   organizationId: this.organizationId,
    //   mentorId: this.mentorId,
    //   studentId: this.studentId,
    //   addedBy: "",
    // }

    this.ratingArr[qusIndex].goals.push({
      ratingMasterId: qusId,
      organizationId: this.organizationId,
      mentorId: this.mentorId,
      studentId: this.studentId,
      addedBy: "",
    })

    this.ratingArr[qusIndex].hasGoals = true;
  }
  removeGoal(qusIndex, ind) {
    this.ratingArr[qusIndex].goals.splice(ind, 1)
  }
  addSubGoal(qusId, subqId, qusIndex, subQusIndex) {
    console.log("add sub goal clicked")
    this.ratingArr[qusIndex].subRatingArray[subQusIndex].goals.push({
      ratingMasterId: qusId,
      organizationId: this.organizationId,
      mentorId: this.mentorId,
      studentId: this.studentId,
      addedBy: "",
      subRatingQuesId: subqId
    })
    // this.goals.push({
    //   ratingMasterId: qusId,
    //   organizationId: this.organizationId,
    //   mentorId: this.mentorId,
    //   studentId: this.studentId,
    //   addedBy: "",
    //   subRatingQuesId: subqId
    // })
    this.ratingArr[qusIndex].subRatingArray[subQusIndex].hasGoals = true;
  }
  removeSubGoal(qusIndex, subQusIndex, ind) {
    this.ratingArr[qusIndex].subRatingArray[subQusIndex].goals.splice(ind, 1)
  }
  sendRating() {
    let data = []
    // this.ratingArr.forEach((element, i) => {
    //   if (element.hasGoals != false || element.comment != "" || element.rating != "") {
    //     data.push(element)
    //   }
    //   if (element.subRatingArray && element.subRatingArray.length > 0) {
    //     element.subRatingArray.forEach(subElement => {
    //       if (subElement.hasGoals != false || subElement.comment != "" || subElement.rating != "") {
    //         data.push(subElement)
    //       }
    //     });
    //   }
    // });
    // if (this.goals.length > 0)
    //   data.push({ "goals": this.goals });

    this.ratingArr.forEach((element, i) => {
      data.push(element)
      if (element.subRatingArray && element.subRatingArray.length > 0) {
        element.subRatingArray.forEach(subElement => {
          data.push(subElement)
        });
      }
    });
    // data = this.ratingArr
    console.log("after alter data...... ", data);
    this.loading = true;
    let isVaild: boolean = true
    let count: number = 0;
    data.forEach((element, i) => {
      if (i < data.length - 1) {
        if (element.comment != "" || element.rating != "") {
          isVaild = true;
          count = count + 1
        } else {
          isVaild = false
        }
      }
    })
    // console.log("isValid....", isVaild)
    console.log("count...", count)
    if (count == 5) {
      this.studentService.mentorRating(this.organizationId, this.studentId, this.mentorId, data).subscribe((res: any) => {
        console.log("res ", res)
        this.loading = false;
        if (res.IsSuccess) {
          this.ts.pop("success", "", "Your response successfully submitted");
          setTimeout(() => {
            this.router.navigate(["student/mentor"])
          }, 2000);
          console.log("IsSuccess ", res)
        } else {
          console.log("IsSuccess ", res.Message)
          this.ts.pop("error", "", "Something went wrong, Please try again!");
        }
      })
    } else {
      this.loading = false;
      this.ts.pop("error", "", "Please fill in the mandatory field(s)");
    }

  }
}

