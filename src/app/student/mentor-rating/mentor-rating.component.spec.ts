import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorRatingComponent } from './mentor-rating.component';

describe('MentorRatingComponent', () => {
  let component: MentorRatingComponent;
  let fixture: ComponentFixture<MentorRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorRatingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
