import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { Location } from '@angular/common';

import { StudentService } from '../../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';

@Component({
  selector: 'app-bid-detail',
  templateUrl: './bid-detail.component.html',
  styleUrls: ['./bid-detail.component.css']
})
export class BidDetailComponent implements OnInit {
  
  constructor( private _ss: StudentService, private cs: CookieServiceProvider, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router, private location: Location ) { }

  loading: boolean = false;
  student_id: any;
  organization_id: any;
  bidId: any;
  courseId: any;
  leftAvailableCoin: any = 0;
  allBidDetail: any = [];
  studentBidDetail: any = {};
  isSelected: any = "";
  viewMore: boolean = false;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  ngOnInit() {
    window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.organization_id = this.cs.getItem('orgId');
    this.route.params.subscribe((params: Params) => {
      // this.firstName = params['fname'];
      // this.lastName = params['lname'];
      this.bidId = params['bidId'];
      this.courseId = params['courseId'];
    })
    this.fetchAvailableCoins();
    this.fetchAllBidDetail();
  }

  fetchAvailableCoins() {
    this.loading = true;
    this._ss.getAvailableCoins(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.leftAvailableCoin = data.Data.leftCoin
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      }
      else {
          this.loading = false;
          this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }

  fetchAllBidDetail() {
    this.loading = true;
    this._ss.getAllBidDetail(this.student_id, this.courseId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.allBidDetail = data.Data
        this.studentBidDetail = this.allBidDetail.find(obj => obj.studentId == this.student_id)
        this.isSelected = this.studentBidDetail.isSelected
        this.loading = false;
      }
      else {
          this.loading = false;
          this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }

  onBidChange(){
    this.router.navigate(["/student/edit-bid/", this.bidId, this.courseId]);
  }

  viewMoreFunc() {
    this.viewMore = !this.viewMore;
  }

  goBack() {
    //this.location.back()
    this.router.navigate(["/student/view-courses"])
  }

}
