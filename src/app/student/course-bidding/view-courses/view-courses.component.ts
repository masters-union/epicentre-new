import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";

import { StudentService } from '../../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';

@Component({
  selector: 'app-view-courses',
  templateUrl: './view-courses.component.html',
  styleUrls: ['./view-courses.component.css']
})
export class ViewCoursesComponent implements OnInit {
  
  constructor( private _ss: StudentService, private cs: CookieServiceProvider, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router ) { }

  loading: boolean = false;
  student_id: any;
  organization_id: any;
  program_id: any;
  allCourses: any = [];
  bidCourses: any = [];
  activeCourse: any = [];
  viewMore: boolean = false;
  viewMoreDesc: boolean = false;
  leftAvailableCoin : any = 0;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });


  ngOnInit() {
    window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.organization_id = this.cs.getItem('orgId');
    this.getStudentDetails();
    // this.fetchAllCourses();
    this.fetchAvailableCoins();
  }

  getStudentDetails() {
    this._ss.getStudentProfileDetails(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.program_id = data.Data.Program.id
        this.fetchAllCourses()
      }
    })
  }

  fetchAllCourses() {
    this.loading = true;
    this._ss.getAllCourses(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
      this.allCourses = data.Data.filter(val=> val.isBid==false)
      this.allCourses.forEach(e=>{
        let timeArr = e.CourseMaster.endTime.split(":")
		    let bidLastDate = new Date(e.CourseMaster.courseEndDate);
        bidLastDate.setHours(+timeArr[0],+timeArr[1], 0, 0)
        e.CourseMaster.courseEndDate = bidLastDate
        let timeArr1 = e.CourseMaster.startTime.split(":")
		    let bidStartDate = new Date(e.CourseMaster.courseStartDate);
        bidStartDate.setHours(+timeArr1[0],+timeArr1[1], 0, 0)
        e.CourseMaster.courseStartDate = bidStartDate
      })
      this.allCourses =  this.allCourses.filter(val=> new Date(val.CourseMaster.courseEndDate) >= new Date() && 
      new Date(val.CourseMaster.courseStartDate) <= new Date())
       this._ss.getAllBidCourses(this.student_id).subscribe((data: any) => {
        if (data.IsSuccess) {
         this.bidCourses = data.Data
        //  this.allCourses.forEach(element => {
        //    this.bidCourses.forEach(e => {
        //         if(element.id !== e.courseId){
        //           this.activeCourse.push(element)
        //         }
        //    });
        //  });
        let ids = new Set(this.bidCourses.map(({courseId})=> courseId));
        this.activeCourse = this.allCourses
        this.activeCourse = this.activeCourse.filter(({id}) => !ids.has(id))
        this.loading = false; 
        }
        else {
            this.loading = false;
            this.ts.pop("error", "", "Something went wrong, Please try again")
        }
      })
                   
      }
      else {
          this.loading = false;
          this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }

  fetchAvailableCoins() {
    this.loading = true;
    this._ss.getAvailableCoins(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.leftAvailableCoin = data.Data.leftCoin
        setTimeout(() => {
          this.loading = false;
        }, 3000);
      }
      else {
          this.loading = false;
          this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }

  viewMoreFunc() {
    this.viewMore = true;
  }

  viewMoreDescFunc() {
    this.viewMoreDesc = true;
  }

  viewCourseDetail(courseId){
    this.router.navigate(["/student/course-detail/", courseId]);
  }

  viewBidDetail(bidId, courseId){
    this.router.navigate(["/student/bid-detail/", bidId, courseId]);
  }

}
