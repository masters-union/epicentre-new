import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { Location } from '@angular/common';

import { StudentService } from '../../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {
  bidLastDate: any;
  

  constructor( private _ss: StudentService, private cs: CookieServiceProvider, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router, private location: Location ) { }

  loading: boolean = false;
  student_id: any;
  organization_id: any;
  courseDetail: any = {};
  allBidDetail: any = [];
  courseId: any;
  highestBidValue: any = 0;
  bidValue : any;
  leftAvailableCoin: any = 0;
  leftCoin: any = 0 ;
  minBidValue: any = 0;
  viewMore: boolean = false;
  bidGiven: boolean = false;
  h: any;
  m: any;
  s: any;
  d: any;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  ngOnInit() {
    window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.organization_id = this.cs.getItem('orgId');
    this.route.params.subscribe((params: Params) => {
      // this.firstName = params['fname'];
      // this.lastName = params['lname'];
      this.courseId = params['courseId'];
    })
    this.fetchCourseDetail();
    this.fetchAllBidDetail();
    this.fetchAvailableCoins();
  }

  fetchCourseDetail() {
    this.loading = true;
    this._ss.getCourseById(this.student_id, this.courseId).subscribe((data: any) => {
      if (data.IsSuccess) {
        if(data.Data){
          this.courseDetail = data.Data.CourseMaster;
          this.minBidValue = this.courseDetail.minBidValue
          let timeArr = this.courseDetail.endTime.split(":")
          this.bidLastDate = new Date(this.courseDetail.courseEndDate);
          this.bidLastDate.setHours(+timeArr[0],+timeArr[1], 0, 0)
          this.loading = false;
          this.countdown(this.bidLastDate);
        }
        else {
          this.loading = false;
          this.bidGiven = true;
          this.ts.pop("error", "", "Bid already given")
        }
        
      }
      else {
          this.loading = false;
          this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }

  countdown(bidLastDate){
    let now = new Date();
    let currentTime = now.getTime();
		let eventTime = bidLastDate.getTime();
    let remTime = eventTime - currentTime;

    this.s = Math.floor(remTime / 1000);
    this.m = Math.floor(this.s / 60);
    this.h = Math.floor(this.m / 60);
    this.d = Math.floor(this.h / 24);

		this.h %= 24;
		this.m %= 60;
		this.s %= 60;
    setTimeout(()=>{this.countdown(bidLastDate)}, 1000);
		
  }


  fetchAllBidDetail() {
    this.loading = true;
    this._ss.getAllBidDetail(this.student_id, this.courseId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        this.allBidDetail = data.Data
        if(this.allBidDetail.length>0){
          this.highestBidValue = this.allBidDetail.sort((a, b) => b.bidValue - a.bidValue)[0].bidValue
        }
        
      }
      else {
          this.loading = false;
          this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }

  fetchAvailableCoins() {
    this.loading = true;
    this._ss.getAvailableCoins(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.leftAvailableCoin = data.Data.leftCoin
        this.loading = false;
      }
      else {
          this.loading = false;
          this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }


  bidCourse() {
    this.leftCoin = this.leftAvailableCoin - this.bidValue
    let courseEndDate= this.bidLastDate
    if(courseEndDate >= new Date()){
      if(+this.bidValue >= +this.minBidValue){
        if(this.leftAvailableCoin >= this.bidValue){
          let obj = {
            bidValue : this.bidValue,
            courseId : this.courseId,
            leftCoin : this.leftCoin
          }
          this.loading = true
          this._ss.bidCourse(this.student_id, obj).subscribe((data: any) => {
            if (data.IsSuccess) {
              this.loading = false;
              this.router.navigate(["/student/view-courses"])
            }
            else {
                this.loading = false;
                this.ts.pop("error", "", data.Message)
            }
          })
        }
        else {
          this.loading = false;
          this.ts.pop("error", "", "You have less available coins")
        }
      }
      else {
        this.loading = false;
        let msg = `Minimum bid is ${this.minBidValue} points`
        this.ts.pop("error", "", msg)
      }
      
    }
    else {
      this.loading = false;
      this.ts.pop("error", "", "Course bid date ended")
    }
    
    
  }

  viewMoreFunc() {
    this.viewMore = !this.viewMore;
  }

  goBack() {
    //this.location.back()
    this.router.navigate(["/student/view-courses"])
  }

}
