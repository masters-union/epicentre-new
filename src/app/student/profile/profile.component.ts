import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
// import { AuthService } from 'angularx-social-login';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loading: boolean = false;
  saveloading: boolean = false;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  // studentForm: FormGroup;
  // personalDetailsForm: FormGroup;
  changePasswordForm: FormGroup;
  confirmPasswordError: boolean;
  submited: boolean;
  oldpasswordError: boolean;
  passwordError: boolean;
  passwordMatch: boolean;
  oldPasswordWrong: boolean;
  student_id: String = "";
  student_token: String = '';
  student_email: String = '';
  details: any;
  organizationId: any;
  updateDetailForm: boolean = false;
  updatePersonalDetailForm: boolean = false;
  qualfication = [];
  profileData: any;
  formD: FormData;
  fileName: string;
  studentProfileId: any;
  firstName: any;
  lastName: any;
  constructor(private _ss: StudentService, private cs: CookieServiceProvider, private fb: FormBuilder, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router) { }

  ngOnInit() {
    window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.student_token = this.cs.getItem('token');

    this.organizationId = this.cs.getItem('orgId')
    this.getStudentDetails();
    this.getStudentProfile();
    // this.studentFormInit();
    this.submited = false;
    this.passwordMatch = true
    this.initForm();

  }

  getStudentDetails() {
    this.loading = true
    this._ss.getStudentProfileDetails(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        // console.log(data.Data)
        this.student_email = data.Data.officialEmail
        this.details = data.Data
        let d = this.details.StudentAcademicDetails;
        d.sort((a, b) => new Date(b.yearOfPassing).getTime() - new Date(a.yearOfPassing).getTime())
        this.qualfication = d;
        this.loading = false
        // console.log(data.Data)
      }
    })
  }

  getStudentProfile() {
    // this.loading = true
    // this._ss.getStudentProfileData(this.organizationId, this.student_id).subscribe((data: any) => {
    //   if (data.IsSuccess) {
    //     this.profileData = data.Data
    //     if(data.Data){
    //       this.studentProfileId = data.Data.id
    //     }
        
    //     // console.log(this.profileData)
    //     this.loading = false
    //     // console.log(data.Data)
    //   }
    // })

  }

  goToResumePage(fname, lname, slug){
    if(slug) {
      this.router.navigate(['/student/resume', slug]);
    }
    else{
      this.firstName = fname
      this.lastName = lname
      slug = this.firstName.toLowerCase() + "-" + this.lastName.toLowerCase()
      this.router.navigate(['/student/resume', slug]);
    }
    
  }

  initForm() {
    this.changePasswordForm = this.fb.group({
      password: ['', Validators.required],
      confirmPassword: [null, Validators.required],
      oldpassword: ['', Validators.required]
    },
      { validators: this.confirmPasswordF('password', 'confirmPassword') })
  }
  confirmPasswordF(password: string, confirmPassword: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let pass = group.controls[password];
      let cnfpass = group.controls[confirmPassword];
      if (pass.value !== cnfpass.value) {
        this.passwordMatch = false
        return {
          confirmPasswordF: true
        };
      }
      this.passwordMatch = true
      return null;
    }
  }

  submitPassword() {
    this.submited = true;
    this.oldpasswordError = this.changePasswordForm.controls.oldpassword.invalid;
    this.passwordError = this.changePasswordForm.controls.password.invalid;
    this.confirmPasswordError = this.changePasswordForm.controls.confirmPassword.invalid;
    this.oldPasswordWrong = false;
    let data = {
      id: this.student_id,
      password: this.changePasswordForm.value.password,
      email: this.student_email,
      oldpass: this.changePasswordForm.value.oldpassword
    }

    if (!this.passwordError && this.passwordMatch && !this.oldpasswordError) {
      this.saveloading = true;
      // console.log(data);

      this._ss.changePassword(data).subscribe((data: any) => {
        if (data.IsSuccess) {
          document.getElementById('closeModel').click();
          this.saveloading = false;
          this.ts.pop("success", "", "Password changed successfully, Redirecting to login page..");
          setTimeout(() => {
            this.changePasswordForm.reset();
            // this.authService.signOut().then().catch(err => { });
            this.cs.logout()
          }, 3000);
        }
        else {
          this.saveloading = false;
          this.oldPasswordWrong = true;
          if (data.Message) {
            this.ts.pop("error", "", data.Message)
          }
          else {
            this.ts.pop("error", "", "Something went wrong! Please try again")
          }
          this.changePasswordForm.reset();
        }

      }, err => {
        console.log(err)
        this.saveloading = false;
        this.ts.pop("error", "", "Something went wrong! Please try again")
        this.changePasswordForm.reset();
      })
    }
  }
  uploadResume(event, firstName, lastName) {
    this.loading = true;
    if (event.target.files) {
      this.formD = new FormData();
      const file: File = event.target.files[0];
      this.fileName = file.name
      this.formD.append('file', file, file.name)
      let data = {
        file: this.formD,
        slug: firstName.toLowerCase() + "-" + lastName.toLowerCase()
      }
      this._ss.uploadResume(this.organizationId, this.student_id, this.formD).subscribe((res: any) => {
        // console.log("resume upload res....", res)
        this.loading = false;
        if (res.IsSuccess) {
          
          // this.updateStudentResumePath(res.Data)
          this.ts.pop("success", "", "Upload Successfully");
        } else {
          this.ts.pop("error", "", res.Message)
        }
      })
    }
  }


  // updateStudentResumePath(path) {
  //   let object = {
  //     resumePath : path
  //   }
  //   this._ss.editStudentProfileData(this.organizationId, this.student_id, this.studentProfileId, object).subscribe((res: any) => {
  //       if (res.IsSuccess) {
  //         this.getStudentProfile()
  //       } else {
          
  //       }
  //     })
  // }

}




  // updateDetail() {
  //   this.updateDetailForm = true;
  //   window.scrollTo({
  //     top: 300,
  //     behavior: 'smooth',
  //   });
  // }
  // updatePersonalDetail() {
  //   this.updatePersonalDetailForm = true;
  //   window.scrollTo({
  //     top: 300,
  //     behavior: 'smooth',
  //   });
  // }
