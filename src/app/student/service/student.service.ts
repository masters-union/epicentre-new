import { ErrorHandler, Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";

@Injectable({
	providedIn: "root",
})
export class StudentService {

  constructor(private http: HttpClient,
    private cookieService: CookieServiceProvider) { }

  fetchStudentFeeDetails2(id) {
    return this.http.post(environment.baseURL + '', id).pipe(catchError(this.handleError))
  }

  fetchStudentFeeDetails(id) {
    return this.http.get<any>(environment.baseURL + '/' + environment.orgId + '/stu/' + id + '/FeeDetails')
  }

  fetchStudentProfile(id) {
    return this.http.get(environment.baseURL + 'student/' + id + '/profile').pipe(catchError(this.handleError))
  }

  getStudentTransationDetails(id, searchParam) {

    return this.http.get<any>(environment.baseURL + '/' + environment.orgId + '/stu/' + id + '/transactionDetails?searchParams=' + searchParam)

  }
  getStudentProfileDetails(id) {
    return this.http.get<any>(environment.baseURL + '/' + environment.orgId + '/stu/' + id + '/studentDetails')
  }
  getFeeSlipByFeeDueId(id, studentFeeDueId) {
    return this.http.get<any>(environment.baseURL + '/' + environment.orgId + '/stu/' + id + '/studentFeeDueId/' + studentFeeDueId)
  }
  getCategory(id) {
    //console.log(id)
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL + '/' + id + '/getCategories').pipe(catchError(this.handleError))

  }
  getSubCategory(catid, orgid) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL + '/' + orgid + '/category/' + catid + '/getSpecificCategories').pipe(catchError(this.handleError))

  }

  fetchAllEnquiry(orgId, organizationStudentId) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL + '/' + orgId + '/stu/' + organizationStudentId + '/getAllEnquiry').pipe(catchError(this.handleError))

  }
  fetchAllStudentEnquiry(orgId, organizationStudentId, searchParam, pageNo, pageSize,statusParam) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL + '/' + orgId + '/stu/' + organizationStudentId + '/getSpecificEnquiry?searchParams=' + searchParam + '&pageNo=' + pageNo + '&pageSize=' + pageSize + '&statusParam=' + statusParam).pipe(catchError(this.handleError))

  }


  addNewEnquiry(orgId, studentId, object) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/addNewEnquiry', object).pipe(catchError(this.handleError))

  }
  fetchAllLeadEnquiry(orgId, organizationLeadId, searchParam, pageNo, pageSize) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL + '/' + orgId + '/lead/' + organizationLeadId + '/getSpecificLeadEnquiry?searchParams=' + searchParam + '&pageNo=' + pageNo + '&pageSize=' + pageSize).pipe(catchError(this.handleError))

  }

  changePassword(object) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post(environment.baseURL + '/changePassword', object).pipe(catchError(this.handleError))

  }

  fetchAllNotification(id, orgId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + id + '/notifications').pipe(catchError(this.handleError))
  }

  upateSeenNotification(id, orgId, obj) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + id + '/updateSeen', obj).pipe(catchError(this.handleError))
  }

  fetchAllMessage(id, orgId, searchParam, pageNo, pageSize) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + id + '/messages?searchParams=' + searchParam + '&pageNo=' + pageNo + '&pageSize=' + pageSize).pipe(catchError(this.handleError))
  }
  deleteMessage(id, orgId) {
    return this.http.delete<any>(environment.baseURL + '/' + orgId + '/messageId/' + id + '/deleteMessage').pipe(catchError(this.handleError))
  }

  deleteSelectedMessages(orgId, messageIds) {
    return this.http.post(environment.baseURL + "/" + orgId + '/deletemessages', { 'msgIds': messageIds })
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.error('Client side error: ', errorResponse.error)
    }
    else {
      console.error('Server side error: ', errorResponse)
    }
    return throwError('There is problem with the service')
  }


  onlineFeePaymentByStudent(stuId, feeDueId, obj) {
    let formData: FormData = new FormData();
    // formData.append("appId", environment.appId);
    // formData.append('secretKey', environment.secretKey);
    formData.append("orderId", obj.id);
    formData.append('orderAmount', obj.amount);
    formData.append('orderCurrency', 'INR');
    formData.append('orderNote', 'fee amount');
    formData.append('customerEmail', obj.studentEmail);
    formData.append('customerName', obj.studentName);
    formData.append('customerPhone', obj.phone);
    formData.append('returnUrl', 'https://fee-test.mastersunion.org/student/paymentStatus/' + obj.id + '/' + obj.studentFeeDueId);
    formData.append('notifyUrl', 'https://fee-test.mastersunion.org/student/transactionHistory');
    let data = {
      customerEmail: obj.studentEmail,
      customerName: obj.studentName,
      customerPhone: obj.phone,
      studentFeeDueId: obj.studentFeeDueId
    }
    //console.log("service data is ", data)
    let orgId = environment.orgId
    // return this.http.get<any>('http://localhost:31350/api/org/9083372f-815f-4d49-b9cc-1576e0b33e46/stu/9c36abcd-24a7-4c0a-90e6-556e166151d/feeDueId/c288fe34-e001-4a22-aa35-953b73f413ea/orderId/8384393_9c36abcd-24a7-4c0a-90e6-556e166151d9/updateFeeAfterPaymentReturnUrl').pipe(catchError(this.handleError))

    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + stuId + '/dueId/' + feeDueId + '/onlineFeePaymentByStudent', data).pipe(catchError(this.handleError))
    // return this.http.post<any>('https://test.cashfree.com/api/v1/order/create', formData)
  }

  getFeeOrderStatus(stuId, orderId) {
    // let formData: FormData = new FormData();
    // formData.append("appId", environment.appId);
    // formData.append('secretKey', environment.secretKey);
    // formData.append("orderId", orderId);
    let orgId = environment.orgId


    // console.log("data is ", obj);
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + stuId + '/orderId/' + orderId + '/feePaymentByStatus').pipe(catchError(this.handleError))
    // return this.http.post<any>('https://test.cashfree.com/api/v1/order/info/status', data)
  }
  
  updateFeeAfterPayment(stuId, obj) {
    let orgId = environment.orgId
    // console.log("data is ", obj);
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + stuId + '/updateFeeAfterPayment', obj).pipe(catchError(this.handleError))
  }
  editEnquiry(orgId, organizationStudentId, id, obj) {
    return this.http.put(environment.baseURL + '/' + orgId + '/stu/' + organizationStudentId + '/enquiry/' + id + '/updateEnquiry', obj)
  }
  editLeadEnquiry(orgId, organizationLeadId, id, obj) {
    return this.http.put(environment.baseURL + '/' + orgId + '/lead/' + organizationLeadId + '/enquiry/' + id + '/updateEnquiryLead', obj)
  }
  leadFeedback(organizationLeadId,id,obj){
    let organizationId = environment.orgId
    return this.http.put(
      environment.baseURL +
      '/'+
      organizationId +
      + '/lead/' + organizationLeadId + +'/enquiry/'+id+ "/updateEnquiryByLead", obj
    );


  }
  leadEmail(id){
    let organizationId = environment.orgId
    return this.http.get(
      environment.baseURL +
      '/'+
      organizationId 
      + '/lead/' + id + "/getLeadEmail"
    );


  }
  addNewLeadEnquiry(orgId, organizationLeadId, object) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post(environment.baseURL + '/' + orgId + '/lead/' + organizationLeadId + '/addNewLeadEnquiry', object).pipe(catchError(this.handleError))

  }
  getspecificLeadResponse(enquiryId,organizationLeadId){
    let orgId = environment.orgId
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/lead/'+ organizationLeadId +"/enquiry/"+ enquiryId+"/getLeadUserResponse" )
  }
  userResponseToLead(organizationLeadId,obj) {
    let organizationId = environment.orgId

    return this.http.post(
      environment.baseURL +
      '/'+
      organizationId +
      '/lead/'+ organizationLeadId + "/userResponseLead", obj
    );


  }
  fetchRatingQuestionsForStudent(organizationId) {
    return this.http.get(environment.baseURL + "/" + organizationId + "/getRatingQuestion")
  }
  mentorRating(organizationId, studentId, mentorId, obj) {
    return this.http.post(environment.baseURL + "/" + organizationId + "/stu/" + studentId + "/" + mentorId + "/mentorRating", obj)
  }
  getAllAssignedStudent(organizationId, studentId, searchParam, pageNo, pageSize) {
    return this.http.get(environment.baseURL + '/' + organizationId + "/stu/" + studentId + "/allAssignedMentors?searchParams=" + searchParam + "&pageNo=" + pageNo + "&pageSize=" + pageSize)
  }
  //not required
  // getRatedQuestion(organizationId, studentId) {
  //   let orgId = environment.orgId
  //   return this.http.get(environment.baseURL + '/' + organizationId + "/stu/" + studentId +  "/getRatedQuestion")
  // }
  getStudentRating(organizationId, studentId, mentorId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/mentorId/" + mentorId + "/specificStudentRatings")
  }

  saveNotes(organizationId, studentId, obj) {
    return this.http.post(environment.baseURL + '/' + organizationId + '/stu/' + studentId + '/addNotepad', obj).pipe(catchError(this.handleError))
  }
  getSpecifcNotes(organizationId, studentId) {
    return this.http.get(environment.baseURL + '/' + organizationId + '/stu/' + studentId + '/getSpecificNotepad').pipe(catchError(this.handleError))
  }
  updateNote(organizationId, studentId, obj) {
    return this.http.put(environment.baseURL + '/' + organizationId + '/stu/' + studentId + '/updateNotepad', obj).pipe(catchError(this.handleError))
  }
  deleteNotes(organizationId, id, obj) {
    return this.http.post(environment.baseURL + '/' + organizationId + '/stu/' + id + '/deleteNotepad', obj).pipe(catchError(this.handleError))
  }
  checkIsStudentAlreadyRateMentor(organizationId, studentId, mentorId) {
    return this.http.get(environment.baseURL + "/" + organizationId + "/stu/" + studentId + "/mentor/" + mentorId + "/getIsRatingAlreadyGivenByStudent")
  }
  uploadResume(orgId, stuId, data) {
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    headers.append('Accept', 'application/json');
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + stuId + '/uploadResume', data).pipe(catchError(this.handleError))

  }
  updateUtrNumber(studentId, studentFeeDueId, obj) {
    let organizationId = environment.orgId
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.put<any>(environment.baseURL + '/' + organizationId + '/stu/' + studentId + '/feeDue/' + studentFeeDueId + '/verifyUtrRequest', obj).pipe(catchError(this.handleError))



  }
  getStudentProfileData(organizationId, studentId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/getProfileByStudentId")
  }

  getStudentProfileMasterUnion(studentId, slug) {
    // return this.http.get('https://api.mastersunion.org/api/studentResume/' + slug)
    // return this.http.get('http://localhost:33000/api/studentResume/' + slug)
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/slug/" + slug  + "/studentResume")
  }
  editStudentProfileData(studentId, slug, obj) {
    // return this.http.put('http://localhost:33000/api/editStudentResume/'+ slug, obj).pipe(catchError(this.handleError))
    // return this.http.put('https://api.mastersunion.org/api/editStudentResume/'+ slug, obj).pipe(catchError(this.handleError))
    let orgId = environment.orgId
    return this.http.put(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/slug/" + slug  + "/editStudentResume", obj)
  }

  //survey
  getSurveyById(studentId, surveyId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/survey/" + surveyId + "/getSurveyById") 
  }

  getAllSurveyByStuId(studentId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/getAllSurveyByStuId") 
  }
  createSurveyAnswers(studentId, obj){
    let orgId = environment.orgId
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/createSurveyAnswers', obj).pipe(catchError(this.handleError))
  }
  
  //get events
  createEvent(studentId, obj) {
    let orgId = environment.orgId
    return this.http.post(environment.baseURL + '/' + orgId + "/stu/" + studentId + '/event', obj)
  }

  getAllEvents(studentId, searchParam, club) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + '/getEvents?searchParams=' + searchParam + '&club=' + club)
  }
  getClubs(studentId, isPublished, clubStatus) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/getClubs?isPublished=" + isPublished + '&clubStatus=' + clubStatus)
  }
  getEventById(studentId, eventId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/getEvent')
  }

  eventRegisration(studentId, obj) {
    let orgId = environment.orgId
    return this.http.post(environment.baseURL + '/' + orgId + "/stu/" + studentId + '/eventRegisration', obj)
  }

  eventReview(studentId, eventId, obj) {
    let orgId = environment.orgId
    return this.http.post(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/eventReview', obj)
  }

  getEventReviews(studentId, eventId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/eventReviews')
  }

  addEventGallery(studentId, eventId, obj) {
    let orgId = environment.orgId
    return this.http.post(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/eventGallery', obj)
  }

  getEventGallery(studentId, eventId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/eventGallery')
  }
  // getSpecificEvent(studentId, eventId){
  //   let orgId = environment.orgId
  //   return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/specificEvent') 
  // }

  deletEvent(studentId, eventId) {
    let orgId = environment.orgId
    return this.http.delete(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/event')
  }

  deletClub(studentId, clubId) {
    let orgId = environment.orgId
    return this.http.delete(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/club/" + clubId + '/club')
  }
  getEventUsers(studentId, eventId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/getEventUsers')
  }
  sendEventRequest(studentId, obj){
    let orgId = environment.orgId
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/sendEventRequest', obj).pipe(catchError(this.handleError))
  }
  createAnonymousSurveyAnswers(obj){
    let orgId = environment.orgId
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/createAnonymousSurveyAnswers', obj).pipe(catchError(this.handleError))
  }

  getspecificStudentResponse(enquiryId,orgStudentId){
    let orgId = environment.orgId
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/'+ orgStudentId +"/enquiry/"+ enquiryId+"/getUserResponse" )
  }
  userResponseToStudent(organizationStudentId,obj) {
    let organizationId = environment.orgId

    return this.http.post(
      environment.baseURL +
      '/'+
      organizationId +
      "/stu/" +
      organizationStudentId + "/userResponse", obj
    );


  }
  studentFeedback(organizationStudentId,id,obj){
    let organizationId = environment.orgId
    return this.http.put(
      environment.baseURL +
      '/'+
      organizationId +
      "/stu/" +
      organizationStudentId +'/enquiry/'+id+ "/updateEnquiryByStudent", obj
    );


  }
  getSpecifcStudentEnquiryDetails(organizationStudentId,id){
    let organizationId = environment.orgId
    return this.http.get(
      environment.baseURL +
      '/'+
      organizationId +
      "/stu/" +
      organizationStudentId +'/enquiry/'+id+ "/getSpecificEnquiryDetails"
    );


  }



  //course bid
  getAllCourses(studentId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/getAllCourses") 
  }
  getCourseById(studentId, courseId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/course/" + courseId + '/getCourseById') 
  }
  bidCourse(studentId, obj){
    let orgId = environment.orgId
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/bidCourse', obj)
  }
  editCourseBid(studentId, bidId, obj){
    let orgId = environment.orgId
    return this.http.put<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/bid/' + bidId + '/editBid', obj)
  }
  getAllBidCourses(studentId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/getAllBidCourses')
  }
  getAllBidDetail(studentId, courseId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + '/stu/' + studentId + "/course/" + courseId + '/getAllBidDetail')
  }
  getAvailableCoins(studentId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/getAvailableCoins')
  }

  submitApplicationForm(orgId,studentId, obj){
    // let orgId = environment.orgId
    console.log("in service obj",obj);
    
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/sendStudentLeaveRequest', obj)
  }

  getAllLeavesList(studentId,searchParam){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/getSpecificStudentLeaveRecords?searchParams=" + searchParam) 
  }

  getStudentLeaveDetails(leaveId) {
    let studentId = this.cookieService.getItem("id");
    let organizationId = this.cookieService.getItem("orgId");

    return this.http.get(
      environment.baseURL +
      '/'+
      organizationId +
      "/stu/" +
      studentId + 
      "/leave/"+
      leaveId+ 
      "/getStudentLeaveDetails"
    );
    
  }

  

  getCourseRosterList(studentId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/getStuPublishedCourseRoster") 
  }

  cancelLeaveRequest(orgId,studentId,leaveId, obj){
    // let orgId = environment.orgId
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/leave/'+leaveId+'/cancelLeaveRequest', obj)
  }

  getAllStudentList(orgId, studentId, programId, programMasterId, searchParam, pageNo, pageSize, sortBy) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/allStudentList?programId=' + programId + "&programMasterId=" + programMasterId + "&searchParams=" +
    searchParam +
    "&pageNo=" +
    pageNo +
    "&pageSize=" +
    pageSize +
    "&sortBy=" +
    sortBy)
  }

  getAllStudentOrgList(orgId, studentId, programId, programMasterId, searchParam, pageNo, pageSize, sortBy) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/allStudentOrgList?programs=' + programId + "&programMaster=" + programMasterId + "&searchParams=" +
    searchParam +
    "&pageNo=" +
    pageNo +
    "&pageSize=" +
    pageSize +
    "&sortBy=" +
    sortBy)
  }

  getProgramMaster(orgId, studentId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/programMaster')
  }

  getProgramsByMasterID(orgId, studentId, programMasterId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' +  studentId + "/programMaster/" + programMasterId + '/programsByMaster')
  }
  createClub(orgId, studentId, data) {
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    headers.append('Accept', 'application/json');
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' +  studentId + "/club",data).pipe(catchError(this.handleError))
  }

  uploadSurveyFile(orgId, data) {
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    // headers.append('Access-Control-Allow-Headers',"*")
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/uploadFile', data).pipe(catchError(this.handleError))

  }

  getAllClubs(orgId, studentId, searchParams, clubType) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/getAllClubs?searchParams=' + searchParams + '&clubType=' + clubType);
  }

  getAllRequestedClubs(orgId, studentId, searchParams, clubStatus) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/getAllRequestedClubs?searchParams=' + searchParams + '&clubStatus=' + clubStatus);
  }

  getClubById(orgId, studentId, clubId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/club/' + clubId + '/getClubById');
  }

  getUnpublishedClubById(orgId, studentId, clubId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/club/' + clubId + '/getUnpublishedClubById');
  }

  getClubComments(orgId, studentId, clubId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/club/' + clubId + '/getSpecificComments');
  }

  getSpecificClub(orgId, studentId, clubId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/club/' + clubId + '/getClubById')
  }

  getEventsByClubId(orgId, studentId, clubId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/club/' + clubId + '/eventsByClubId')
  }


  
  getAllClubIntersetedStu(orgId, studentId, clubId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/club/' + clubId + '/allClubIntersetedStu')
  }
  
  uploadClubReport(orgId, studentId, obj) {
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    // headers.append('Accept', 'application/json');
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/addClubReport', obj)
  }
  getCourseByTermList(studentId, batch, term) {
		let orgId = environment.orgId;
		return this.http.get(
			environment.baseURL +
				"/" +
				orgId +
				"/stu/" +
				studentId +
				"/course/" +
        term+
        "/"+
				batch +
				"/getStuPublishedCourseRoster"
		);
	}
  getAssigmentsTotalCount(studentId, batch, term,outClass) {
		let orgId = environment.orgId;
		return this.http.get(
			environment.baseURL +
				"/" +
				orgId +
				"/stu/" +
				studentId +
				"/course/" +
        term+
        "/"+
				batch +
        "/outClass/"+
        outClass+
				"/getAssignmentsTotalCount"
		);
	}
  getTranscriptDetails(studentId, batch, term,outClass) {
		let orgId = environment.orgId;
		return this.http.get(
			environment.baseURL +
				"/" +
				orgId +
				"/stu/" +
				studentId +
				"/course/" +
        term+
        "/"+
				batch +
        "/"+
				outClass +
				"/getTranscriptDetails"
		);
	}
  createClubComment(orgId, studentId, data){
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/createClubComments', data)
  }
  editApprovedClubRequest(orgId, studentId, data) {
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    headers.append('Accept', 'application/json');
    return this.http.put<any>(environment.baseURL + '/' + orgId + '/stu/'+ studentId +'/editApprovedClubRequest', data).pipe(catchError(this.handleError))

  }
  editClubRequest(orgId, studentId, data) {
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    headers.append('Accept', 'application/json');
    return this.http.put<any>(environment.baseURL + '/' + orgId + '/stu/'+ studentId +'/editClubRequest', data).pipe(catchError(this.handleError))

  }




  getAllUnPublishedEvents(orgId, studentId, searchParam, eventStatus) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/getAllUnPublishedEvents?searchParams=' + searchParam +'&eventStatus=' + eventStatus);
  }

  getAllPresidentPublishedClub(orgId, studentId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/getAllPresidentPublishedClub');
  }

  getAllPublishedEvents(orgId, studentId, searchParam, eventType) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/getAllPublishedEvents?searchParams=' + searchParam + '&eventType=' + eventType);
  }

  getUnpublishedEventById(studentId, eventId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId + "/event/" + eventId + '/getUnpublishedEventById') 
  }

  editApprovedEventRequest(orgId, studentId, data) {
    return this.http.put<any>(environment.baseURL + '/' + orgId + '/stu/'+ studentId +'/editApprovedEventRequest', data).pipe(catchError(this.handleError))

  }
  editEventRequest(orgId, studentId, data) {
    return this.http.put<any>(environment.baseURL + '/' + orgId + '/stu/'+ studentId +'/editEventRequest', data).pipe(catchError(this.handleError))

  }

  getEventComments(orgId, studentId, eventId) {
    return this.http.get<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/event/' + eventId + '/getSpecificComments');
  }
  createEventComment(orgId, studentId, data){
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/createEventComments', data)
  }

  getCoursesList(studentId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/getSpecificStudentCoursePrefrences") 
  }
    // /:organizationId/user/:userId/course/:coursePreferId/getStudentSpecificCoursePrefrence
  fetchCoursesList(coursepreferenceId,userId){
    
    let orgId = environment.orgId
    return this.http.get(environment.baseURL+'/'+orgId+'/user'+'/'+userId+'/course/'+coursepreferenceId+'/'+'getStudentSpecificCoursePrefrence')

  }

  submitCoursePreference(orgId,studentId, data,coursePrefrenceId){
    let obj ={
      selectedPreference:data,
      coursePrefrenceId:coursePrefrenceId
  }
    console.log("in service data",obj);
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/addStudentPreference', obj)
  }

  fetchCoursePreferenceList(coursepreferenceId,studentId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL+'/'+orgId+'/stu/'+studentId+'/course/'+coursepreferenceId+'/'+studentId+'/getSpecificStudentResponse')
  }
  // Assingment 
  getAllSubjectMarks(studentId,term){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL+'/'+orgId+'/stu/'+studentId+'/term/'+term+'/getAllSubjectMarks')
  }
  getMarksByCourseId(studentId, courseId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL+'/'+orgId+'/stu/'+studentId+'/course/'+courseId+'/getSpecificStudentMarks')
  }

  getStudentCourses(studentId,programId,searchParam){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/programId/"+programId+ "/getStudentCourses?searchParams=" + searchParam);
  }
  getMyCourses(studentId,programId,searchParam){
    let orgId = environment.orgId;
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/programId/"+programId+ "/getMyCourses?searchParams=" + searchParam);
  }
  getParticularCourseDetails(studentId,courseId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/courseId/"+courseId+ "/getParticularCourseDetails");
  }
  // getParticularElectiveCourseDetails(studentId,courseId){
  //   let orgId = environment.orgId
  //   return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/courseId/"+courseId+ "/getParticularElectiveCourseDetails");
  // }
  getDashboardDetails(studentId,programId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/programId/"+programId+ "/getDashboard");
  }
  getListOfElectiveCourses(studentId,programId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/programId/"+programId+ "/getListOfElectiveCourses");
  }
  submitCourseBiddingValues(studentId,data){
    let orgId = environment.orgId;
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/stu/' + studentId + '/addStudentBidValues', data)  
  }
  // /:organizationId/stu/:studentId/courseId/:courseId/getStudentCourseAttendance
  getStudentCourseAttendance(studentId,courseId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/courseId/"+courseId+ "/getStudentCourseAttendance");
  }
  getAllElectiveCourse(organizationId, studentId, programId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/program/"+programId+ "/getAllElectiveCourse");
  }

  getAllElectiveSession(organizationId, studentId, courseId){
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  +"/course/"+courseId+ "/getAllElectiveSession");
  }
  getCourseByStudentId(organizationId, studentId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/getCourseByStudentId") 
  } 
  // DOWNLOAD TRANSCRIPT
  downloadTranscript(studentId,batch,term) {
    let orgId = environment.orgId
      return this.http.get(
        environment.baseURL +'/'+
        orgId +
        "/stu/" +
        studentId +'/'+batch+'/'+term+
        "/downloadTranscript",
        { responseType: "blob" }
      );
   
  }
  // userlist
  getUserList(organizationId, studentId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/stu/" + studentId  + "/usersList") 
  } 
}
