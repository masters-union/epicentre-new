import { Component, OnInit } from '@angular/core';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';
declare var $
@Component({
  selector: 'app-student-menu',
  templateUrl: './student-menu.component.html',
  styleUrls: ['./student-menu.component.css']
})
export class StudentMenuComponent implements OnInit {
email:any
student_id: any;
details:any;
programId:any
password:any
  constructor(private _cs: CookieServiceProvider, private _ss: StudentService,) { }

  ngOnInit() {
    this.student_id = this._cs.getItem('id');
    this.password = this._cs.getItem('password');
    this.email = this._cs.getItem('email');
    // window.console.log = function () { };
    $(document).ready(function(){    
      // toggle menu 
    $(".sub-menu-toggle").click(function(e){
         $(" #side-menu").toggleClass("sub-menu-open");
         e.stopPropagation();
        
      });
      //  on body click close menu 
      $(document).click(function(e) {
          if (!$(e.target).is(' #side-menu')) {
              $(" #side-menu").removeClass("sub-menu-open");
          }
        });
        //  on body click close menu ends

    });
    this.email = this._cs.getItem('email')
    this.programId = this._cs.getItem('programBatchId')

    this.getStudentDetails();
  }
  getStudentDetails() {
    this._ss.getStudentProfileDetails(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.details = data.Data    
        }
    })
  }
  coachSession(){
    let link=`http://mentee-coaching.mastersunion.org/`

    window.open(link, "_blank");
  }
  coWork(){
    let link=`https://cowork.mastersunion.org/?loginURL=${this.email}&password=${this.password}`
    window.open(link, "_blank");
  }

}
