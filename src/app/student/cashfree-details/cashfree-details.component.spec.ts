import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashfreeDetailsComponent } from './cashfree-details.component';

describe('CashfreeDetailsComponent', () => {
  let component: CashfreeDetailsComponent;
  let fixture: ComponentFixture<CashfreeDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashfreeDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashfreeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
