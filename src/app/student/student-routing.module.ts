import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { StudentPortalComponent } from "./student-portal/student-portal.component";
import { AuthGuard } from "../common/service/auth.guard";
import { EnquiryPortalComponent } from "./enquiry-portal/enquiry-portal.component";
import { TransactionHistoryComponent } from "./transaction-history/transaction-history.component";
import { ProfileComponent } from "./profile/profile.component";
import { FeeSlipComponent } from "./fee-slip/fee-slip.component";
import { InboxComponent } from "./inbox/inbox.component";
import { PaymentStatusComponent } from "./payment-status/payment-status.component";
import { AssignMentorsComponent } from "./assign-mentors/assign-mentors.component";
import { MentorRatingComponent } from "./mentor-rating/mentor-rating.component";
import { ShowRatingComponent } from "./show-rating/show-rating.component";
import { StudentResumeComponent } from "./student-resume/student-resume.component";
import { RefundPolicyComponent } from "./refund-policy/refund-policy.component";
import { SurveyListComponent } from "./survey-list/survey-list.component";
import { SurveyDetailComponent } from "./survey-detail/survey-detail.component";
import { CashfreeDetailsComponent } from "./cashfree-details/cashfree-details.component";
import { ViewEventsComponent } from "./events/view-events/view-events.component";
import { EventDetailComponent } from "./events/event-detail/event-detail.component";
import { EventClubDetailComponent } from "./events/event-club-detail/event-club-detail.component";
import { EnquiryConversationComponent } from "./enquiry-conversation/enquiry-conversation.component";
import { ViewCoursesComponent } from "./course-bidding/view-courses/view-courses.component";
import { CourseDetailComponent } from "./course-bidding/course-detail/course-detail.component";
import { BidDetailComponent } from "./course-bidding/bid-detail/bid-detail.component";
import { EditBidComponent } from "./course-bidding/edit-bid/edit-bid.component";
import { LeaveApplicationFormComponent } from "./leave-application-form/leave-application-form.component";
import { LeaveListComponent } from "./leave-list/leave-list.component";
import { EventListComponent } from "./studentEvent/event-list/event-list.component";
import { EventPreviewComponent } from "./studentEvent/event-preview/event-preview.component";
import { CreateClubComponent } from "./studentClub/create-club/create-club.component";
import { ViewClubComponent } from "./studentClub/view-club/view-club.component";
import { ClubStatusComponent } from "./studentClub/club-status/club-status.component";
import { EditClubComponent } from "./studentClub/edit-club/edit-club.component";
import { ClubDetailComponent } from "./studentClub/club-detail/club-detail.component";
import { EventStatusComponent } from "./studentEvent/event-status/event-status.component";
import { EventDetailsComponent } from "./studentEvent/event-details/event-details.component";

import { CreateEventComponent } from "./studentEvent/create-event/create-event.component";
import { SpecificLeaveComponent } from "./specific-leave/specific-leave.component";
import { CourseListComponent } from "./course-list/course-list.component";
import { CoursePreferenceComponent } from "./course-preference/course-preference.component";
import { CoursePreferenceDetailsComponent } from "./course-preference-details/course-preference-details.component";
import { TermWiseMarksComponent } from "./course-assingment/term-wise-marks/term-wise-marks.component";
import { CourseWiseMarksComponent } from "./course-assingment/course-wise-marks/course-wise-marks.component";
import { DownloadTranscriptComponent } from "./download-transcript/download-transcript.component";
import { TranscriptDetailsComponent } from "./transcript-details/transcript-details.component";
import { NewCourseListComponent } from "./new-course-list/new-course-list.component";
import { NewMyCourseComponent } from "./new-my-course/new-my-course.component";
import { NewBiddingListComponent } from "./new-bidding-list/new-bidding-list.component";
import { NewCourseDetailsComponent } from "./new-course-details/new-course-details.component";
import { NewDashboardComponent } from "./new-dashboard/new-dashboard.component";
import { NewElectiveBiddingComponent } from "./new-elective-bidding/new-elective-bidding.component";
import { StudentAttendaceComponent } from "./student-attendace/student-attendace.component";
import { CoacheeSessionComponent } from "./coachee-session/coachee-session.component";
const routes: Routes = [
  { path: "", component: StudentPortalComponent },
  {
    path: "student-portal",
    component: StudentPortalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "transactionHistory",
    component: TransactionHistoryComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "enquiry",
    component: EnquiryPortalComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'account',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "feeSlip/:id",
    component: FeeSlipComponent,
    canActivate: [AuthGuard],
  },
  // { path: 'inbox', component: InboxComponent, canActivate: [AuthGuard] },
  {
    path: "paymentStatus/:orderId/:studentFeeDueId",
    component: PaymentStatusComponent,
    canActivate: [AuthGuard],
  },
  // {
  //   path: "resume/:fname/:lname",
  //   component: StudentResumeComponent,
  //   canActivate: [AuthGuard],
  // },
  // { path: 'resume/:slug', component: StudentResumeComponent, canActivate: [AuthGuard] },
  {
    path: "profile",
    component: StudentResumeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "refund-policy",
    component: RefundPolicyComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "mentor",
    component: AssignMentorsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "rateMentor/:id/:mentorName",
    component: MentorRatingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "showRating/:id/:mentorName",
    component: ShowRatingComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "survey-list",
    component: SurveyListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "survey-detail/:surveyId",
    component: SurveyDetailComponent,
    canActivate: [AuthGuard],
  },
  { path: "events", component: ViewEventsComponent, canActivate: [AuthGuard] },
  {
    path: "event-detail/:eventId",
    component: EventDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "event-club-detail/:eventId",
    component: EventClubDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "cashFreeBillingDetails",
    component: CashfreeDetailsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "refundPolicy",
    component: RefundPolicyComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "enquiryConversation/:enqId/:orgStuId",
    component: EnquiryConversationComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "view-courses",
    component: ViewCoursesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "course-detail/:courseId",
    component: CourseDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "bid-detail/:bidId/:courseId",
    component: BidDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "edit-bid/:bidId/:courseId",
    component: EditBidComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "leave-application-form",
    component: LeaveApplicationFormComponent,
    canActivate: [AuthGuard],
  }, {
    path: "leaveList",
    component: LeaveListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "create-club",
    component: CreateClubComponent,
    canActivate: [AuthGuard],
  }
  ,
  {
    path: "event-list",
    component: EventListComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "event-preview",
    component: EventPreviewComponent,
    canActivate: [AuthGuard],
  }
  ,
  {
    path: "view-club",
    component: ViewClubComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "club-status/:clubId",
    component: ClubStatusComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "edit-club",
    component: EditClubComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "club-detail/:clubId",
    component: ClubDetailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: "event-status/:eventId",
    component: EventStatusComponent ,
    canActivate: [AuthGuard],
  },
  {
    path: "event-details/:eventId",
    component: EventDetailsComponent ,
    canActivate: [AuthGuard],
  },
  {
    path: 'createEvent/:clubId',
    component: CreateEventComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "courseList",
    component:CourseListComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'coursePreference/:id',
    component:CoursePreferenceComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'coursePreferencePreview/:id/:studentId',
    component:CoursePreferenceDetailsComponent,
    canActivate:[AuthGuard]
  },
  {
    path : "specificLeave/:id", 
    component: SpecificLeaveComponent, 
    canActivate: [AuthGuard]
  },
  {
    path: "termWiseAssingmentMarks",
    component: TermWiseMarksComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "courseWiseAssingmentMarks/:id",
    component: CourseWiseMarksComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "downloadTransript/:term/:batch/:outClass",
    component: DownloadTranscriptComponent,
    canActivate: [AuthGuard]
  },
  {
    path: "transcriptDetails/:term/:batch/:studentId/:outClass",
    component: TranscriptDetailsComponent,
  },
  {
    path: "newCourseList",
    component:NewCourseListComponent,
    canActivate:[AuthGuard]
  },
  {
    path: "newMyCourse",
    component:NewMyCourseComponent,
    canActivate:[AuthGuard]
  },
  {
    path: "newBiddingList",
    component:NewBiddingListComponent,
    canActivate:[AuthGuard]
  },
  {
    path: "newCourseDetails/:courseId",
    component:NewCourseDetailsComponent,
    canActivate:[AuthGuard]
  },
  {
    path: "dashboard",
    component:NewDashboardComponent,
    canActivate:[AuthGuard]
  },
  {
    path: "electiveBidding",
    component:NewElectiveBiddingComponent,
    canActivate:[AuthGuard]
  },
  {
    path: "studentAttendance",
    component:StudentAttendaceComponent,
    canActivate:[AuthGuard]
  },
  {
    path: "coacheeSession",
    component:CoacheeSessionComponent,
    canActivate:[AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentRoutingModule { }
