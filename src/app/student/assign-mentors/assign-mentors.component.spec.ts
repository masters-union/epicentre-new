import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignMentorsComponent } from './assign-mentors.component';

describe('AssignMentorsComponent', () => {
  let component: AssignMentorsComponent;
  let fixture: ComponentFixture<AssignMentorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignMentorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignMentorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
