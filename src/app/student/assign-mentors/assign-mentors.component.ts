import { Component, OnInit } from '@angular/core';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';
declare var $;
declare var swal;
@Component({
  selector: 'app-assign-mentors',
  templateUrl: './assign-mentors.component.html',
  styleUrls: ['./assign-mentors.component.css'],
  providers: [LoadscriptsService]
})
export class AssignMentorsComponent implements OnInit {

  organizationId: any;
  loading: any
  searchParam: String = "";
  pageNo: number = 1;
  pageSize: any = 50;
  pageCount: number = 1;
  isCrossEnable: boolean;
  studentId: string;
  mentorList = [];
  constructor(
    private studentService: StudentService,
    private cs: CookieServiceProvider,
    private loadscriptsService: LoadscriptsService) { }

  ngOnInit() {
    window.console.log = function () { };
    this.organizationId = this.cs.getItem("Organization id");

    this.studentId = this.cs.getItem('id')
    this.getAllAssignedStu()
  }

  getAllAssignedStu() {
    this.loading = true
    this.studentService.getAllAssignedStudent(this.organizationId, this.studentId, this.searchParam, this.pageNo, this.pageSize).subscribe((res: any) => {
      if (res.IsSuccess) {
        setTimeout(() => {
          this.loadscriptsService.loadStuff()
        }, 100);
        //console.log("getAllAssignedStudent...... ", res)
        this.mentorList = res.Data
        this.pageCount = Math.ceil(res.Count / this.pageSize);
        this.loading = false
      }
    })
  }



  search() {
    this.pageSize = $('#pageSize').val()
    this.getAllAssignedStu()
  }
  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
  }




  resetsearch() {
    this.searchParam = "";
    this.search();
    this.isCrossEnable = false;

  }

  previous() {
    this.pageNo = this.pageNo - 1;
    this.search()
    this.setPage()


  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.search()
    this.setPage()
  }

  nextMessage(pageNo) {
    this.pageNo = pageNo;
    this.search();
    this.setPage()
  }

  setPage() {

    $('.pitem').removeClass('active');
    $(`.pitem:eq(${(this.pageNo - 1)})`).addClass('active');
    if (this.pageNo == 1 && this.pageCount == 1) {

      $('.previous').addClass('disabled');
      $('.next').addClass('disabled');

    }
    if (this.pageNo == 1 && this.pageCount > 1) {
      $('.previous').addClass('disabled');
      $('.next').removeClass('disabled');

    }
    if (this.pageNo > 1 && this.pageCount > 1) {
      $('.previous').removeClass('disabled');
      $('.next').removeClass('disabled');

    }
    if (this.pageNo > 1 && this.pageNo == this.pageCount) {

      $('.previous').removeClass('disabled');
      $('.next').addClass('disabled');

    }
  }
  arrayOne(n: number): any[] {
    if (n == undefined)
      n = 0
    n = Math.ceil(n)
    if (n != undefined)
      return Array(n);
  }




}
