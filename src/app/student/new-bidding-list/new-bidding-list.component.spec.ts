import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewBiddingListComponent } from './new-bidding-list.component';

describe('NewBiddingListComponent', () => {
  let component: NewBiddingListComponent;
  let fixture: ComponentFixture<NewBiddingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewBiddingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewBiddingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
