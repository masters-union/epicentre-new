import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { StudentService } from '../service/student.service';
declare var $: any;
@Component({
  selector: 'app-new-bidding-list',
  templateUrl: './new-bidding-list.component.html',
  styleUrls: ['./new-bidding-list.component.css'],
  providers: [LoadscriptsService],

})
export class NewBiddingListComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  studentId: string;
  organizationId: string;
  loading: boolean = false;
  courses = [];
  finalCourses =[]
  programId: string;
  isShow:boolean= false;
  studentDetails:any;
  Data:any;
  noOfCourses:number=0;
  pendingPoints:number=100;
  alreadyBidded:boolean = false;
  searchParam: string ="1";
  biddingLength: number = 0;
  minElectiveBidCourses:number = 0;
  checkTotal:number = 0;
  bid:number = 0;
  bids=[]
  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.programId = this.cs.getItem("programBatchId");
    this.getListOfElectiveCourses()
  }
  // onTermSelect() {
  //   this.searchParam = $("#term").val();
  //   this.getListOfElectiveCourses();
  //   console.log("search Param",this.searchParam)
  // }
  biddingValue(value,i,courseId){
    let obj = {
      studentId:this.studentId,
      programId:this.programId,
      courseId:courseId,
      value:value
    }
    // this.bid = this.finalCourses.length;
    this.finalCourses.splice(i, 1, obj);
    this.biddingLength += 1;
    // if(value== null || value =="" || value == undefined){
    //   value = 0;
    // }else{
    //   this.pendingPoints -=parseInt(value);
    //   console.log(this.pendingPoints) 
    // }
    }
  getListOfElectiveCourses() {
    this.loading = true;
    this._ss.getListOfElectiveCourses(this.studentId,this.programId).subscribe((data: any) => {
      if (data.IsSuccess) {
        
        this.loading = false;
        this.minElectiveBidCourses = data.termDetail.TermDetails[0].minElectiveCourse;
        // this.pendingPoints = 100;
        this.courses = []
        this.courses = data.Data;
        this.bids = data.bidValues
        // this.courses.forEach((ele)=>{
          // if(ele.isCourseBidSelected==false){
          //   console.log(this.noOfCourses)
          //   this.noOfCourses +=1;
          // }
          this.noOfCourses = this.courses.length; 


          this.courses.forEach((ele,i)=>{
            // let index = bids.findIndex((x)=>{
            //   x.courseId==ele.id && x.studentId ==req.params.studentId
            // })
            this.courses[i].StudentBids =[];
            // console.log("before",this.courses[i].StudentBids)
            this.bids.forEach((x)=>{
              if(x.courseId==ele.id && x.studentId == this.studentId){
                this.courses[i].StudentBids.push(x);
              }
            })
            this.pendingPoints -=this.courses[i].StudentBids[0].bidPoint;
            // console.log("after",this.courses[i].StudentBids[0].bidPoint)      
          })
          // let bidArr = [];
          // ele.StudentBids.forEach((bid)=>{
          //   if(this.studentId == bid.studentId && ele.id == bid.courseId){
          //     bidArr.push(bid);
          //   }
          //   ele.StudentBids= bidArr;
          //   console.log("adsadasdsasd",ele.StudentBids)           
          // })
          // })

        this.courses.forEach((ele)=>{
          if(ele.StudentBids.length>0 &&  this.alreadyBidded==false){
            this.alreadyBidded = true;
          }
          // if(ele.StudentBids.length>0 && ele.isCourseBidSelected==false)
          // this.pendingPoints -= ele.StudentBids[0].bidPoint;
          // this.biddingLength +=1;
        })
        // this.courses.map((ele)=>{

        // })
        // console.log(this.courses);
        // console.log("already bid",this.alreadyBidded)
        if(this.alreadyBidded){
          // console.log("i'm here");
          this.courses.forEach((ele)=>{
          // if(ele.StudentBids.length>0 && ele.isCourseBidSelected==false){
          //  console.log(ele.StudentBids[0].bidPoint);
          //  this.pendingPoints -= ele.StudentBids[0].bidPoint;

          // }
          // if(ele.StudentBids.length>0){
          //   this.bid +=1;
          // }
            if (this.bids.length > 0) {
              this.bid = this.bids.length;
            }
          })
 
        }
       }else{
        this.loading = false;
        console.log("Error Occured, Please Check")
      }
    });
  }

  checkIsTotalEqualToHundred(){
    // this.checkTotal = 0;
    this.pendingPoints = 100;
    this.finalCourses.forEach((ele)=>{
      // this.checkTotal += ele.value;
      this.pendingPoints -=parseInt(ele.value);

    })
    if(this.pendingPoints!=0){
      this.ts.pop("error", "Sum of point is less than 100");
    }else{
      if(this.finalCourses.length>0 && this.finalCourses.length>=this.minElectiveBidCourses && this.pendingPoints==0){
        this.submitCourseBiddingValues()
      }
    }
  }
  submitCourseBiddingValues(){

    // if(this.checkTotal<100){
    //   this.ts.pop("error", "Sum of point is less than 100");
    //   return;
    // }else{
      // if(this.finalCourses.length>0 && this.finalCourses.length>=this.minElectiveBidCourses){
        this.loading = true;
        this._ss.submitCourseBiddingValues(this.studentId,this.finalCourses).subscribe((data: any) => {
          if (data.IsSuccess) {
            this.loading = false;
            // console.log(data.Data);
            this.ts.pop("success", "Preference Saved Successfully");
            setTimeout(()=>{
              this.router.navigate(['student/dashboard']);
            },1000)
           }else{
            this.loading = false;
            console.log("Error Occured, Please Check")
          }
        });
      // }
    // }


  }
}
