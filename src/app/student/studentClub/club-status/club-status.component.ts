import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, Form } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
import { client } from 'src/app/common/service/config';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import * as moment from 'moment';
declare var $: any;
@Component({
  selector: 'app-club-status',
  templateUrl: './club-status.component.html',
  styleUrls: ['./club-status.component.css'],
  providers: [LoadscriptsService]
})
export class ClubStatusComponent implements OnInit {
  student_id: string;
  organizationId: string;
  loading: boolean;
  clubId: any;
  clubData: any;
  pendingClubData: any;
  approvedClubData: any;
  rejectedClubData: any;
  clubComments: any = [];
  status: any = "";
  clubForm: FormGroup;
  formD: FormData;
  formDA: FormData;
  file: File;
  fileName: any;
  fileA: any;
  responseText: any = "";
  clubGuidingDocument: any ="";

  studentData = [];
  searchParam: String = "";
  programs: String = "";
  sortBy: any = "officialEmail";
  recordCount: number = 0;
  allCount: number = 0;
  selectAll: boolean = false;
  checkAll: boolean = false;
  pageNo: number = 1;
  pageSize: any = 50;
  pageCount: number = 1;
  paginationSearch: boolean = false;
  isCrossEnable: boolean;
  allStudentData: any = [];
  programNames: any = [];
  batchShow: boolean = false;
  proName: any;
  orgProgramsData = [];
  selectedStudentList = [];
  rejectedReason: any = "";
  clubType: any = "";


  constructor(
    private _ss: StudentService, private cs: CookieServiceProvider,private ts: ToasterService, 
    private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private loadscriptsService: LoadscriptsService
  ) { }

  ngOnInit() {
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    this.route.params.subscribe((params: Params) =>{
      this.clubId = params['clubId']
    });
    this.formDA = new FormData();
    this.clubInitForm();
    this.getClubDataById();
    this.getClubComments();
    this.getProgramName();
    this.fetchDetails();
  }

  getClubDataById(){
    // this.loading = true;
    this._ss.getUnpublishedClubById(this.organizationId, this.student_id, this.clubId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.clubData = res.Data;
        this.status = this.clubData.clubStatus
        // if(this.status == "pending"){
        //   this.pendingClubData = res.Data;
        // }
        // else if(this.status == "approved"){
        //   this.approvedClubData = res.Data;
        // }
        // else if(this.status == "rejected"){
        //   this.rejectedClubData = res.Data;
        // }
        this.clubType = this.clubData.clubType
        this.rejectedReason = this.clubData.rejectedReason
        this.clubGuidingDocument = this.clubData.clubGuidedDoc
        this.clubForm.controls.id.patchValue(this.clubData.id);
        this.clubForm.controls.name.patchValue(this.clubData.name);
        this.clubForm.controls.description.patchValue(this.clubData.description);
        this.clubForm.controls.mission.patchValue(this.clubData.mission);
        this.clubForm.controls.vision.patchValue(this.clubData.vision);
        this.clubForm.controls.clubCoverImage.patchValue(this.clubData.clubCoverImage);
        this.clubForm.controls.clubProfileImage.patchValue(this.clubData.clubProfileImage);
        this.clubForm.controls.clubGuidedDoc.patchValue(this.clubData.clubGuidedDoc);
        this.clubForm.controls.clubType.patchValue(this.clubData.clubType);
      }
      else {
        this.loading = false;
      }
    })
  }

  getClubComments(){
    // this.loading = true;
    this._ss.getClubComments(this.organizationId, this.student_id, this.clubId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let data = res.Data;
        data.forEach((e)=>{
          e.createdAt = new Date(e.createdAt);
        })
        data.sort((a,b)=>a.createdAt - b.createdAt);
        this.clubComments = data;
        // console.error(this.clubComments)
      }
      else {
        this.loading = false;
      }
    })
  }
  
  clubInitForm() {
    this.clubForm = this.fb.group({
      id: ["", Validators.required],
      name: ["", Validators.required],
      description: ["", [Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      mission: ["", [Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      vision: ["", [Validators.required,Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      clubType: ["", Validators.required],
      clubCoverImage: "",
      clubProfileImage: "",
      clubGuidedDoc: "",
      students: this.fb.array([this.student, Validators.required]),
      studentIdsData: this.fb.array([])
    })
  }
  get student(): FormGroup {
    return this.fb.group({
      studentId: [""],
    });
  }

  onUploadFile(event) {
    if (event.target.files) {
        const file: File = event.target.files[0];
        this.file = file
        this.fileName = file.name;
        $('.filenm').text(file.name);
        
    }
  }

  changeClubType(value) {
    this.clubForm.controls.clubType.patchValue(value);
  }

  editClubRequest() {
    this.formD = new FormData();
    if(this.fileName){
      this.formD.append('file', this.file, this.fileName);
    }
    this.clubForm.value.students = this.setStudentsId(this.allStudentData);
    this.formD.append('id', this.clubForm.value.id);
    this.formD.append('name', this.clubForm.value.name);
    this.formD.append('description', this.clubForm.value.description);
    this.formD.append('mission', this.clubForm.value.mission);
    this.formD.append('vision', this.clubForm.value.vision);
    this.formD.append('students', JSON.stringify(this.clubForm.value.students))
    this.formD.append('clubCoverImage', this.clubForm.value.clubCoverImage);
    this.formD.append('clubProfileImage', this.clubForm.value.clubProfileImage);
    this.formD.append('clubGuidedDoc', this.clubForm.value.clubGuidedDoc);
    this.formD.append('clubType', this.clubForm.value.clubType);

    // console.error(this.formD)
    if(this.clubForm.controls.name.value.trim().length>0){
      if (this.clubForm.valid && this.clubForm.value.students.length > 0) {
        if (this.clubForm.value.students.length >= 1) {
          this.loading = true;
          this._ss.editClubRequest(this.organizationId, this.student_id, this.formD).subscribe((res: any) => {
            if (res.IsSuccess) {
              this.loading = false;
              this.ts.pop("success", "", "Changes updated")
              this.router.navigate(['/student/view-club'])
            }
            else {
              this.loading = false;
            }
          })
        }else {
          this.ts.pop("error", "", "Please select at least 1 student")
        }
      }else {
        this.ts.pop("error", "", "Please fill in the required fields")
      }
    }else {
      this.ts.pop("error", "", "Club name is empty")
    }
    
    
    
  }



  onUploadGuidingDoc(event, str) {
    if (event.target.files) {
        if (event.target.files) {
          const file: File = event.target.files[0];

          $('.fileA').text(file.name);
        
          // let appendFileName = str;
          // this.formDA.append(appendFileName, file, appendFileName);
          this.formDA.append('file', file, file.name)
          this.clubForm.patchValue({
            clubGuidedDoc: file.name
          })   
      }    
    }
  }

  onUploadCoverPic(event, str) {
    // const files = event.target.files;
    // let message;
    // if (files.length === 0)
    //     return;

    // const mimeType = files[0].type;
    // if (mimeType.match(/image\/*/) == null) {
    //     message = "Only images are supported.";
    //     this.ts.pop("error",message)
    //     return;
    // }
    // else{
    //   const reader = new FileReader();
    //   reader.readAsDataURL(files[0]);
    //   reader.onload = (_event) => {
    //       this.clubForm.patchValue({
    //         clubCoverImage: reader.result
    //       })
    //   }
    //   const file: File = event.target.files[0];
    //   // this.fileName = file.name;
    //   // this.formDA.append(str, file, str)
    // }

    const options = {
      accept: ["image/*"],
      storeTo: {
        location: "s3",
        path: "/eventClub/",
        region: "ap-south-1",
        access: "public"
      },
      onFileSelected(file) { 
          // if (file.size > 10485760) {
          //     alert('Video size is too big, select something smaller than 10 MB');
          // }
      }, 
      // maxSize: 10485760,
      onFileUploadFinished: file => {
        this.clubForm.patchValue({
        clubCoverImage: file.url
        })
        
      }
    };
    client.picker(options).open();


  }

  onUploadProfilePic(event, str) {
    // const files = event.target.files;
    // let message;
    // if (files.length === 0)
    //     return;

    // const mimeType = files[0].type;
    // if (mimeType.match(/image\/*/) == null) {
    //     message = "Only images are supported.";
    //     this.ts.pop("error",message)
    //     return;
    // }
    // else{
    //   const reader = new FileReader();
    //   reader.readAsDataURL(files[0]);
    //   reader.onload = (_event) => {
    //       this.clubForm.patchValue({
    //         clubProfileImage: reader.result
    //       })
    //   }
    //   const file: File = event.target.files[0];
    //   // this.fileName = file.name;
    //   // this.formDA.append(str, file, str)
    // }

    const options = {
      accept: ["image/*"],
      storeTo: {
        location: "s3",
        path: "/eventClub/",
        region: "ap-south-1",
        access: "public"
      },
      onFileSelected(file) { 
          if (file.mimetype != Image) {
              alert('Only image is supported.');
          }
      }, 
      // maxSize: 10485760,
      onFileUploadFinished: file => {
        this.clubForm.patchValue({
          clubProfileImage: file.url
        })
        
      }
    };
    client.picker(options).open();

  }


  editApprovedClubRequest() {
    this.clubForm.value.students = this.setStudentsId(this.allStudentData);
    this.formDA.append('id', this.clubForm.value.id);
    this.formDA.append('name', this.clubForm.value.name);
    this.formDA.append('description', this.clubForm.value.description);
    this.formDA.append('mission', this.clubForm.value.mission);
    this.formDA.append('vision', this.clubForm.value.vision);
    this.formDA.append('clubCoverImage', this.clubForm.value.clubCoverImage);
    this.formDA.append('clubProfileImage', this.clubForm.value.clubProfileImage);
    this.formDA.append('clubGuidedDoc', this.clubForm.value.clubGuidedDoc);
    this.formDA.append('students', JSON.stringify(this.clubForm.value.students));
    this.formDA.append('clubType', this.clubForm.value.clubType);
    // console.error(this.formDA)
    if(this.clubForm.controls.name.value.trim().length>0){
      if (this.clubForm.valid && this.clubForm.value.students.length > 0){
        if (this.clubForm.value.students.length >= 1){
          this.loading = true;
          this._ss.editApprovedClubRequest(this.organizationId, this.student_id, this.formDA).subscribe((res: any) => {
            if (res.IsSuccess) {
              this.loading = false;
              this.ts.pop("success", "", "Request sent")
              this.router.navigate(['/student/view-club'])
              
            }
            else {
              this.loading = false;
            }
          })
        }else {
          this.ts.pop("error", "", "Please select at least 1 student")
        }
        
      }else {
        this.ts.pop("error", "", "Please fill in the required fields")
      }
    }else {
      this.ts.pop("error", "", "Club name is empty")
    }
    
    
    
  }

  setStudentsId(cur: any[]): FormArray {
    const formArray = new FormArray([]);
    cur.forEach(s => {
      let c = this.student
      c.patchValue({
        studentId: s
      })
      formArray.push(c)
    });
    return formArray.value;
  }


  getProgramName() {
    this._ss.getProgramMaster(this.organizationId, this.student_id)
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          this.programNames = res.Data;
          setTimeout(() => {
            this.loadscriptsService.loadStuff();
          }, 100);
          //console.log("res program name", this.programNames);
        } else {
          //console.log("error ", res);
        }
      });
  }

  fetchPrograms() {
    this.loading = true;
    this._ss.getProgramsByMasterID(this.organizationId, this.student_id, this.proName)
      .subscribe((res: any) => {
        //console.log("res ", res)
        if (res.IsSuccess) {
          setTimeout(() => {
            this.loadscriptsService.loadStuff();
          }, 100);
          this.loading = false;
          this.orgProgramsData = res.Data;
          //console.log("orgProgramsData ", this.orgProgramsData);
        } else {
          //console.log("error ", res);
          this.loading = false;
        }
      });
  }

  fetchDetails() {
    this.loading = true;
    // if (this.adminId != null) {
    this._ss.getAllStudentOrgList(
      this.organizationId,
      this.student_id,
      this.programs,
      this.proName,
      this.searchParam,
      this.pageNo,
      this.pageSize,
      this.sortBy
    )
      .subscribe(
        (res: any) => {
          if (res.IsSuccess) {
            this.loading = false;
            this.studentData = res.Data;

            console.log("daya", this.studentData);

            this.pageCount = Math.ceil(res.count / this.pageSize);
            this.recordCount = this.studentData.length;
            this.allCount = res.count;
            setTimeout(() => {
              this.loadscriptsService.loadStuff();
            }, 100);
            this.setPage();
            if (this.allStudentData.length > 0) {
              this.studentData.forEach((el) => {
                if (this.allStudentData.indexOf(el.id) > -1) {
                  setTimeout(() => {
                    $(`#${el.id}`).prop("checked", true);
                  }, 100);
                }
                else {
                  this.checkAll = false;
                  setTimeout(() => {
                    $(`#${el.id}`).prop("checked", false);
                  }, 100);
                }
              });
              // this.flagFunction()
            }
            else {
              this.studentData.forEach((el) => {
                setTimeout(() => {
                  $(`#${el.id}`).prop("checked", false);
                }, 100);
              });
            }
            this.viewSpecificClubsStudents()
          } else {
            //console.log("error ", res);
          }
        },
        (error) => {
          this.loading = false;
          this.ts.pop("error", "", error.error.respObj.Message);
        }
      );
  }

  fetchDetailsAfterDataBind() {
    this.loading = true;
    // if (this.adminId != null) {
    this._ss.getAllStudentOrgList(
      this.organizationId,
      this.student_id,
      this.programs,
      this.proName,
      this.searchParam,
      this.pageNo,
      this.pageSize,
      this.sortBy
    )
      .subscribe(
        (res: any) => {
          if (res.IsSuccess) {
            this.loading = false;
            this.studentData = res.Data;

            console.log("daya", this.studentData);

            this.pageCount = Math.ceil(res.count / this.pageSize);
            this.recordCount = this.studentData.length;
            this.allCount = res.count;
            setTimeout(() => {
              this.loadscriptsService.loadStuff();
            }, 100);
            this.setPage();
            if (this.allStudentData.length > 0) {
              this.studentData.forEach((el) => {
                if (this.allStudentData.indexOf(el.id) > -1) {
                  setTimeout(() => {
                    $(`#${el.id}`).prop("checked", true);
                  }, 100);
                }
                else {
                  this.checkAll = false;
                  setTimeout(() => {
                    $(`#${el.id}`).prop("checked", false);
                  }, 100);
                }
              });
              this.flagFunction()
            }
            else {
              this.studentData.forEach((el) => {
                setTimeout(() => {
                  $(`#${el.id}`).prop("checked", false);
                }, 100);
              });
            }
            // this.viewSpecificClubsStudents()
          } else {
            //console.log("error ", res);
          }
        },
        (error) => {
          this.loading = false;
          this.ts.pop("error", "", error.error.respObj.Message);
        }
      );
  }

  viewSpecificClubsStudents() {
    // this.loading = true;
    this._ss.getAllClubIntersetedStu(this.organizationId, this.student_id, this.clubId).subscribe((data: any) => {
        if (data.IsSuccess) {
          this.selectedStudentList = data.Data;
          if (this.selectedStudentList.length == this.allCount) {
            setTimeout(() => {
              // $(`#zero`).prop("checked", true);
              // this.checkAll = !this.checkAll;
            }, 100);
          }
          // this.selectedStudentList.forEach((el) => {
          //   setTimeout(() => {
          //     $(`#${el.Student.id}`).prop("checked", true);
          //   }, 100);
          // });

          this.loading = false;

          setTimeout(() => {
            // this.fetchSelectedStudent();
          }, 2000);
          this.selectedStudentList.forEach(el => {
            if (this.allStudentData.indexOf(el.Student.id) === -1) {
              this.allStudentData.push(el.Student.id)
            }
          });
          if (this.allStudentData.length > 0) {
            this.studentData.forEach((el) => {
              if (this.allStudentData.indexOf(el.id) > -1) {
                setTimeout(() => {
                  $(`#${el.id}`).prop("checked", true);
                }, 100);
              }
            });
          }
          this.flagFunction()
        }
      });
  }


  flagFunction() {
    let flag = true;
    this.studentData.forEach((el) => {
      if (this.allStudentData.indexOf(el.id) == -1) {
        console.log(this.allStudentData)
        flag = false;
      }
    });
    $('#zero').prop('checked', false);
    if (flag) {
      setTimeout(() => {
        $('#zero').prop('checked', true);
      }, 100);
      this.checkAll = true;
    }
  }

  selectedAll() {
    this.selectAll = true;
  }
  unselectedAll() {
    this.selectAll = false;
  }

  isSelected(id) {
    // if (this.studentIds.indexOf(id) > -1) {
    //   return true;
    // } else {
    //   return false;
    // }
    if (this.allStudentData.indexOf(id) > -1) {
      return true;
    } else {
      return false;
    }
  }


  searchStudent(pageNo) {
    this.pageNo = pageNo;
    this.search();
    this.setPage();
  }

  previous() {
    this.pageNo = this.pageNo - 1;
    this.search();
    this.setPage();
  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.search();
    this.setPage();
  }

  setPage() {
    $(".pitem").removeClass("active");
    $(`.pitem:eq(${this.pageNo - 1})`).addClass("active");
    if (this.pageNo == 1 && this.pageCount == 1) {
      $(".previous").addClass("disabled");
      $(".next").addClass("disabled");
    }
    if (this.pageNo == 1 && this.pageCount > 1) {
      $(".previous").addClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageCount > 1) {
      $(".previous").removeClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageNo == this.pageCount) {
      $(".previous").removeClass("disabled");
      $(".next").addClass("disabled");
    }
  }

  arrayOne(n: number): any[] {
    if (n == undefined) n = 0;
    n = Math.ceil(n);
    if (n != undefined) return Array(n);
  }


  checkClicked() {
    this.checkAll = !this.checkAll;
    this.studentData.forEach((stu) => {
      // this.innercheckClicked(stu.id);
      if (this.checkAll) {
        if (this.allStudentData.indexOf(stu.id) === -1) {
          this.allStudentData.push(stu.id);
        }
      }
      else {

        if (this.allStudentData.indexOf(stu.id) === -1) {
          this.allStudentData.push(stu.id);
        } else {
          this.allStudentData.splice(this.allStudentData.indexOf(stu.id), 1);
        }
      }
    });
    this.selectAll = false;
  }

  innercheckClicked(id) {
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);

    if (this.allStudentData.indexOf(id) === -1) {
      this.allStudentData.push(id);
    } else {
      this.allStudentData.splice(this.allStudentData.indexOf(id), 1);
      this.checkAll = false;
      $(`#zero`).prop("checked", false);
    }
    this.allStudentData.forEach((el) => {
      setTimeout(() => {
        $(`#${el}`).prop("checked", true);
      }, 100);
    });
    console.log(this.allStudentData);
    setTimeout(() => {
      $("#selectOption").selectpicker("refresh");
    }, 100);
  }

  programNameSearch() {
    this.pageNo = 1;
    if ($("#proName").val() == "All") {
      this.batchShow = true;
      this.proName = undefined;
      this.selectAll = false;
      this.checkAll = false;
      // this.clubDetailsForm.patchValue({ programMasterId: "" });
      // this.clubDetailsForm.patchValue({ programId: "" });
      console.log(this.proName);
      this.programs = undefined;
      this.fetchDetailsAfterDataBind();
      this.fetchPrograms();
    } else {
      this.batchShow = true;
      this.proName = $("#proName").val();
      this.selectAll = false;
      this.checkAll = false;
      // this.clubDetailsForm.patchValue({ programMasterId: this.proName });
      // this.clubDetailsForm.patchValue({ programId: "" });
      console.log(this.proName);
      this.programs = undefined;
      this.fetchDetailsAfterDataBind();
      this.fetchPrograms();
    }
  }

  programsSearch() {
    this.pageNo = 1;
    if ($("#programs").val() == "All") {
      this.programs = undefined;
      this.checkAll = false;
      this.selectAll = false;
      $("#zero").prop("checked", false);
      // this.clubDetailsForm.patchValue({ programId: "" });
      console.log(this.programs);
      this.fetchDetailsAfterDataBind();
    } else {
      this.programs = $("#programs").val();
      this.checkAll = false;
      this.selectAll = false;
      $("#zero").prop("checked", false);
      // this.clubDetailsForm.patchValue({ programId: this.programs });
      console.log(this.programs);
      this.fetchDetailsAfterDataBind();
    }
  }

  search() {
    // console.log("m here")
    this.sortBy = $("#sortBy").val();
    this.pageSize = $("#pageSize").val();
    this.fetchDetailsAfterDataBind();
  }

  resetsearch() {
    this.pageNo = 1;
    this.searchParam = "";
    this.search();
    this.isCrossEnable = false;
  }

  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
    // console.log("yes typing...........", value)
  }

  searchSpecific() {
    // console.log("m here")
    this.pageNo = 1;
    this.sortBy = $("#sortBy").val();
    this.pageSize = $("#pageSize").val();
    this.fetchDetailsAfterDataBind();
  }

  sendMessage() {
    let obj = {
      clubId: this.clubId,
      studentId: this.student_id,
      userResponseId: null,
      userComment: null,
      userCommentDate: null,
      studentComment: this.responseText,
      studentCommentDate:moment(),
    }
    if(this.responseText.trim().length>0){
      this.loading = true;
      this._ss.createClubComment(this.organizationId, this.student_id, obj).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          this.responseText = "";
          this.ts.pop("success", "", "Sent")
          this.getClubComments();
          
        }
        else {
          this.loading = false;
        }
      })
    }
    else {
      this.ts.pop("warning", "", "Please write something")
    }
    
  }

  goBack(){
    this.router.navigate(["/student/view-club"]);
  }

}
