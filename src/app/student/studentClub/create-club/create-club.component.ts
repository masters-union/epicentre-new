import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, Form } from '@angular/forms';
import { Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
// import { AuthService } from 'angularx-social-login';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
declare var $: any;
@Component({
  selector: 'app-create-club',
  templateUrl: './create-club.component.html',
  styleUrls: ['./create-club.component.css'],
  providers: [LoadscriptsService]
})
export class CreateClubComponent implements OnInit {
  student_id: string;
  organizationId: string;
  loading: boolean;
  // programId: any;
  // programMasterId: any;
  // studentsListData: any;
  // programMasterData: any;
  // programsData: any;
  clubForm: FormGroup;
  studentIdData = [];
  formD: FormData;
  guidDoc: File;
  fileName: string;

  studentData = [];
  searchParam: String = "";
  programs: String = "";
  sortBy: any = "firstName";
  recordCount: number = 0;
  allCount: number = 0;
  selectAll: boolean = false;
  checkAll: boolean = false;
  pageNo: number = 1;
  pageSize: any = 50;
  pageCount: number = 1;
  paginationSearch: boolean = false;
  isCrossEnable: boolean;
  allStudentData: any = [];
  programNames: any = [];
  batchShow: boolean = false;
  proName: any;
  orgProgramsData = [];

  constructor(private _ss: StudentService, private cs: CookieServiceProvider, private fb: FormBuilder,
    private ts: ToasterService, private router: Router, private loadscriptsService: LoadscriptsService) { }

  ngOnInit() {
    // window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    this.programInitForm();
    // this.getStudentDetails();
    // this.getProgramMaster();
    this.getProgramName();
    this.fetchDetails();
    console.log("init console######################");
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);
    this.allStudentData[0] = this.student_id
  }

  programInitForm() {
    this.clubForm = this.fb.group({
      name: ["", Validators.required],
      description: ["", [Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      mission: ["", [Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      vision: ["", [Validators.required, Validators.pattern(".*\\S.*[a-zA-z0-9 ]")]],
      students: this.fb.array([this.student]),
      clubType: ["", Validators.required],
      studentId: [this.student_id],
      studentIdsData: this.fb.array([])
    })
  }
  get student(): FormGroup {
    return this.fb.group({
      studentId: [""],
    });
  }
  checkedStudent(event, stuId) {
    if (event.target.checked) {
      this.studentIdData.push(stuId);
      // Add a new control in the arrayForm
      // (this.clubForm.get("students") as FormArray).push(event.target.value);
    }
    // console.error("################ ", this.clubForm.get('students')['controls'].value)
  }
  // getProgramMaster() {
  //   this.loading = true;
  //   this._ss.getProgramMaster(this.organizationId, this.student_id).subscribe((res: any) => {
  //     if (res.IsSuccess) {
  //       this.loading = false;
  //       this.programMasterData = res.Data;
  //     }
  //     else {
  //       this.loading = false;
  //     }
  //   })
  // }
  // getBatch(programMasterId) {
  //   this.loading = true;
  //   this.programMasterId = programMasterId
  //   this.getStudentDetails();
  //   this._ss.getProgramsByMasterID(this.organizationId, this.student_id, programMasterId).subscribe((res: any) => {
  //     if (res.IsSuccess) {
  //       this.loading = false;
  //       this.programsData = res.Data;
  //       console.log("this.programsData", this.programsData)
  //     }
  //     else {
  //       this.loading = false;
  //     }
  //   })
  // }
  // onChancheBatch(programId) {
  //   this.programId = programId;
  //   this.getStudentDetails();
  // }

  // getStudentDetails() {
  //   this.loading = true;
  //   this._ss.getAllStudentList(this.organizationId, this.student_id, this.programId, this.programMasterId, this.searchParam, this.pageNo, this.pageSize, this.sortBy).subscribe((data: any) => {
  //     if (data.IsSuccess) {
  //       this.loading = false;
  //       this.studentsListData = data.Data;
  //       console.log("this.studentsListData", this.studentsListData)
  //     }
  //     else {
  //       this.loading = false;
  //     }
  //   })
  // }
  setStudentsId(cur: any[]): FormArray {
    const formArray = new FormArray([]);
    cur.forEach(s => {
      let c = this.student
      c.patchValue({
        studentId: s
      })
      formArray.push(c)
    });
    return formArray.value;
  }
  changeClubType(event) {
    this.clubForm.patchValue({ 'clubType': event });
  }
  uploadGuidDoc(event) {
    if (event.target.files) {
      let formData: FormData = new FormData();
      // this.formD = new FormData();
      const file: File = event.target.files[0];
      // this.fileName = file.name
      this.fileName = file.name;
      // console.error("const file", file)
      this.guidDoc = file;
      // this.formD.append('file', file, file.name)
      // console.error("this.formD", file)
      let data = {
        file: this.formD,
      }
    }
    // this.formD.append('file', this.guidDoc)
    // console.error(this.formD)
    //  this._ss.uploadSurveyFile(this.organizationId, this.formD).subscribe((res: any) => {
    //   if (res.IsSuccess) {
    //     this.fileName = res.Data
    //     this.loading = false
    //     this.ts.pop("success", "", "Uploaded Successfully");
    //   } else {
    //     this.loading = false
    //     this.ts.pop("error", "", res.Message)
    //   }
    // })
  }
  onSaveBtn(data) {
    // data.students = this.setStudentsId(this.studentIdData);
    data.students = this.setStudentsId(this.allStudentData);
    this.formD = new FormData();
    if (this.guidDoc == undefined) this.formD.append('file', "null")
    else this.formD.append('file', this.guidDoc)
    this.formD.append('name', data.name)
    this.formD.append('description', data.description)
    this.formD.append('mission', data.mission)
    this.formD.append('vision', data.vision)
    this.formD.append('students', JSON.stringify(data.students))
    this.formD.append('clubType', data.clubType)
    this.formD.append('studentId', data.studentId)
    if(this.clubForm.controls.name.value.trim().length>0){
      if (this.clubForm.valid && this.clubForm.value.students.length > 0) {
        if (this.clubForm.value.students.length >= 1) {
          this.loading = true;
          this._ss.createClub(this.organizationId, this.student_id, this.formD).subscribe((res: any) => {
            if (res.IsSuccess) {
              this.loading = false;
              this.router.navigate(['/student/view-club'])
            }
            else {
              if (res.Message == 'format mismatch') {
                // this.ts.pop("success", "", "Club created successfully") 
                this.ts.pop("error", "", "Guidence document format mismatch")
              }
              else
                this.ts.pop("error", "", "Something wents wrong")
              this.loading = false;
            }
          })
        } else {
          this.ts.pop("error", "", "Please select at least 1 student")
        }
      } else {
        this.ts.pop("error", "", "Please fill in the required fields")
      }
    }
    else {
      this.ts.pop("error", "", "Club name is empty")
    }
    
  }
  getProgramName() {
    this._ss.getProgramMaster(this.organizationId, this.student_id)
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          this.programNames = res.Data;
          setTimeout(() => {
            this.loadscriptsService.loadStuff();
          }, 100);
          //console.log("res program name", this.programNames);
        } else {
          //console.log("error ", res);
        }
      });
  }

  fetchPrograms() {
    this.loading = true;
    this._ss.getProgramsByMasterID(this.organizationId, this.student_id, this.proName)
      .subscribe((res: any) => {
        //console.log("res ", res)
        if (res.IsSuccess) {
          setTimeout(() => {
            this.loadscriptsService.loadStuff();
          }, 100);
          this.loading = false;
          this.orgProgramsData = res.Data;
          //console.log("orgProgramsData ", this.orgProgramsData);
        } else {
          //console.log("error ", res);
          this.loading = false;
        }
      });
  }

  fetchDetails() {
    this.loading = true;
    // if (this.adminId != null) {
    this._ss.getAllStudentOrgList(
      this.organizationId,
      this.student_id,
      this.programs,
      this.proName,
      this.searchParam,
      this.pageNo,
      this.pageSize,
      this.sortBy
    )
      .subscribe(
        (res: any) => {
          if (res.IsSuccess) {
            this.loading = false;
            this.studentData = res.Data;

            console.log("daya", this.studentData);

            this.pageCount = Math.ceil(res.count / this.pageSize);
            this.recordCount = this.studentData.length;
            this.allCount = res.count;
            setTimeout(() => {
              this.loadscriptsService.loadStuff();
            }, 100);
            this.setPage();
            if (this.allStudentData.length > 0) {
              this.studentData.forEach((el) => {
                if (this.allStudentData.indexOf(el.id) > -1) {
                  setTimeout(() => {
                    $(`#${el.id}`).prop("checked", true);
                  }, 100);
                }
                else {
                  this.checkAll = false;
                  setTimeout(() => {
                    $(`#${el.id}`).prop("checked", false);
                  }, 100);
                }
              });
              this.flagFunction()
            }
            else {
              this.studentData.forEach((el) => {
                setTimeout(() => {
                  $(`#${el.id}`).prop("checked", false);
                }, 100);
              });
            }
          } else {
            //console.log("error ", res);
          }
        },
        (error) => {
          this.loading = false;
          this.ts.pop("error", "", error.error.respObj.Message);
        }
      );
  }

  flagFunction() {
    let flag = true;
    this.studentData.forEach((el) => {
      if (this.allStudentData.indexOf(el.id) == -1) {
        console.log(this.allStudentData)
        flag = false;
      }
    });
    $('#zero').prop('checked', false);
    if (flag) {
      setTimeout(() => {
        $('#zero').prop('checked', true);
      }, 100);
      this.checkAll = true;
    }
  }

  selectedAll() {
    this.selectAll = true;
  }
  unselectedAll() {
    this.selectAll = false;
  }

  isSelected(id) {
    // if (this.studentIds.indexOf(id) > -1) {
    //   return true;
    // } else {
    //   return false;
    // }
    if (this.allStudentData.indexOf(id) > -1) {
      return true;
    } else {
      return false;
    }
  }


  searchStudent(pageNo) {
    this.pageNo = pageNo;
    this.search();
    this.setPage();
  }

  previous() {
    this.pageNo = this.pageNo - 1;
    this.search();
    this.setPage();
  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.search();
    this.setPage();
  }

  setPage() {
    $(".pitem").removeClass("active");
    $(`.pitem:eq(${this.pageNo - 1})`).addClass("active");
    if (this.pageNo == 1 && this.pageCount == 1) {
      $(".previous").addClass("disabled");
      $(".next").addClass("disabled");
    }
    if (this.pageNo == 1 && this.pageCount > 1) {
      $(".previous").addClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageCount > 1) {
      $(".previous").removeClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageNo == this.pageCount) {
      $(".previous").removeClass("disabled");
      $(".next").addClass("disabled");
    }
  }

  arrayOne(n: number): any[] {
    if (n == undefined) n = 0;
    n = Math.ceil(n);
    if (n != undefined) return Array(n);
  }


  checkClicked() {
    this.checkAll = !this.checkAll;
    this.studentData.forEach((stu) => {
      // this.innercheckClicked(stu.id);
      if (this.checkAll) {
        if (this.allStudentData.indexOf(stu.id) === -1) {
          this.allStudentData.push(stu.id);
        }
      }
      else {

        if (this.allStudentData.indexOf(stu.id) === -1) {
          this.allStudentData.push(stu.id);
        } else {
          this.allStudentData.splice(this.allStudentData.indexOf(stu.id), 1);
        }
      }
    });
    this.selectAll = false;
  }

  innercheckClicked(id) {
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);

    if (this.allStudentData.indexOf(id) === -1) {
      this.allStudentData.push(id);
    } else {
      this.allStudentData.splice(this.allStudentData.indexOf(id), 1);
      this.checkAll = false;
      $(`#zero`).prop("checked", false);
    }
    this.allStudentData.forEach((el) => {
      setTimeout(() => {
        $(`#${el}`).prop("checked", true);
      }, 100);
    });
    console.log(this.allStudentData);
    setTimeout(() => {
      $("#selectOption").selectpicker("refresh");
    }, 100);
  }

  programNameSearch() {
    this.pageNo = 1;
    if ($("#proName").val() == "All") {
      this.batchShow = true;
      this.proName = undefined;
      this.selectAll = false;
      this.checkAll = false;
      // this.clubDetailsForm.patchValue({ programMasterId: "" });
      // this.clubDetailsForm.patchValue({ programId: "" });
      console.log(this.proName);
      this.programs = undefined;
      this.fetchDetails();
      this.fetchPrograms();
    } else {
      this.batchShow = true;
      this.proName = $("#proName").val();
      this.selectAll = false;
      this.checkAll = false;
      // this.clubDetailsForm.patchValue({ programMasterId: this.proName });
      // this.clubDetailsForm.patchValue({ programId: "" });
      console.log(this.proName);
      this.programs = undefined;
      this.fetchDetails();
      this.fetchPrograms();
    }
  }

  programsSearch() {
    this.pageNo = 1;
    if ($("#programs").val() == "All") {
      this.programs = undefined;
      this.checkAll = false;
      this.selectAll = false;
      $("#zero").prop("checked", false);
      // this.clubDetailsForm.patchValue({ programId: "" });
      console.log(this.programs);
      this.fetchDetails();
    } else {
      this.programs = $("#programs").val();
      this.checkAll = false;
      this.selectAll = false;
      $("#zero").prop("checked", false);
      // this.clubDetailsForm.patchValue({ programId: this.programs });
      console.log(this.programs);
      this.fetchDetails();
    }
  }

  search() {
    // console.log("m here")
    this.sortBy = $("#sortBy").val();
    this.pageSize = $("#pageSize").val();
    this.fetchDetails();
  }

  resetsearch() {
    this.pageNo = 1;
    this.searchParam = "";
    this.search();
    this.isCrossEnable = false;
  }

  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
    // console.log("yes typing...........", value)
  }

  searchSpecific() {
    // console.log("m here")
    this.pageNo = 1;
    this.sortBy = $("#sortBy").val();
    this.pageSize = $("#pageSize").val();
    this.fetchDetails();
  }
}
