import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// import { AuthService } from 'angularx-social-login';
import * as moment from 'moment';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { OwlOptions } from 'ngx-owl-carousel-o';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
declare var require: any
const FileSaver = require('file-saver');
@Component({
  selector: 'app-club-detail',
  templateUrl: './club-detail.component.html',
  styleUrls: ['./club-detail.component.css']
})
@Pipe({
  name: 'splitComma'
})
export class ClubDetailComponent implements OnInit, PipeTransform {
  isThisStuPresident: boolean = false;
  presidentStuData: any;
  fileName: File;
  formD: FormData;
  eventData: any;
  transform(val: string): string[] {
    return val.split('_');
  }
  loading: boolean;
  clubData: any;
  student_id: string;
  organizationId: string;
  clubMasterId: any;
  allClubStuData: any;
  customOptions: OwlOptions;
  responseText: any = "";
  clubComments: any = [];

  constructor(private _ss: StudentService, private cs: CookieServiceProvider, private router: Router,
    private route: ActivatedRoute, private ts: ToasterService) { }

  ngOnInit() {
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    this.route.params.subscribe(param => {
      this.clubMasterId = param.clubId;
    })
    this.getClubDetails();
    this.getAllClubIntersetedStu();
    // this.getEventsByClubId();
    this.getClubComments();
  }

  getClubDetails() {
    this.loading = true;
    this._ss.getSpecificClub(this.organizationId, this.student_id, this.clubMasterId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.clubData = res.Data;
        this.clubData.ClubIntrestedStudents.forEach(element => {
          if (element.studentId == this.student_id) {
            if (element.isPresident) { this.isThisStuPresident = true; this.presidentStuData = element; }
          } else {
            this.presidentStuData = element;
          }
        });
      }
      else {
        this.loading = false;
      }
      this.getEventsByClubId();
    })
  }
  getEventsByClubId() {
    this.loading = true;
    this._ss.getEventsByClubId(this.organizationId, this.student_id, this.clubMasterId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.eventData = res.Data;
        this.clubData.ClubIntrestedStudents.forEach(element => {
          if (element.studentId == this.student_id) {
            if (element.isPresident) { this.isThisStuPresident = true; this.presidentStuData = element; }
          } else {
            this.presidentStuData = element;
          }
        });
        setTimeout(() => {
          this.customOptions = {
            loop: true,
            autoplay: true,
            center: true,
            dots: false,
            margin: 10,
            autoHeight: true,
            autoWidth: true,
            responsive: {
              0: {
                items: 1,
              },
              600: {
                items: 4,
              },
              1000: {
                items: 6,
              },
              1200: {
                items: 6,
              }
            }
          }
          this.loading = false;
        }, 1000);

      }
      else {
        this.loading = false;
      }
    })
  }
  getAllClubIntersetedStu() {
    this.loading = true;
    this._ss.getAllClubIntersetedStu(this.organizationId, this.student_id, this.clubMasterId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.allClubStuData = res.Data;
      }
      else {
        this.loading = false;
      }
    })
  }
  uploadReport(event) {
    if (event.target.files) {
      this.formD = new FormData();
      let formData: FormData = new FormData();
      // this.formD = new FormData();
      const file: File = event.target.files[0];
      // this.fileName = file.name
      this.fileName = file;
      this.formD.append('file', file, file.name)
      this.formD.append('clubId', this.clubMasterId)
      // console.error("this.formD",this.formD)
      let data = {
        file: this.formD,
      }
    }
    this.loading = true;
    this._ss.uploadClubReport(this.organizationId, this.student_id, this.formD).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.getClubDetails();
      }
      else {
        this.ts.pop("error", "", res.Message)
        this.loading = false;
      }
    })
  }
  guidingDocDownload(url) {
    const pdfName = 'guidingDoc';
    FileSaver.saveAs(url, pdfName);
  }
  surveyReportDownload(url) {
    const pdfName = 'surveyReport';
    FileSaver.saveAs(url, pdfName);
  }

  getClubComments() {
    // this.loading = true;
    this._ss.getClubComments(this.organizationId, this.student_id, this.clubMasterId)
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false
          let data = res.Data
          data.forEach((e) => {
            e.createdAt = new Date(e.createdAt)
          })
          data.sort((a, b) => a.createdAt - b.createdAt);
          this.clubComments = data;
          // console.error(this.clubComments)
        }
        else {
          this.loading = false;
        }
      })
  }

  sendMessage() {
    let obj = {
      clubId: this.clubMasterId,
      studentId: this.student_id,
      userResponseId: null,
      userComment: null,
      userCommentDate: null,
      studentComment: this.responseText,
      studentCommentDate: moment(),
    }
    if(this.responseText.trim().length>0){
      this.loading = true;
      this._ss.createClubComment(this.organizationId, this.student_id, obj).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          this.responseText = "";
          this.ts.pop("success", "", "Sent")
          this.getClubComments();
  
        }
        else {
          this.loading = false;
        }
      })
    }
    else {
      this.ts.pop("warning", "", "Please write something")
    }
    
  }
}
