import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { StudentService } from '../../service/student.service';
declare var $: any;

@Component({
  selector: 'app-view-club',
  templateUrl: './view-club.component.html',
  styleUrls: ['./view-club.component.css'],
  providers: [LoadscriptsService]
})
export class ViewClubComponent implements OnInit {
  student_id: string;
  organizationId: string;
  loading: boolean;
  clubResponse : any = [];
  joinedClubs : any = [];
  clubStatus: any = '';
  clubType: any = '';
  searchParams1: String = '';
  searchParams2: String = '';
  isCrossEnable1: boolean;
  isCrossEnable2: boolean;

  constructor(
    private _ss: StudentService, private cs: CookieServiceProvider,
    private ts: ToasterService, private router: Router, private loadscriptsService: LoadscriptsService
  ) { }

  ngOnInit() {
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    this.allJoinedList();
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);
  }

  allJoinedList(){
    this.loading = true;
    this._ss.getAllClubs(this.organizationId, this.student_id, this.searchParams1, this.clubType).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.joinedClubs = res.Data;
        this.joinedClubs.sort((a,b)=> new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime())
        this.joinedClubs.forEach((o) => {
          o.isJoinedClub = o.ClubIntrestedStudents.filter((e)=> e.studentId == this.student_id)
        })
      }
      else {
        this.loading = false;
      }
    })
  }
  allRequestedList(){
    this.loading = true;
    this._ss.getAllRequestedClubs(this.organizationId, this.student_id, this.searchParams2, this.clubStatus).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.clubResponse = res.Data;
        this.clubResponse.sort((a,b)=> new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime())
      }
      else {
        this.loading = false;
      }
    })
  }

  search1() {
    // this.sortBy = $('#sortBy').val()
    this.allJoinedList();
  }

  resetsearch1() {
    this.searchParams1 = "";
    this.search1();
    this.isCrossEnable1 = false;
  }

  isSearchType1(value) {
    if (value) {
      this.isCrossEnable1 = true;
    } else {
      this.isCrossEnable1 = false;
    }
  }

  search2() {
    // this.sortBy = $('#sortBy').val()
    this.allRequestedList();
  }

  resetsearch2() {
    this.searchParams2 = "";
    this.search2();
    this.isCrossEnable2 = false;
  }

  isSearchType2(value) {
    if (value) {
      this.isCrossEnable2 = true;
    } else {
      this.isCrossEnable2 = false;
    }
  }

  clubDelete(clubId) {
    this.loading = true;
    this._ss.deletClub(this.student_id, clubId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.allRequestedList();
        this.ts.pop("success", "Deleted successfully");
      }
      else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    })
  }
  getByType(){
    this.clubType = $('#clubType').val();
    this.allJoinedList()
  }

  getByStatus(){
    this.clubStatus = $('#clubStatus').val();
    this.allRequestedList()
  }

  goToStatusPage(clubId){
    this.router.navigate(["/student/club-status",clubId]);
  }

  joinClubRequest(clubId){
    console.error(clubId)
  }

}
