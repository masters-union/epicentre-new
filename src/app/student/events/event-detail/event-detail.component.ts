import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";

declare var $: any;
@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {
    loading: boolean = false;
    public config: ToasterConfig = new ToasterConfig({ limit: 1 });
    student_id: String = "";
    eventDetail: any;
    requestSent: any;
    event_id: any;
    eventDate: Date;
    eventStartTime: any;
    eventEndTime: any;
  

    constructor( private _ss: StudentService, private cs: CookieServiceProvider, private route: ActivatedRoute,
        private ts: ToasterService, private router: Router, private location: Location) { }

    ngOnInit() {
        window.console.log = function () { };
        this.student_id = this.cs.getItem('id');
        this.route.params.subscribe((params: Params) => {
            this.event_id = params['eventId'];
        })
        this.getEventDetailById()
    }
    
    goBack() {
        this.location.back()
    }

    getEventDetailById() {
        this.loading = true;
        this._ss.getEventById(this.student_id, this.event_id ).subscribe((data: any) => {
        
            if (data.IsSuccess) {
                this.eventDetail = data.Data;
                let timeArr = this.eventDetail.EventMaster.startTime.split(":")
                this.eventDate = new Date(this.eventDetail.EventMaster.eventDateTime);
                this.eventDate.setHours(+timeArr[0],+timeArr[1], 0, 0)
                this.eventStartTime = this.tConvert(this.eventDetail.EventMaster.startTime);
                this.eventEndTime = this.tConvert(this.eventDetail.EventMaster.endTime);
                // this.eventDetail.EventMaster.startTime = this.tConvert(this.eventDetail.EventMaster.startTime);
                this.requestSent = this.eventDetail.isAccepted
                this.loading = false
            }
            else {
                this.loading = false;
            }
          })
    }

     tConvert(time) {
        time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
      
        if (time.length > 1) { 
          time = time.slice (1);  
          time[5] = +time[0] < 12 ? ' AM' : ' PM';
          time[0] = +time[0] % 12 || 12; 
        }
        return time.join ('');
    }

    registerEvent() {
        if(new Date(this.eventDate) < new Date()){
            this.ts.pop("error", "", "Event already held");
        }
        else {
            if(this.requestSent != null){
                if(this.requestSent){
                    this.ts.pop("error", "", "Event already registered");
                }
                else {
                    this.ts.pop("warning", "", "Approval is pending");
                    
                }
            }else{
                this.loading = true
                let eventId = {
                    eventId : this.event_id
                }
                this._ss.sendEventRequest(this.student_id, eventId ).subscribe((data: any) => {
                
                    if (data.IsSuccess) {
                        this.loading = false
                        this.ts.pop("success", "", "Event registration done successfully");
                        setTimeout(() => {
                            this.router.navigate(["/student/events"]);
                        }, 1000);
                        
                    }
                    else {
                        this.loading = false;
                        this.ts.pop("error", "", "Something Went wrong, please try again later");
                    }
                  })
            }
        }
        
        
    }

    goToClubDetail(id){
        this.router.navigate(['/student/event-club-detail', id]);
    }

}
