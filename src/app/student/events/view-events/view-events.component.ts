import { Component, ElementRef, OnInit } from '@angular/core';
import { CalendarOptions, DayCellContent } from '@fullcalendar/angular';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import * as _ from 'lodash';
import * as moment from 'moment';
import {LoadscriptsService } from '../../../common/service/loadscripts.service';
declare var $: any;


@Component({
  selector: 'app-view-events',
  templateUrl: './view-events.component.html',
  styleUrls: ['./view-events.component.css'],
  providers: [LoadscriptsService]
})
export class ViewEventsComponent implements OnInit {
    
  loading: boolean = false;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  calendarOptions: CalendarOptions;
  student_id: String = "";
  allEvents = [];
  eventsArray = [];
  orgClubsData = [];
  club: any;
  searchParam = "";
  isCrossEnable: boolean;
  today = new Date();
  activeCount : any = 0;
  pastCount: any = 0;
  upcomingCount: any = 0;
  eventDetailId: any;
  clubTitle : any;
  clubPresident: any;
  presidentImg: any;
  presidentArrayLen: any = 0;

  constructor( private _ss: StudentService, private cs: CookieServiceProvider, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router, private loadscriptsService: LoadscriptsService, private elRef:ElementRef ) { }

  ngOnInit() {
    window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.getAllEventsData();
    this.fetchClubs();
    this.nextPrevButton();
    this.monthDayWeekButton();
    this.today.setHours(23,59,0,0)
    
  }

  getAllEventsData() {
    this.activeCount = 0;
    this.pastCount = 0;
    this.upcomingCount = 0;
    setTimeout(() => {
        $(".fc-daygrid-event-harness").parents("td.fc-day-past").removeClass("past-bg-pink");
        $(".fc-daygrid-event-harness").parents("td.fc-day-future").removeClass("upcoming-bg-yellow");
        $(".fc-daygrid-event-harness").parents("td.fc-day-today").removeClass("active-bg-green");
    }, 100);
    this.loading = true;
      this._ss.getAllEvents(this.student_id, this.searchParam, this.club).subscribe((data: any) => {
        
        if (data.IsSuccess) {
            
            this.allEvents = data.Data;
            this.eventsArray = this.allEvents.filter(e=> e.EventMaster? e.EventMaster.isPublished == true : false);
            this.eventsArray.forEach( (o) => {
                if(o.EventMaster){
                    o.title = o.EventMaster.title
                    o.id = o.EventMaster.id
                    //this.presidentArrayLen = o.EventMaster.ClubMaster.ClubPresidents.length
                    if(o.EventMaster.ClubMaster.ClubPresidents.length>0){
                        o.president = o.EventMaster.ClubMaster.ClubPresidents[0].Student.firstName + " " + o.EventMaster.ClubMaster.ClubPresidents[0].Student.lastName
                    }
                    Object.keys(o.EventMaster).forEach( (k) => {
                        let eventDate = new Date(o.EventMaster[k])
                        eventDate.setHours(23,59,0,0)
                        if (k == 'eventDateTime') {
                           if(this.today > eventDate){
                               o.status = "past"
                               this.pastCount = this.pastCount +1
                               setTimeout(() => {
                                $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                            }, 100);
                           }else if(this.today < eventDate){
                                o.status = "upcoming"
                                this.upcomingCount = this.upcomingCount +1
                                setTimeout(() => {
                                    $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("upcoming-bg-yellow");
                                }, 100);
                           }else{
                                o.status = "active"
                                this.activeCount = this.activeCount +1
                                setTimeout(() => {
                                    $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                                }, 100);
                           }
                            o.date = moment(o.EventMaster[k]).format('YYYY-MM-DD');
                            delete o.EventMaster[k];
                        }
                        if (k == "ClubMaster") {
                            if(o.EventMaster.ClubMaster.ClubPresidents.length>0){
                                if(o.EventMaster.ClubMaster.ClubPresidents[0].Student.StudentDetail){
                                o.presidentImg = o.EventMaster.ClubMaster.ClubPresidents[0].Student.StudentDetail.profilePicUrl
                                }
                                else {
                                    o.presidentImg = null
                                }
                                o.presidentArrayLen = o.EventMaster.ClubMaster.ClubPresidents.length
                            }
                            o.clubName = o.EventMaster.ClubMaster.name;
                            delete o[k];
                        }
                    });
                }

            });
            this.eventsArray = this.allEvents.map(({ title, date, id, president, clubName, status, presidentImg, presidentArrayLen}) => ({ title, date, id, president, clubName, status, presidentImg, presidentArrayLen}))
            this.loading = false
            this.initializeCalendar();
        }
        else {
            this.loading = false;
        }
      })
  }

  initializeCalendar() {
    this.calendarOptions = {
        initialView: 'dayGridMonth',
        // dateClick: this.handleDateClick.bind(this),
        events: this.eventsArray,
        eventClick: this.showModal.bind(this),
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridDay,dayGridMonth,dayGridWeek'
        },
        eventBackgroundColor: "transparent",
        eventBorderColor: "transparent",
        eventContent: this.renderEventContent,
    };
    // var detailBox = '<div class="view-event-box">'+
    //                       '<div class="cover-outer">'+
    //                           `<h2 class="mb15" id="clubTitle"></h2>`+
    //                           '<div class="ev-box-inner mb15">'+
    //                               '<div class="cover-outer">'+
    //                                   '<span>Club President</span>' +
    //                                   `<p id="clubPresident"></p>`+
    //                               '</div>'+
    //                             //   '<figure><img src="../../../assets/img/person-profile.jpg"></figure>'+
    //                           '</div>'+
    //                         //   '<div class="cover-outer">'+
    //                         //     '<span>Club Members</span>' +
    //                         //     '<p>50</p>'+
    //                         //   '</div>'+
    //                           '<button type="button" class="btn4 mt15">View Detials</button>'

    //                       '</div>'+
    //                   '</div>';
    // setTimeout(() => {
    //     $(detailBox).appendTo( ".fc-event");
    // }, 100);
    setTimeout(() => {
    $(".fc-header-toolbar.fc-toolbar .fc-toolbar-title").appendTo("fc-header-toolbar.fc-toolbar .fc-toolbar-chunk:nth-child(1) .fc-button-group");
    }, 100);
    
  }
  renderEventContent(eventInfo, createElement) {
    let innerHtml;
    innerHtml =
      "<h4 class='red-color text-capitalize'>" +
      eventInfo.event.title +
      "</h4>" +
      "<span class='text-yellow-bg text-capitalize mt-1'>" +
      eventInfo.event._def.extendedProps.clubName +
      "</span>";
    //Event with rendering html
    return (createElement = {
      html: "<div class='cover-outer ml18'>" + innerHtml + "</div>",
    });
    // }
  }

  fetchClubs() {
    // this._ss.getClubs(this.student_id).subscribe((res: any) => {
    //     if (res.IsSuccess) {
    //       setTimeout(() => {
    //         this.loadscriptsService.loadStuff()
    //       }, 100);
    //       this.loading = false;
    //       this.orgClubsData = res.Data;
    //     } else {
    //       // console.log("error ", res);
    //       this.loading = false
    //     }
    //   })
  }

  nextPrevButton() {
    setTimeout(() => {
        $('body').on('click', 'button.fc-prev-button', () => {
            this.eventsArray.forEach( (o) => {
                if(this.today > new Date(o.date)){
                    setTimeout(() => {
                    $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 10);
                }else if(this.today < new Date(o.date)){
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("upcoming-bg-yellow");
                    }, 10);
                }else{
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                    }, 10);
                }
            });
        });
     
        $('body').on('click', 'button.fc-next-button', () => {
            this.eventsArray.forEach( (o) => {
                if(this.today > new Date(o.date)){
                    setTimeout(() => {
                    $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 10);
                }else if(this.today < new Date(o.date)){
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("upcoming-bg-yellow");
                    }, 10);
                }else{
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                    }, 10);
                }
                
            });
        });
    }, 100);
  }

  monthDayWeekButton() {
    setTimeout(() => {
        $('body').on('click', 'button.fc-dayGridDay-button', () => {
            this.eventsArray.forEach( (o) => {
                if(this.today > new Date(o.date)){
                    setTimeout(() => {
                    $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 10);
                }else if(this.today < new Date(o.date)){
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("upcoming-bg-yellow");
                    }, 10);
                }else{
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                    }, 10);
                }
            });
        });
     
        $('body').on('click', 'button.fc-dayGridMonth-button', () => {
            this.eventsArray.forEach( (o) => {
                if(this.today > new Date(o.date)){
                    setTimeout(() => {
                    $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 10);
                }else if(this.today < new Date(o.date)){
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("upcoming-bg-yellow");
                    }, 10);
                }else{
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                    }, 10);
                }
                
            });
        });
        $('body').on('click', 'button.fc-dayGridWeek-button', () => {
            this.eventsArray.forEach( (o) => {
                if(this.today > new Date(o.date)){
                    setTimeout(() => {
                    $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 10);
                }else if(this.today < new Date(o.date)){
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("upcoming-bg-yellow");
                    }, 10);
                }else{
                    setTimeout(() => {
                        $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                    }, 10);
                }
                
            });
        });
    }, 100);
  }
  
  clubsSearch() {
    if($('#club').val() == "Reset"){
        this.club = undefined
    }
    else {
        this.club = $('#club').val()
    }
    this.getAllEventsData()
  }

  search() {
    this.getAllEventsData()
  }
  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
  }

  resetsearch() {
    this.searchParam = "";
    this.search()
    this.isCrossEnable = false;
  }
  

//   handleDateClick(arg) {
//     alert('date click! ' + arg.dateStr)
//   }

  showModal(arg) {
    // var detailBox = '<div class="view-event-box"> '+
    // '<div class="cover-outer">'+
    // '<h2>Indoor Sports Fest</h2>'+
    // '</div>'+
    // '</div>';
    // $(".fc-event").click(function(){
    //     $(this).children('.view-event-box').find('#clubTitle').text(`${arg.event.title}`);
    //     $(this).children('.view-event-box').find( '#clubPresident').text(`${arg.event.extendedProps.president}`);
    //     $(this).children('.view-event-box').toggleClass("show");
    // });
    // this.elRef.nativeElement.querySelector('button').addEventListener('click', this.goToDetailsPage.bind(this));
   
    // setTimeout(() => {
    //     $('.fc-event').children('.view-event-box').toggleClass("show");
    // }, 100);
    // $(this).children('.view-event-box').toggleClass("show");
      // console.error(arg.event.title, arg.event.id, arg.event.extendedProps.president)
      this.eventDetailId = arg.event.id
      this.clubTitle = arg.event.title;
      this.clubPresident = arg.event.extendedProps.president;
      this.presidentImg = arg.event.extendedProps.presidentImg;
      this.presidentArrayLen = arg.event.extendedProps.presidentArrayLen
      $('#exampleModal').modal('show')
  }

  goToDetailsPage() {
    $('#exampleModal').modal('hide')
    this.router.navigate(['/student/event-detail', this.eventDetailId]);
  }
}
