import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventClubDetailComponent } from './event-club-detail.component';

describe('EventClubDetailComponent', () => {
  let component: EventClubDetailComponent;
  let fixture: ComponentFixture<EventClubDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventClubDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventClubDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
