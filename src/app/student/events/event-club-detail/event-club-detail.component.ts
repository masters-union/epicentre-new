import { Component, OnInit } from '@angular/core';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import {LoadscriptsService } from '../../../common/service/loadscripts.service';
import { Location } from '@angular/common';
declare var $: any;

@Component({
  selector: 'app-event-club-detail',
  templateUrl: './event-club-detail.component.html',
  styleUrls: ['./event-club-detail.component.css'],
  providers: [LoadscriptsService]
})
export class EventClubDetailComponent implements OnInit {
  loading: boolean = false;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  student_id: String = "";
  eventUsers: any = [];
  event_id: any;
  eventTitle: any;
  eventClubName: any;
  requestSent: any;

  constructor( private _ss: StudentService, private cs: CookieServiceProvider, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router, private loadscriptsService: LoadscriptsService, private location: Location ) { }

  ngOnInit() {
    // window.console.log = function () { };
    this.student_id = this.cs.getItem('id');
    this.route.params.subscribe((params: Params) => {
      this.event_id = params['eventId'];
  })
  this.getEventUsers()
  }
  goBack() {
    this.location.back()
}

  getEventUsers() {
    this.loading = true;
    this._ss.getEventUsers(this.student_id, this.event_id ).subscribe((data: any) => {
    
        if (data.IsSuccess) {
            this.eventUsers = data.Data.filter(e=> e.isAccepted != null);
            if(this.eventUsers.length>0){
                this.eventTitle = this.eventUsers[0].EventMaster.title;
                this.eventClubName = this.eventUsers[0].EventMaster.ClubMaster.name;
                this.requestSent = this.eventUsers.find(e => e.isAccepted != null);
                // console.error(this.requestSent)
            }
            this.loading = false
        }
        else {
            this.loading = false;
        }
      })
}
registerEvent() {
    if(new Date(this.eventUsers[0].EventMaster.eventDateTime) < new Date()){
        this.ts.pop("error", "", "Event already held");
    }
    else {
        if(this.requestSent){
            if(this.requestSent.isAccepted){
                this.ts.pop("error", "", "Event already registered");
            }
            else {
                this.ts.pop("warning", "", "Approval is pending");
                
            }
        }else{
            this.loading = true
            let eventId = {
                eventId : this.event_id
            }
            this._ss.sendEventRequest(this.student_id, eventId ).subscribe((data: any) => {
            
                if (data.IsSuccess) {
                    this.loading = false
                    this.ts.pop("success", "", "Event registration done successfully");
                    setTimeout(() => {
                        this.router.navigate(["/student/events"]);
                    }, 1000);
                }
                else {
                    this.loading = false;
                    this.ts.pop("error", "", "Something Went wrong, please try again later");
                }
              })
        }
    }
    
    
}

}
