import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router,Params } from "@angular/router";
import { ToasterService } from "angular2-toaster";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from '../service/student.service';

@Component({
  selector: 'app-transcript-details',
  templateUrl: './transcript-details.component.html',
  styleUrls: ['./transcript-details.component.css'],
  providers:[LoadscriptsService]
})
export class TranscriptDetailsComponent implements OnInit {
	loading : boolean = false;
	courseData = [];
  organizationId: string;
	studentId: string;
  batch: string;
  term:string
  gradeValue:number=0
	totalScore: number=0
	totalCredits:number=0
  finalTgpa:number=0
  outClass:any
  constructor(		private route: ActivatedRoute,
		private cs: CookieServiceProvider,
		private _ss: StudentService,
		private router: Router,
		private ts: ToasterService,
		private loadscriptsService: LoadscriptsService) { }


    ngOnInit() {
      // this.studentId = this.cs.getItem("id");
      // this.organizationId = this.cs.getItem("orgId");
      this.batch = this.cs.getItem("batch");
      this.route.params.subscribe((params: Params) => {
        this.term = params['term'];
        this.batch = params['batch'];
        this.studentId = params['studentId'];
        this.outClass = params['outClass'];

      }) 
      this.getTranscriptDetails()
    }
    getTranscriptDetails(){
      // this.loading=true
    this._ss.getTranscriptDetails(this.studentId,this.batch, this.term,this.outClass).subscribe((data: any) => {
      if(data.IsSuccess){
        this.loading = false;
        console.log(data.Data);
        this.courseData = data.Data;			
        this.courseData.forEach(element => {
          if(element.grades =="" || element.grades ==null ){
            this.gradeValue=0
          }else{
            this.gradeValue=element.grades
            console.log(this.gradeValue);
          }
          var str = element.CourseRoster.courseName;
          var mapObj = {
            'Section A': "",
            'Section B': "",
            'Section C': ""
          };
          str = str.replace(/Section A|Section B|Section C/gi, function (matched) {
            return mapObj[matched];
          });
          element.CourseRoster.courseName=str;
          this.totalScore += element.CourseRoster.credits* +this.gradeValue
          this.totalCredits +=parseFloat(element.CourseRoster.credits)
          });
          let tgpa
          tgpa = (this.totalScore / this.totalCredits)
          this.finalTgpa = tgpa.toFixed(2)
          console.log(this.finalTgpa);
          
            
      }else {
        this.loading = false;
        this.ts.pop("error", "", "Something went wrong, Please try again")
      }
    })
  }
}
