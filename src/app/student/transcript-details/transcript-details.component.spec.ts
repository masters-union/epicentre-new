import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranscriptDetailsComponent } from './transcript-details.component';

describe('TranscriptDetailsComponent', () => {
  let component: TranscriptDetailsComponent;
  let fixture: ComponentFixture<TranscriptDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranscriptDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranscriptDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
