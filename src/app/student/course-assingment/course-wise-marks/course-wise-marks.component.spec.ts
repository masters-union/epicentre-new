import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseWiseMarksComponent } from './course-wise-marks.component';

describe('CourseWiseMarksComponent', () => {
  let component: CourseWiseMarksComponent;
  let fixture: ComponentFixture<CourseWiseMarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseWiseMarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseWiseMarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
