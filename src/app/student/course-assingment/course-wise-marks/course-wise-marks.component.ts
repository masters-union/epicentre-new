import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToasterService } from "angular2-toaster";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from "../../service/student.service";

@Component({
	selector: "app-course-wise-marks",
	templateUrl: "./course-wise-marks.component.html",
	styleUrls: ["./course-wise-marks.component.css"],
	providers: [LoadscriptsService],
})
export class CourseWiseMarksComponent implements OnInit {
	studentId: string;
	organizationId: string;
	batch: string;
	loading: boolean = false;
	assingmentData =[];
	courseId: string;
	courseName: string;
	percentage: number;
	obtainedMarks: number;
	totalMarks: number;

	constructor(
		private route: ActivatedRoute,
		private cs: CookieServiceProvider,
		private _ss: StudentService,
		private router: Router,
		private ts: ToasterService,
		private loadscriptsService: LoadscriptsService
	) {}

	ngOnInit() {
		this.studentId = this.cs.getItem("id");
		this.organizationId = this.cs.getItem("orgId");
		this.batch = this.cs.getItem("batch");
		this.courseId = this.route.snapshot.paramMap.get("id");
		this.loadscriptsService.loadStuff();
		this.getAssingmentMarks();
	}

	getAssingmentMarks() {
		this.loading = true;
		this._ss
			.getMarksByCourseId(this.studentId, this.courseId)
			.subscribe((data: any) => {
				if (data.IsSuccess) {
					this.loading = false;
					this.loadscriptsService.loadStuff();
					this.assingmentData = data.Data;
					console.log(data.Data);
					if(this.assingmentData.length > 0){
						this.courseName = this.assingmentData[0].CourseRoster.courseName;
					}					
					this.getPercentageScore(data.Data);
				} else {
					this.loading = false;
					this.ts.pop("error", "Something Went Wrong");
				}
			});
	}

	getPercentageScore(data){
		this.obtainedMarks = 0,this.totalMarks = 0;
		data.forEach((element)=>{			
            this.obtainedMarks += parseInt(element.StudentMarks[0].obtainedMarks);
            this.totalMarks += parseInt(element.totalPoint);
          })
          
		this.percentage = (this.obtainedMarks/ this.totalMarks)*100;
		this.percentage = parseInt(this.percentage.toFixed(2));	
	}
}
