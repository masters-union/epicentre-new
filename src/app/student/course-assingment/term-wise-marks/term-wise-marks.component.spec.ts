import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermWiseMarksComponent } from './term-wise-marks.component';

describe('TermWiseMarksComponent', () => {
  let component: TermWiseMarksComponent;
  let fixture: ComponentFixture<TermWiseMarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermWiseMarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermWiseMarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
