import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToasterService } from "angular2-toaster";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from "../../service/student.service";
import { saveAs } from 'file-saver';

@Component({
	selector: "app-term-wise-marks",
	templateUrl: "./term-wise-marks.component.html",
	styleUrls: ["./term-wise-marks.component.css"],
	providers: [LoadscriptsService],
})
export class TermWiseMarksComponent implements OnInit {
	organizationId: string;
	batch: string;
	studentId: string;
	loading: boolean = false;
	courseData = [];
	totalData = []
	term: any
	gradeValue: number = 0
	totalScore: number = 0
	totalCredits: number = 0
	outClass: boolean = false;

	// courseData = [
	// 	{ name: "Statistics", scored: 68, total: 100 },
	// 	{ name: "Maths", scored: 89, total: 100 },
	// 	{ name: "Geography", scored: 77, total: 100 },
	// 	{ name: "Computer Science", scored: 97, total: 100 },
	// 	{ name: "Data Science", scored: 65, total: 100 },
	// 	{ name: "Physics", scored: 33, total: 100 },
	// 	{ name: "Chemistry", scored: 20, total: 100 },
	// 	{ name: "English", scored: 90, total: 100 },
	// ];

	constructor(
		private route: ActivatedRoute,
		private cs: CookieServiceProvider,
		private _ss: StudentService,
		private router: Router,
		private ts: ToasterService,
		private loadscriptsService: LoadscriptsService
	) { }

	ngOnInit() {
		this.studentId = this.cs.getItem("id");
		this.organizationId = this.cs.getItem("orgId");
		this.batch = this.cs.getItem("batch");
		this.loadscriptsService.loadStuff();
	}

	onTermSelect(term: any) {
		this.term=term
		this.loading = true;
		// this.term = term
		this._ss.getAssigmentsTotalCount(this.studentId, this.batch, this.term,this.outClass).subscribe((data: any) => {
			if (data.IsSuccess) {
				this.loading = false;
				console.log(data.Data);
				this.courseData = data.Data;
				this.totalData = data.Data1;
				this.courseData.forEach(element => {
					if (element.grades == "" || element.grades == null) {
						this.gradeValue = 0
					} else {
						this.gradeValue = element.grades
						console.log(this.gradeValue);

					}
					var str = element.CourseRoster.courseName;					
					var mapObj = {
					  'Section A': "",
					  'Section B': "",
					  'Section C': ""
					};					
					str = str.replace(/Section A|Section B|Section C/gi, function (matched) {
					  return mapObj[matched];
					});
					element.CourseRoster.courseName=str
					this.totalScore += element.CourseRoster.credits * +this.gradeValue
					this.totalCredits += parseFloat(element.CourseRoster.credits)
				});
				let tgpa
				tgpa = (this.totalScore / this.totalCredits) * 100



				if (data.Data.length == 0) {
					this.ts.pop("error", "", "No courses found for this term");
				}
			} else {
				this.loading = false;
				this.ts.pop("error", "", "Something went wrong, Please try again")
			}
		})	
	}
	downloadTransript() {
		this.loading = true
		this._ss.downloadTranscript(this.studentId, this.batch, this.term).subscribe((res: any) => {
			this.loading = false
			console.log(res);

			saveAs(res, "downloadTranscript" + this.studentId + ".pdf");
			this.ts.pop("success", "", "Transcript Download Successfully")
			setTimeout(() => {
				this.router.navigate(['/student/downloadTransript/' + this.term + '/' + this.batch + '/' + this.outClass])
			}, 2000);
		})
	}

	onCourseClick(id: any) {
		this.router.navigate([`/student/courseWiseAssingmentMarks/${id}`]);
	}
	updateOutClass(event) {
		this.outClass=false
		if (event.target.checked) {
			this.outClass=true
		}
		this.loading = true;
		// this.term = term
		this._ss.getAssigmentsTotalCount(this.studentId, this.batch, this.term,this.outClass).subscribe((data: any) => {
			if (data.IsSuccess) {
				this.loading = false;
				console.log(data.Data);
				this.courseData = data.Data;
				this.totalData = data.Data1;
				this.courseData.forEach(element => {
					if (element.grades == "" || element.grades == null) {
						this.gradeValue = 0
					} else {
						this.gradeValue = element.grades
						console.log(this.gradeValue);
					}
					var str = element.CourseRoster.courseName;
					var mapObj = {
					  'Section A': "",
					  'Section B': "",
					  'Section C': ""
					};
					str = str.replace(/Section A|Section B|Section C/gi, function (matched) {
					  return mapObj[matched];
					});
					this.totalScore += element.CourseRoster.credits * +this.gradeValue
					this.totalCredits += parseFloat(element.CourseRoster.credits)
				});
				let tgpa
				tgpa = (this.totalScore / this.totalCredits) * 100



				if (data.Data.length == 0) {
					this.ts.pop("error", "", "No courses found for this term");
				}
			} else {
				this.loading = false;
				this.ts.pop("error", "", "Something went wrong, Please try again")
			}
		})		
	}

}
