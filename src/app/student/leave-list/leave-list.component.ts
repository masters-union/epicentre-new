import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { StudentService } from "../service/student.service";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
declare var $: any;
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import * as moment from "moment";

declare var swal;

@Component({
  selector: "app-leave-list",
  templateUrl: "./leave-list.component.html",
  styleUrls: ["./leave-list.component.css"],
  providers: [LoadscriptsService],

})
export class LeaveListComponent implements OnInit {
  studentId: string;
  organizationId: string;
  leavesList = [];
  loading: boolean = false;
  searchParam:String=""
  isCrossEnable:boolean
  leaveType:any
  currentDate : any;
  fromDate : any;

  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService, private loadscriptsService: LoadscriptsService,
  ) {}

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.currentDate = new Date().toISOString();    
    this.getMyLeavesList();
  }
  getMyLeavesList() {
    this.loading = true;
    this._ss.getAllLeavesList(this.studentId,this.searchParam).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        this.leavesList = data.Data;
        setTimeout(() => {
					this.loadscriptsService.loadStuff();
				}, 1000);
        // console.log("check data", data.Data);
      }
    });
  }

  cancelLeave(leaveId) {
    this.showPopupCancel(leaveId);
  }

  // shouldCancel(leaveRow){
  //   console.log("row",leaveRow)
 

  //   let timing = moment(leaveRow).format('MM/DD/YYYY');
  //   console.log("ds",timing)
  //   console.log("111",this.currentDate)

  //  if(leaveRow.leaveStatus != ('cancelled' || 'rejected' ) && (timing<this.currentDate)){
  //     return true
  //  }

  
  // }

  showPopupCancel(leaveId) {
    swal({
      title: "Cancel Leave",
      text: "Are you sure you want to cancel your leave application?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes",
      cancelButtonText: "No",     

    }).then(
      (result) => {
        if (result) {
          this.loading = true;
          this._ss
            .cancelLeaveRequest(
              this.organizationId,
              this.studentId,
              leaveId,
              {}
            )
            .subscribe((data: any) => {
              if (data.IsSuccess) {
                this.getMyLeavesList();
              }
            });
        }
      },
      function (dismiss) {
        if (dismiss === "No") {
          // also handle 'close' or 'timer' if we used those
          // ignore
        } else {
          // throw dismiss;
          // ignore
        }
      }
    );
  }
  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
  }
  resetsearch() {
    this.searchParam = "";

    this.getMyLeavesList();
    this.isCrossEnable = false;
  }

  search() {
    this.getMyLeavesList();
  }
  changeLeaveType() {
    this.searchParam = $("#leaveType").val();
    this.getMyLeavesList();
  }
  changeLeaveStatus() {
    this.searchParam = $("#leaveStatus").val();
    this.getMyLeavesList();
  }
  onLeaveClick(leaveId){
    this.router.navigate(['student/specificLeave/',leaveId]);
  }
  

}
