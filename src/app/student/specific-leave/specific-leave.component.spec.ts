import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecificLeaveComponent } from './specific-leave.component';

describe('SpecificLeaveComponent', () => {
  let component: SpecificLeaveComponent;
  let fixture: ComponentFixture<SpecificLeaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecificLeaveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecificLeaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
