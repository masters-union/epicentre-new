import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';
declare var $, CKEDITOR;

@Component({
  selector: 'app-specific-leave',
  templateUrl: './specific-leave.component.html',
  styleUrls: ['./specific-leave.component.css']
})
export class SpecificLeaveComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: any;
  organizationId :any;
  userId: any;
  leaveId: any;
  userData: any;
  searchParam: String = "";
  rejectedReason: any;

  constructor(private cs: CookieServiceProvider,
              private route: ActivatedRoute,
              private router : Router,
              private studentService: StudentService ,
              private ts: ToasterService ) { }

  ngOnInit() {
    this.loading = true;
    this.organizationId = this.cs.getItem("Organization id");
    this.userId = this.cs.getItem('userId')
    this.leaveId = this.route.snapshot.paramMap.get('id');
    this.getSpecificStudentLeave()
  }

  getSpecificStudentLeave(){
    this.studentService.getStudentLeaveDetails(this.leaveId)
    .subscribe((data:any)=>{
      if(data.IsSuccess){
        this.userData = data.Data;
        console.log(data.Data);
        this.loading =false
      }else{
        this.ts.pop("error","","Somethimg Went Wrong")
      }
    })
  }
  viewUploadDoc(link) {
    window.open(link, "_blank");
  }
}
