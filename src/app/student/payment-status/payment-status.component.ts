import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';
import { environment } from "../../../environments/environment";
@Component({
  selector: 'app-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.css']
})
export class PaymentStatusComponent implements OnInit {
  paramesOrderId: any;
  paymentStatusData: any;
  student_id: any;
  paramesFeeDueId: any;
  loading: boolean;
  constructor(private _ss: StudentService, private route: ActivatedRoute, private cookie: CookieServiceProvider) { }

  ngOnInit() {
    window.console.log = function () { };
    this.loading = true;
    this.route.params.subscribe((params: Params) => {
      this.paramesOrderId = params['orderId'];
      this.paramesFeeDueId = params['studentFeeDueId']
    })
    this.student_id = this.cookie.getItem('id')
    this.getOrderStatus();
  }

  getOrderStatus() {
    let data = {
      orderId: this.paramesOrderId,
      studentFeeDueId: this.paramesFeeDueId
    }
    this._ss.getFeeOrderStatus(this.student_id, this.paramesOrderId).subscribe((res: any) => {
      console.log("getFeeOrderStatus is ", res)

      if (res.Data.status == 'OK') {
        this.paymentStatusData = res.Data;
        if (res.Data.orderStatus == 'PAID') {
          console.log("res.Data.status ", res.Data.status);
          this.loading = false;
          // this._ss.updateFeeAfterPayment(this.student_id, data).subscribe((res: any) => {
          //   this.loading = false;

          // })
        } else {
          console.log("oder status orderStatus active")
          this.loading = false;
        }
      }
      if (res.Data.status == 'ERROR') {
        this.loading = false;
        console.log("res.Data.status ", res.Data.status)
      }

    })
  }
}
