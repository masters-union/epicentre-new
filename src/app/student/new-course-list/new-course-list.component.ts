import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from '../service/student.service';
declare var $: any;
@Component({
  selector: 'app-new-course-list',
  templateUrl: './new-course-list.component.html',
  styleUrls: ['./new-course-list.component.css'],
  providers: [LoadscriptsService],
})
export class NewCourseListComponent implements OnInit {

  studentId: string;
  organizationId: string;
  loading: boolean = false;
  courseRoster = [];
  programId: string;
  isShow:boolean= false;
  searchParam: string ="";

  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.programId = this.cs.getItem("programBatchId");
    console.log("student id sis ",this.studentId);
    this.getStudentCourses();
  }
  isToggle(){
    this.isShow =!this.isShow
  }
  onCourseClick(courseId){
    console.log("course id",courseId)
    this.router.navigate(['student/newCourseDetails/',courseId]);
  }
  onCourseTypeSelect() {
    this.searchParam = $("#courseType").val();
    this.getStudentCourses();
  }
  onTermSelect() {
    this.searchParam = $("#term").val();
    this.getStudentCourses();
  }
  onModeSelect() {
    this.searchParam = $("#mode").val();
    this.getStudentCourses();
  }
  getStudentCourses() {
    this.loading = true;
    this._ss.getStudentCourses(this.studentId,this.programId,this.searchParam).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        console.log(data.Data);
        this.courseRoster = data.Data;
        console.log(this.courseRoster);
        this.isShow = true;
        // setTimeout(() => {
				// 	this.loadscriptsService.loadStuff();
				// }, 1000);
        // console.log("check data", data.Data);
      }else{
        this.loading = false;
        console.log("Error Occured, Please Check")
      }
    });
  }
}
