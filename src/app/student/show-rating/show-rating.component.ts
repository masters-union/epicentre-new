import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";

import { CookieServiceProvider } from '../../common/service/cookie.service';
import { StudentService } from '../service/student.service';
import { StarRatingComponent } from 'ng-starrating';
declare var $: any;


@Component({
  selector: 'app-show-rating',
  templateUrl: './show-rating.component.html',
  styleUrls: ['./show-rating.component.css']
})
export class ShowRatingComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean;
  mentorName: any;
  organizationId: string;
  mentorId: string;
  questionData: any;
  // ratingForm: FormGroup;
  studentId: any;
  // goals = [];
  // subRatingArr: any = [{ goals: this.goals }];
  ratingArrayData= []
  subRatingArrayData= []
  subRatingGoals= []
  // ratingArr: any = [{
  //   subRatingArray: this.subRatingArr,
  //   goals: this.goals
  // }];
  // goalIndex: number = 0;
  // questionIndex: any;



  constructor(
    private route: ActivatedRoute, private cs: CookieServiceProvider,
    private studentService: StudentService, private fb: FormBuilder, private router: Router,
    private ts: ToasterService

  ) { }

  ngOnInit() {
    window.console.log = function () { };
    this.route.params.subscribe((params: Params) => {
      this.mentorName = params['mentorName'];
      this.mentorId = params['id'];
      console.log(this.mentorId)
    })
    this.organizationId = this.cs.getItem('Organization id')
    this.studentId = this.cs.getItem('id')
    this.getRatingQuestions();
    // this.ratingInitForm();
  }

  // ratingInitForm() {
  //   this.ratingForm = this.fb.group({
  //     ratingArray: this.fb.array([this.ratingArray]),
  //     // ratingMasterId: "",
  //     // comment: "",
  //     // rating: "",
  //     // organizationId: this.organizationId,
  //     // studentId: this.studentId,
  //     // mentorId: this.mentorId,
  //     // hasGoals : ""
  //   })
  // }
  // get ratingArray(): FormGroup {
  //   return this.fb.group({
  //     ratingMasterId: "",
  //     comment: "",
  //     rating: "",
  //     organizationId: this.organizationId,
  //     studentId: this.studentId,
  //     mentorId: this.mentorId,
  //     hasGoals: ""
  //   });
  // }

  getRatingQuestions() {
    this.loading = true;

    // this.studentService.getRatedQuestion(this.organizationId, this.studentId).subscribe((res: any) => {
    //   // console.log("res ",res)
    //   if (res.IsSuccess) {
    //     this.loading = false;
    //     console.log("rating question ", res)
    //     this.questionData = res.Data
    //     this.ratingArr.length = this.questionData.length;

    //     this.questionData.forEach((element, i) => {
    //       this.ratingArr[i] = {
    //         comment: "",
    //         ratingMasterId: this.questionData[i].id,
    //         rating: "",
    //         organizationId: this.organizationId,
    //         studentId: this.studentId,
    //         mentorId: this.mentorId,
    //         hasGoals: false,
    //         subRatingQuesId: "",
    //         goals: []
    //       }
    //       if (element.SubRatings.length > 0) {
    //         element.SubRatings.forEach((subElement, j) => {
    //           this.subRatingArr[j] = {
    //             comment: "",
    //             ratingMasterId: this.questionData[i].id,
    //             subRatingQuesId: subElement.id,
    //             rating: "",
    //             organizationId: this.organizationId,
    //             studentId: this.studentId,
    //             mentorId: this.mentorId,
    //             hasGoals: false,
    //             goals: []
    //           }
    //         });
    //         this.ratingArr[i].subRatingArray = this.subRatingArr
    //       }
    //       // console.log("rating arr i ", i)
    //     });
    //     console.log("this.ratingArr length", this.ratingArr.length)
    //     console.log("this.ratingArr", this.ratingArr)
    //   } else {
    //     console.log("error ", res);
    //   }
    // })


    this.studentService.getStudentRating(this.organizationId, this.studentId, this.mentorId).subscribe((res: any) => {
      // console.log("res ",res)
      if (res.IsSuccess) {
        this.loading = false;
        // console.log("rating done ", res.Data)
        // let arrRes = res.Data
        // let unique = [];
        // arrRes.map(x=> unique.filter(a=> a.ratingMasterId == x.ratingMasterId).length> 0 ? null : unique.push(x));
        // console.log("filtered array ", unique)
        let subRateArray = []
        let arrResSub  = res.Data.filter(x => x.subRatingQuesId !=null)
        arrResSub.map(x=> subRateArray.filter(a=> a.subRatingQuesId == x.subRatingQuesId).length> 0 ? null : subRateArray.push(x));
        this.subRatingArrayData = subRateArray
        // let subgoals = []
        // subgoals=subRateArray[0].RatingMaster.Goals
        // this.subRatingGoals = subRateArray[0].RatingMaster.Goals
        // console.log(this.subRatingArrayData)
        // let arrRes  = res.Data.filter(x => x.subRatingQuesId ==null)
        // this.ratingArrayData = arrRes
        // console.log("filtered sub array ", this.subRatingArrayData)
        // console.log("filtered array ", this.ratingArrayData)
        let test =[]
        res.Data.map(x=> test.filter(a=> a.subRatingQuesId == x.subRatingQuesId && x.subRatingQuesId !==null).length> 0 ? null : test.push(x));
        console.log(test)
        this.ratingArrayData = test
      } else {
        console.log("error ", res);
      }
    })
  }


}
