import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursePreferenceDetailsComponent } from './course-preference-details.component';

describe('CoursePreferenceDetailsComponent', () => {
  let component: CoursePreferenceDetailsComponent;
  let fixture: ComponentFixture<CoursePreferenceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursePreferenceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursePreferenceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
