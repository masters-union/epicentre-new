import { Component, OnInit} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToasterConfig, ToasterService } from "angular2-toaster";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from "../service/student.service";
@Component({
  selector: 'app-course-preference-details',
  templateUrl: './course-preference-details.component.html',
  styleUrls: ['./course-preference-details.component.css'],
  providers: [LoadscriptsService],

})
export class CoursePreferenceDetailsComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean;
  courseId:any
  studentId:string
  organizationId:String
  numberOfPreferences:number;
  coursesData=[]

  constructor(
    private studentService: StudentService,
    private cs: CookieServiceProvider,
    private route: ActivatedRoute,
    private ts: ToasterService,
    private router: Router,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.organizationId = this.cs.getItem('orgId')
    this.courseId = this.route.snapshot.paramMap.get('id');
    this.studentId = this.route.snapshot.paramMap.get('studentId');
    this.loadscriptsService.loadStuff();
    this.getCourses();
  }

  getCourses() {
    this.loading = true;
    this.studentService.fetchCoursePreferenceList(this.courseId,this.studentId).subscribe((data:any) => {
      if(data.IsSuccess){
        this.loading = false;
        this.loadscriptsService.loadStuff();
        this.coursesData=data.Data;  
        console.log("Data recieved is ",data.Data);
        
        this.numberOfPreferences= data.Data.length
        // console.log("No. of preferences are ",this.numberOfPreferences);
        
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    })
  }
}
