import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  studentId: string;
  organizationId: string;
  loading : boolean = false;
  courseData =[]
  pendingCourses=[]
  completedCourses=[]
  coursePreference =[]
  todayDate: any
  constructor(
    private route: ActivatedRoute, private cs: CookieServiceProvider,
    private _ss: StudentService, private router: Router,
    private ts: ToasterService
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem('id')
    this.organizationId = this.cs.getItem('orgId')
    this.getCourses();
  }
  onCourseClick(courseId){
    this.router.navigate(['/student/coursePreference',courseId]);
  }
  getSpecificCourseDetails(courseId,studentId){
    this.router.navigate(['/student/coursePreferencePreview',courseId,studentId])
  }
  getCourses() {
    this.loading = true;
    this._ss.getCoursesList(this.studentId).subscribe((data:any) => {
      if(data.IsSuccess){
        this.loading = false;
        this.courseData = data.Data;
        this.courseData.forEach((element)=>{
            if(element.isAttempted){
              this.completedCourses.push(element);
            }
            else{
              this.pendingCourses.push(element);
            }
        });
       
        this.todayDate = new Date()
        this.todayDate = this.todayDate.toISOString()
        this.todayDate = this.todayDate.split('T')[0];
      }
    })
  }

}
