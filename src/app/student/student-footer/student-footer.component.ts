import { Component, OnInit } from '@angular/core';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { StudentService } from '../service/student.service';
declare var swal;


@Component({
  selector: 'app-student-footer',
  templateUrl: './student-footer.component.html',
  styleUrls: ['./student-footer.component.css'],
  providers: [LoadscriptsService]

})
export class StudentFooterComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  title: any
  saveNote: boolean = false
  description: any
  organizationId: any
  student_id: any
  notes = []
  updateTitle: boolean = true
  update: boolean = false
  id: any
  inputClick: boolean = false
  showTitle: boolean = true
  titleShow: any
  showBack: boolean = true
  loading: any



  constructor(private loadscriptsService: LoadscriptsService, private _cs: CookieServiceProvider, private ts: ToasterService, private _ss: StudentService) { }

  ngOnInit() {
    window.console.log = function () { };
    this.loadscriptsService.loadNotePad()

    this.organizationId = this._cs.getItem("orgId");
    this.student_id = this._cs.getItem('id')
    this.getSpecificNotes()

  }
  getStudentDetails() {
    this.loading = true
    this._ss.getStudentProfileDetails(this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false
      }
    })
  }
  notepadPopUp() {
    this.loadscriptsService.loadNotePad()
  }
  titleSave() {
    let tit = this.title.charCodeAt(0)
    if (tit == 32 || this.title == "") {
      this.saveNote = false
    } else {
      this.saveNote = true
      this.inputClick = false
      this.showTitle = true
      this.update = false
    }




  }
  saveNotes() {
    let object = {
      title: this.title,
      description: this.description,
      studentId: this.student_id,
      organizationId: this.organizationId,
      id: this.id

    }
    this._ss.saveNotes(this.organizationId, this.student_id, object).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.saveNote = false
        this.title = ""
        this.description = ""
        this.ts.pop("success", "", "Note Added");
        this.getSpecificNotes()
      }
    })
  }
  getSpecificNotes() {
    this._ss.getSpecifcNotes(this.organizationId, this.student_id).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.notes = data.Data
      }
    })
  }
  editNote(data) {
    this.title = data.title,
      this.description = data.description
    this.id = data.id
    this.updateTitle = false
  }
  deleteNote(id) {
    swal({
      title: "DELETE NOTE",
      // html: '<div>Line0<br />Line1<br /></div>',
      text: "Are you Sure you Want to Delete Note?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes Delete"
    }).then(result => {
      if (result) {
        if (id) {
          let object = {
            id: id
          }
          this._ss.deleteNotes(this.organizationId, id, object).subscribe((data: any) => {
            if (data.IsSuccess) {
              this.ts.pop("success", "", "Note Deleted");
              this.getSpecificNotes()
            } else {
              this.ts.pop("error", "", "something went wrong");
            }
          }, error => {
            this.ts.pop("error", "", "Soemthing went Wrong ");
          })
        }
      }

    },
      function (dismiss) {
        if (dismiss === 'cancel') {
          // ignore
          console.log("dismiss", dismiss)
        } else {
          // throw dismiss;
          // ignore
        }
      });

  }
  Enter(event) {
    if (event.keyCode == 13 && this.updateTitle == true
    ) {
      this.titleSave()
    }
    if (event.keyCode == 13 && this.updateTitle == false) {
      this.UpdateTitle()
    }
  }
  UpdateTitle() {
    this.saveNote = true
    this.update = true
    this.inputClick = false
    this.showBack = false
    this.showTitle = true


  }
  UpdateDescription() {
    let object = {
      studentId: this.student_id,
      title: this.title,
      description: this.description,
      organizationId: this.organizationId,
      id: this.id
    }
    console.log(object)
    this._ss.updateNote(this.organizationId, this.student_id, object).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.saveNote = false
        this.title = ""
        this.description = ""
        this.updateTitle = true
        this.ts.pop("success", "", "Note Updated");
        this.getSpecificNotes()
      }
    })
  }
  onInputTitle(data) {
    this.titleShow = data.title
    this.description = data.description
    this.saveNote = true
    this.inputClick = true
    this.showTitle = false
  }
  onBack() {
    this.saveNote = false
    this.title = ""
    this.description = ""
    this.updateTitle = true

  }
  onBack1() {
    console.log("m here")
    this.saveNote = false
    this.title = ""
    this.description = ""
    this.updateTitle = true
  }


}
