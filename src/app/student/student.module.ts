import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FullCalendarModule } from "@fullcalendar/angular"; // the main connector. must go first
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction"; // a plugin
import { StudentRoutingModule } from "./student-routing.module";
import { StudentPortalComponent } from "./student-portal/student-portal.component";
import { CookieServiceProvider } from "../common/service/cookie.service";
import { AuthenticateModule } from "../authenticate/authenticate.module";
import { StudentHeaderComponent } from "./student-header/student-header.component";
import { StudentService } from "./service/student.service";
import { AuthGuard } from "../common/service/auth.guard";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { myInterceptor } from "../common/service/interceptor.service";
import { EnquiryPortalComponent } from "./enquiry-portal/enquiry-portal.component";
import { StudentMenuComponent } from "./student-menu/student-menu.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ToasterService, ToasterModule } from "angular2-toaster";
import { TransactionHistoryComponent } from "./transaction-history/transaction-history.component";
import { ProfileComponent } from "./profile/profile.component";
import { FeeSlipComponent } from "./fee-slip/fee-slip.component";
import { InboxComponent } from "./inbox/inbox.component";
// import {
//   SocialLoginModule,
//   AuthServiceConfig,
//   GoogleLoginProvider,
// } from "angularx-social-login";
import { PaymentStatusComponent } from "./payment-status/payment-status.component";
import { MentorRatingComponent } from "./mentor-rating/mentor-rating.component";
import { AssignMentorsComponent } from "./assign-mentors/assign-mentors.component";
import { RatingModule } from "ng-starrating";
import { StudentFooterComponent } from "./student-footer/student-footer.component";
import { ShowRatingComponent } from "./show-rating/show-rating.component";
import { DatePickerModule } from "@syncfusion/ej2-angular-calendars";
import { StudentResumeComponent } from "./student-resume/student-resume.component";
import { RefundPolicyComponent } from "./refund-policy/refund-policy.component";
import { SurveyListComponent } from "./survey-list/survey-list.component";
import { SurveyDetailComponent } from "./survey-detail/survey-detail.component";
import { CashfreeDetailsComponent } from "./cashfree-details/cashfree-details.component";
import { ViewEventsComponent } from "./events/view-events/view-events.component";
import { EventDetailComponent } from "./events/event-detail/event-detail.component";
import { EventClubDetailComponent } from "./events/event-club-detail/event-club-detail.component";
import { EnquiryConversationComponent } from "./enquiry-conversation/enquiry-conversation.component";

import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { ViewCoursesComponent } from "./course-bidding/view-courses/view-courses.component";
import { BidDetailComponent } from "./course-bidding/bid-detail/bid-detail.component";
import { CourseDetailComponent } from "./course-bidding/course-detail/course-detail.component";
import { EditBidComponent } from "./course-bidding/edit-bid/edit-bid.component";
import { LeaveApplicationFormComponent } from "./leave-application-form/leave-application-form.component";
import { LeaveListComponent } from "./leave-list/leave-list.component";
import { CalendarModule } from "@syncfusion/ej2-angular-calendars";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { EventListComponent } from './studentEvent/event-list/event-list.component';
import { EventPreviewComponent } from './studentEvent/event-preview/event-preview.component';
import { CreateClubComponent } from './studentClub/create-club/create-club.component';
import { ViewClubComponent } from "./studentClub/view-club/view-club.component";
import { ClubStatusComponent } from './studentClub/club-status/club-status.component';
import { EditClubComponent } from "./studentClub/edit-club/edit-club.component";
import { ClubDetailComponent } from './studentClub/club-detail/club-detail.component';
import { EventStatusComponent } from './studentEvent/event-status/event-status.component';
import { EventDetailsComponent } from './studentEvent/event-details/event-details.component';
import { CreateEventComponent } from "./studentEvent/create-event/create-event.component";
import { EditEventComponent } from "./studentEvent/edit-event/edit-event.component";
import { SpecificLeaveComponent } from './specific-leave/specific-leave.component';
import { CarouselModule } from "ngx-owl-carousel-o";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CourseListComponent } from './course-list/course-list.component';
import { CoursePreferenceComponent } from './course-preference/course-preference.component';
import { CoursePreferenceDetailsComponent } from './course-preference-details/course-preference-details.component';
import { TermWiseMarksComponent } from './course-assingment/term-wise-marks/term-wise-marks.component';
import { CourseWiseMarksComponent } from './course-assingment/course-wise-marks/course-wise-marks.component';
import { DownloadTranscriptComponent } from './download-transcript/download-transcript.component';
import { TranscriptDetailsComponent } from './transcript-details/transcript-details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NewCourseListComponent } from './new-course-list/new-course-list.component';
import { NewMyCourseComponent } from './new-my-course/new-my-course.component';
import { NewBiddingListComponent } from './new-bidding-list/new-bidding-list.component';
import { NewCourseDetailsComponent } from './new-course-details/new-course-details.component';
import { NewDashboardComponent } from './new-dashboard/new-dashboard.component';
import { NewElectiveBiddingComponent } from './new-elective-bidding/new-elective-bidding.component';
import { StudentAttendaceComponent } from './student-attendace/student-attendace.component';
import { CoacheeSessionComponent } from './coachee-session/coachee-session.component';

const google_oauth_client_id: string =
  "762205953238-gjf9ju2l5le6p46cbcr492vt01o272v9.apps.googleusercontent.com";
// let config = new AuthServiceConfig([
//   {
//     id: GoogleLoginProvider.PROVIDER_ID,
//     provider: new GoogleLoginProvider(google_oauth_client_id),
//   },
// ]);

FullCalendarModule.registerPlugins([
  // register FullCalendar plugins
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin,
]);

// export function provideConfig() {
//   return config;
// }

@NgModule({
  declarations: [
    StudentPortalComponent,
    StudentHeaderComponent,
    EnquiryPortalComponent,
    StudentMenuComponent,
    TransactionHistoryComponent,
    ProfileComponent,
    FeeSlipComponent,
    InboxComponent,
    PaymentStatusComponent,
    MentorRatingComponent,
    AssignMentorsComponent,
    StudentFooterComponent,
    ShowRatingComponent,
    StudentResumeComponent,
    RefundPolicyComponent,
    SurveyListComponent,
    SurveyDetailComponent,
    CashfreeDetailsComponent,
    ViewEventsComponent,
    EventDetailComponent,
    EventClubDetailComponent,
    EnquiryConversationComponent,
    ViewCoursesComponent,
    BidDetailComponent,
    CourseDetailComponent,
    EditBidComponent,
    LeaveApplicationFormComponent,
    LeaveListComponent,
    CreateClubComponent,
    EventListComponent,
    EventPreviewComponent,
    ViewClubComponent,
    ClubStatusComponent,
    EditClubComponent,
    ClubDetailComponent,
    EventStatusComponent,
    EventDetailsComponent,
    CreateEventComponent,
    EditEventComponent,
    SpecificLeaveComponent,
    CourseListComponent,
    CoursePreferenceComponent,
    CoursePreferenceDetailsComponent,
    TermWiseMarksComponent,
    CourseWiseMarksComponent,
    DownloadTranscriptComponent,
    TranscriptDetailsComponent,
    DashboardComponent,
    NewCourseListComponent,
    NewMyCourseComponent,
    NewBiddingListComponent,
    NewCourseDetailsComponent,
    NewDashboardComponent,
    NewElectiveBiddingComponent,
    StudentAttendaceComponent,
    CoacheeSessionComponent,
  ],
  imports: [
    CommonModule,
    StudentRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ToasterModule,
    // SocialLoginModule,
    RatingModule,
    DatePickerModule,
    FullCalendarModule,
    CKEditorModule,
    CalendarModule,
    NgMultiSelectDropDownModule,
    // BrowserAnimationsModule,
    CarouselModule
  ],
  providers: [
    ToasterService,
    AuthGuard,
    CookieServiceProvider,
    StudentService,
    { provide: HTTP_INTERCEPTORS, useClass: myInterceptor, multi: true },
    // {
    //   provide: AuthServiceConfig,
    //   useFactory: provideConfig,
    // },
  ],
})
export class StudentModule { }
