import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoacheeSessionComponent } from './coachee-session.component';

describe('CoacheeSessionComponent', () => {
  let component: CoacheeSessionComponent;
  let fixture: ComponentFixture<CoacheeSessionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoacheeSessionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoacheeSessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
