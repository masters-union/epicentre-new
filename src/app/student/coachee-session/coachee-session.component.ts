import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToasterService } from "angular2-toaster";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from "../service/student.service";
@Component({
  selector: 'app-coachee-session',
  templateUrl: './coachee-session.component.html',
  styleUrls: ['./coachee-session.component.css'],
  providers: [LoadscriptsService],

})
export class CoacheeSessionComponent implements OnInit {
  organizationId: string;
	studentId: string;
	loading : boolean = false;
	userList=[]
  user:any

  constructor(		private route: ActivatedRoute,
		private cs: CookieServiceProvider,
		private _ss: StudentService,
		private router: Router,
		private ts: ToasterService,
		private loadscriptsService: LoadscriptsService
) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
		this.organizationId = this.cs.getItem("orgId");
    this.getUserList()
  }
  getUserList() {
		this.loading =true;
		this._ss.getUserList(this.organizationId,this.studentId).subscribe((data: any) => {
			if(data.IsSuccess){
				this.loading = false;
				console.log(data.Data);
				this.userList = data.Data;
        this.loadscriptsService.loadStuff();

		}
	})
  }
  onCoachSelect(userId){
window.location.href=`https://epicenter.mastersunion.org/meetingList?studentId=${this.studentId}&userId=${userId}`
  }
}
