import { Component, OnInit } from "@angular/core";

// import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StudentService } from "../service/student.service";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import * as moment from "moment";

@Component({
  selector: "app-enquiry-conversation",
  templateUrl: "./enquiry-conversation.component.html",
  styleUrls: ["./enquiry-conversation.component.css"],
  providers: [LoadscriptsService],
})
export class EnquiryConversationComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  enquiryId: String = "";
  orgStudentId: String = "";
  loading: boolean;
  enquiryList = [];
  org_id: any;
  reply: any;
  details: any;
  student_id: any;
  constructor(
    private studentService: StudentService,
    private fb: FormBuilder,
    private cookie: CookieServiceProvider,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    window.console.log = function () { };
    this.org_id = this.cookie.getItem("orgId");

    this.route.params.subscribe((params: Params) => {
      this.enquiryId = params["enqId"];
      this.orgStudentId = params["orgStuId"];
    });

    this.getDetails();
  }

  getStudentConversationData() {
    // console.log("fefeeffe", enquiryId);
    this.loading = true;
    this.studentService
      .getspecificStudentResponse(this.enquiryId, this.orgStudentId)
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          //console.log("check data", res);

          this.enquiryList = res.Data;
          // this.ts.pop("success", "Updated Successfully");
          // this.router.navigate([
          //   "/student/enquiryConversation",
          //   enquiryId,
          //   orgStudentId,
          // ]);

          // if (res.Data.length > 4) {
          setTimeout(() => {
            this.loading = false;
            window.scrollTo(
              0,
              document.body.scrollHeight ||
                document.documentElement.scrollHeight
            );
          }, 1000);
          // } else {
          //   this.loading = false;
          //   window.scrollTo(
          //     0,
          //     document.body.scrollHeight ||
          //       document.documentElement.scrollHeight
          //   );
          // }
        } else {
          this.loading = false;
        }
      });
  }
  getDetails() {
    this.loading = true;
    this.studentService
      .getSpecifcStudentEnquiryDetails(this.orgStudentId, this.enquiryId)
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          //console.log("check data", res);

          this.details = res.Data;

          // document.getElementById('text-box-reply').focus();

          // this.ts.pop("success", "Updated Successfully");
          // this.router.navigate([
          //   "/student/enquiryConversation",
          //   enquiryId,
          //   orgStudentId,
          // ]);
          // this.loading = false;

          this.getStudentConversationData();
        } else {
          this.loading = false;
        }
      });
  }
  sendReply() {
    let rep = this.reply.charCodeAt(0);
    if (rep == 32 || this.reply == "") {
      this.ts.pop("error", "", "Please Enter Something");
    } else {
      let object = {
        organizationId: this.org_id,
        organizationStudentId: this.orgStudentId,
        // userResponseId: this.userId,
        enquiryId: this.enquiryId,
        studentReply: this.reply,
        studentReplyDate: moment(),
      };
      //console.log("ob", object);
      this.loading = true;
      this.studentService
        .userResponseToStudent(this.orgStudentId, object)
        .subscribe((res: any) => {
          if (res.IsSuccess) {
            this.loading = false;
            this.reply = "";
            this.ts.pop("success", "", "Reply Updated");
            this.getStudentConversationData();
          } else {
            //console.log("error ", res);
            this.loading = false;
          }
        });
    }
  }
  unSatisfied(value) {
    let object = {
      studentFeedback: value,
    };
    this.loading = true;
    this.studentService
      .studentFeedback(this.orgStudentId, this.enquiryId, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Feedback Updated");
          // this.router.navigate(['student/enquiry'])
          this.getDetails();
          // this.loading = false;
        } else {
          this.loading = false;
        }
      });
  }
  neutral(value) {
    let object = {
      studentFeedback: value,
    };
    this.loading = true;
    this.studentService
      .studentFeedback(this.orgStudentId, this.enquiryId, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Feedback Updated");
          this.getDetails();
          // this.router.navigate(['student/enquiry'])
          // this.loading = false;
        } else {
          this.loading = false;
        }
      });
  }
  satisfied(value) {
    let object = {
      studentFeedback: value,
    };
    this.loading = true;
    this.studentService
      .studentFeedback(this.orgStudentId, this.enquiryId, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Feedback Updated");
          // this.router.navigate(['student/enquiry'])
          this.getDetails();
          // this.loading = false;
        } else {
          this.loading = false;
        }
      });
  }
}
