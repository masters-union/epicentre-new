import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryConversationComponent } from './enquiry-conversation.component';

describe('EnquiryConversationComponent', () => {
  let component: EnquiryConversationComponent;
  let fixture: ComponentFixture<EnquiryConversationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryConversationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryConversationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
