import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewElectiveBiddingComponent } from './new-elective-bidding.component';

describe('NewElectiveBiddingComponent', () => {
  let component: NewElectiveBiddingComponent;
  let fixture: ComponentFixture<NewElectiveBiddingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewElectiveBiddingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewElectiveBiddingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
