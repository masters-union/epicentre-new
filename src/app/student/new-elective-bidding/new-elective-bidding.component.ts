import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CalendarOptions } from '@fullcalendar/angular';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import { StudentService } from '../service/student.service';
import * as moment from 'moment';
import { element } from 'protractor';

declare var $: any;

@Component({
  selector: 'app-new-elective-bidding',
  templateUrl: './new-elective-bidding.component.html',
  styleUrls: ['./new-elective-bidding.component.css'],
  providers: [LoadscriptsService]
})
export class NewElectiveBiddingComponent implements OnInit {

  studentId: string;
  organizationId: string;
  loading: boolean = false;
  electiveCourse = [];
  programId: string;
  allSessions = [];

  allSessions1 = [];
  calendarOptions: CalendarOptions;
  courseName : any;
  today = new Date();
  initialized = false;
  sessionName : any;
  courseCode : any;
  courseCount : any = 0;
  description: any;
  clashedCourse: any = [];
  clashed1: any = [];
  hoursArr = ["0","0"];

  constructor(
    private route: ActivatedRoute,
    private cs: CookieServiceProvider,
    private _ss: StudentService,
    private router: Router,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
  ) { }

  ngOnInit() {
    this.studentId = this.cs.getItem("id");
    this.organizationId = this.cs.getItem("orgId");
    this.programId = this.cs.getItem("programBatchId");
    this.getAllElectiveCourse();
    this.getAllStudentCourse();
    this.nextPrevButton();
    this.monthDayWeekButton();
  }

  

  getAllElectiveCourse() {
    this.loading = true;
    this._ss.getAllElectiveCourse(this.organizationId, this.studentId, this.programId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let arr = res.Data;
        this.electiveCourse = arr;
      }
      else {
        this.loading = false;
      }
    })
  }
  onCourseClick(courseId){
    console.log("course id",courseId)
    this.router.navigate(['student/newCourseDetails/',courseId]);
  }
  getAllStudentCourse() {
    this.loading = true;
    this._ss.getCourseByStudentId(this.organizationId, this.studentId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let arr = res.Data;
        arr.forEach((e) => {
          // console.error(e.CourseRoster)
          if (e.CourseRoster && e.CourseRoster.CourseSessions) {
            e.CourseRoster.CourseSessions.forEach(element => {
              element.courseCode = e.CourseRoster.courseCode;
              element.courseName = e.CourseRoster.courseName;
              this.allSessions.push(element)
            });
          }
        })
        this.allSessions.forEach(element => {
          let timeArr = element.fromTime.split(":")
          element.date = new Date(element.date);
          element.date.setHours(+timeArr[0],+timeArr[1], 0, 0)
        });
        // console.error(this.allSessions)
        this.setEvents()
      }
      else {
        this.loading = false;
      }
    })
  }

  setEvents() {
    this.allSessions.forEach((o) => {

      o.title = o.courseCode;
      o.name = o.name;
      o.description = o.description;
      o.courseName = o.courseName;
      let startDate = new Date(o.date);
      let endDate = new Date(o.date);
      let timeArr1 = o.fromTime.toString().split(":");
      let timeArr2 = o.toTime.toString().split(":");
     
        
      startDate.setHours(+timeArr1[0],+timeArr1[1], 0, 0)
      endDate.setHours(+timeArr2[0],+timeArr2[1], 0, 0)
      o.start = moment(startDate).format();
      o.end = moment(endDate).format();
      
      if(this.today > new Date(o.date)) {
        o.color = "lightgray";
      } else {
        o.color = o.color || "#229E6D"
      }
    });

    this.allSessions1 = this.allSessions.map(({ title, date, id, start, end, name, description, color, courseName}) => ({ title, date, id, start, end, name, description, color, courseName}))
    this.initializeCalendar(this.allSessions1);
  }

  initializeCalendar(e) {
    this.calendarOptions = {
      initialView: 'timeGridWeek',
      // dateClick: this.handleDateClick.bind(this),
      events: e,
      eventClick: this.showModal.bind(this),
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'timeGridWeek,timeGridDay'
      },
      views: {
        timeGridDay: {
          titleFormat: { year: 'numeric', month: '2-digit', day: '2-digit' },
          // columnHeaderFormat: { year: 'numeric', month: 'long', day: '2-digit', weekday:'long' } //Optional - for column headers
        },
        timeGridWeek: {
          // titleFormat: { year: 'numeric', month: 'numeric', day: '2-digit' },
          columnHeaderFormat: { year: 'numeric', month: 'long', day: '2-digit', weekday:'long' } //Optional - for column headers
        },
      },
      
      // slotDuration: "00:60:00",
      slotMinTime: "07:00:00",
      slotMaxTime: "20:00:00",
      eventBackgroundColor: "transparent",
      eventBorderColor: "transparent",
      // defaultAllDay: false
    };

    this.initialized = true;

    setTimeout(() => {
      $("table.fc-scrollgrid").parent("div.fc-view").addClass("fc-timegrid");
      $("table.fc-scrollgrid").parent("div.fc-view").addClass("fc-timeGridWeek-view");
    }, 100);
    setTimeout(() => {
      $(".fc-dayGridMonth-button").removeClass("fc-button-active");
    }, 100);
  
    setTimeout(() => {
      $(".fc-header-toolbar.fc-toolbar .fc-toolbar-title").appendTo("fc-header-toolbar.fc-toolbar .fc-toolbar-chunk:nth-child(1) .fc-button-group");
    }, 100);
    setTimeout(() => {
      $("tr.fc-scrollgrid-section .fc-scrollgrid-section-body").addClass("hide");
    }, 100);

  }

  nextPrevButton() {
    setTimeout(() => {
        $('body').on('click', 'button.fc-prev-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event-harness").removeClass("active-bg-green");
                }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event.fc-event-past").parents("div.fc-timegrid-event-harness").addClass("past-bg-pink");
                }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event-harness").addClass("past-bg-pink");
                // }, 100);
              } else {
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-future").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-today").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
              }
            });
        });
     
        $('body').on('click', 'button.fc-next-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
              }, 100);
              // setTimeout(() => {
              //   $(".fc-timegrid-event-harness").addClass("past-bg-pink");
              // }, 100);
              setTimeout(() => {
                $(".fc-timegrid-event.fc-event-past").parents("div.fc-timegrid-event-harness").addClass("past-bg-pink");
              }, 100);
              } else {
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-future").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-today").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
              }
            });
        });
    }, 100);
  }

  monthDayWeekButton() {
    setTimeout(() => {
        $('body').on('click', 'button.fc-dayGridDay-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event-harness").addClass("past-bg-pink");
                // }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event.fc-event-past").parents("div.fc-timegrid-event-harness").addClass("past-bg-pink");
                }, 100);
              } else {
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-future").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-today").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
              }
            });
        });
     
        $('body').on('click', 'button.fc-dayGridMonth-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
                }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event-harness").addClass("past-bg-pink");
                // }, 100);
                setTimeout(() => {
                  $(".fc-timegrid-event.fc-event-past").parents("div.fc-timegrid-event-harness").addClass("past-bg-pink");
                }, 100);
                
              } else {
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-future").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-today").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
              }
                
            });
        });
        $('body').on('click', 'button.fc-timeGridWeek-button', () => {
            this.allSessions.forEach( (o) => {
              if(this.today > new Date(o.date)) {
                setTimeout(() => {
                  $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
              }, 100);
              // setTimeout(() => {
              //   $(".fc-timegrid-event-harness").addClass("past-bg-pink");
              // }, 100);
              setTimeout(() => {
                $(".fc-timegrid-event.fc-event-past").parents("div.fc-timegrid-event-harness").addClass("past-bg-pink");
              }, 100);
              } else {
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-future").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-future").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
                // setTimeout(() => {
                //   $(".fc-timegrid-event.fc-event-today").parents("div.fc-timegrid-event-harness").addClass("active-bg-green");
                // }, 100);
              }
                
            });
        });
    }, 100);
  }

  showModal(arg) {
      // this.sessionName = arg.event.title;
      // this.courseCode = arg.event.extendedProps.courseCode;
      this.courseCode = arg.event.title;
      this.courseName = arg.event.extendedProps.courseName;
      this.sessionName = arg.event.extendedProps.name;
      this.description = arg.event.extendedProps.description;
      document.getElementById("description").innerHTML= arg.event.extendedProps.description;
      $('#exampleModal').modal('show')
  }

  checkClicked(e, courseId) {
    if(e) {
      this.courseCount = this.courseCount+1;
      this.loading = true;
      this._ss.getAllElectiveSession(this.organizationId, this.studentId, courseId).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          let data = res.Data;
            if (data.CourseSessions) {
              let color ="#" + ((1<<24)*Math.random() | 0).toString(16)
              data.CourseSessions.forEach(element => {
                element.courseCode = data.courseCode;
                element.color = color;
                this.allSessions.push(element)
              });
            }
          let arr = this.allSessions
          this.setEvents()
          // console.log(arr)
          this.clashedCourse = [];
          arr.forEach(element => {
            let timeArr = element.fromTime.split(":")
            element.date = new Date(element.date);
            element.date.setHours(+timeArr[0],+timeArr[1], 0, 0)
          });
          arr.forEach(element => {
            if(moment(element.date) >= moment()){
              this.clashedCourse.push(element)
            }
          });
          this.clashedCourse.sort((a,b)=>new Date(a.date).getTime()-new Date(b.date).getTime());
          // const lookup = this.clashedCourse.reduce((a, e) => {
          //   a[e.date] = ++a[e.date] || 0;
          //   return a;
          // }, {});
          // this.clashedCourse = this.clashedCourse.filter(e => lookup[e.date]);
          let result = [];
          let durationArr = [];
          for (var i=0, iLen=this.clashedCourse.length - 1; i<iLen; i++) {
            let a = this.clashedCourse[i];
            let b = this.clashedCourse[i + 1];
        
            if ((a.fromTime <= b.fromTime && a.toTime > b.fromTime && new Date(a.date).getDate()==new Date(b.date).getDate()) ||
                (a.fromTime < b.toTime && a.toTime >= b.toTime && new Date(a.date).getDate()==new Date(b.date).getDate()) ) {
                result.push(a, b);
                durationArr.push([a,b]);
            }
          }
          let count = 0
          durationArr.forEach(element => {
            for (var i=0, iLen=element.length - 1; i<iLen; i++) {
              let a = element[i];
              let b = element[i + 1];
          
              count = count + Math.abs(this.strToMins(b.fromTime) - this.strToMins(a.toTime));
            }
          });
          let hours = this.convertMinsToHrsMins(count)
          this.hoursArr = hours.toString().split(":");
          this.clashedCourse = result;
        }
        else {
          this.loading = false;
        }
      })
    }
    else {
      this.loading = true;
      this.courseCount = this.courseCount-1;
      this._ss.getAllElectiveSession(this.organizationId, this.studentId, courseId).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          let data = res.Data;
          let arr = data.CourseSessions
          arr.forEach(element => {
            let timeArr = element.fromTime.split(":")
            element.date = new Date(element.date);
            element.date.setHours(+timeArr[0],+timeArr[1], 0, 0);
            element.color = "";
          });
            if (arr.length>0) {
              this.allSessions = this.allSessions.filter(element => !arr.find(e => (e.id === element.id) ))
            }
          this.setEvents()
          this.clashedCourse = [];
          let arr1 = this.allSessions
          arr1.forEach(element => {
            if(moment(element.date) >= moment()){
              this.clashedCourse.push(element)
            }
          });
          this.clashedCourse.sort((a,b)=>new Date(a.date).getTime()-new Date(b.date).getTime());
          // const lookup = this.clashedCourse.reduce((a, e) => {
          //   a[e.date] = ++a[e.date] || 0;
          //   return a;
          // }, {});
          // this.clashedCourse = this.clashedCourse.filter(e => lookup[e.date]);
          let result = [];
          let durationArr = [];
          for (var i=0, iLen=this.clashedCourse.length - 1; i<iLen; i++) {
            let a = this.clashedCourse[i];
            let b = this.clashedCourse[i + 1];
        
            if ((a.fromTime <= b.fromTime && a.toTime > b.fromTime && new Date(a.date).getDate()==new Date(b.date).getDate()) ||
                (a.fromTime < b.toTime && a.toTime >= b.toTime && new Date(a.date).getDate()==new Date(b.date).getDate()) ) {
                result.push(a, b);
                durationArr.push([a,b]);
            }
          }
          let count = 0
          durationArr.forEach(element => {
            for (var i=0, iLen=element.length - 1; i<iLen; i++) {
              let a = element[i];
              let b = element[i + 1];
          
              count = count + Math.abs(this.strToMins(b.fromTime) - this.strToMins(a.toTime));
            }
          });
          let hours = this.convertMinsToHrsMins(count)
          this.hoursArr = hours.toString().split(":");
          

          this.clashedCourse = result;
        }
        else {
          this.loading = false;
        }
      })
    }
  }

  showClash() {
    $('#exampleModal1').modal('show')
  }

  strToMins(t) {
    var s = t.split(":");
    return Math.abs(Number(s[0]) * 60 + Number(s[1]));
  }

  minsToStr(t) {
    return Math.trunc(t / 60)+':'+('00' + t % 60).slice(-2);
  }

  convertMinsToHrsMins = (mins) => {
    let h = Math.floor(mins / 60);
    let m = mins % 60;
    h = h < 10 ? 0 + h : h;
    m = m < 10 ? 0 + m : m;
    return `${h}:${m}`;
  }

}
