import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../service/student.service';
import {LoadscriptsService } from '../../common/service/loadscripts.service';
declare var $;
@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css'],
  providers: [LoadscriptsService]
})
export class InboxComponent implements OnInit {

  loading: boolean
  constructor(
    private _ss: StudentService,
    private router: Router,
    private cookie: CookieServiceProvider,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService

  ) { }
  
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  student_id: any;
  organization_id: any;
  pageNo: number = 1;
  pageSize: any = 50;
  pageCount: number = 1;
  recordCount: number = 0;
  allCount: number = 0;
  fetchedData=[];
  messageId: any;
  searchParam: String = "";
  isCrossEnable:boolean;
  sortBy ="dsc";
  selectAll: boolean = false
  checkAll: boolean = false;
  messageIds = []


  ngOnInit() {
    window.console.log = function () { };
    this.student_id = this.cookie.getItem('id');
    this.organization_id = this.cookie.getItem('orgId');
    this.fetchDetails();
   
   
  }

  fetchDetails() {
    this.loading = true;
    this._ss.fetchAllMessage( this.student_id, this.organization_id, this.searchParam, this.pageNo, this.pageSize).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let d = res.Data ;
        if(this.sortBy == "dsc"){
          d.sort((a,b)=> new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())
          this.fetchedData = d
        }
        else if(this.sortBy == "asc"){
          d.sort((a,b)=> new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime())
          this.fetchedData = d
        }
       
        // console.log(res)
          setTimeout(() => {
            this.loadscriptsService.loadStuff()
          }, 100);
        this.pageCount = Math.ceil(res.count / this.pageSize);
        this.recordCount = this.fetchedData.length;
        this.allCount = res.count
        this.setPage()
        this.messageIds = [];
      } else {
        // console.log("error ", res);
      }
    })
  }
  selectedAll() {
  
    this.checkAll = true
    this.messageIds =[]
    this.fetchedData.forEach(msg => {
      this.messageIds.push(msg.id)
    });
  }
  unselectedAll() {
    this.checkAll = false
    this.messageIds = []
    // console.log(this.messageIds)
  }


  sortByDate() {
    this.sortBy = $('#sortBy').val()
    this.fetchDetails();
  }

  nextMessage(pageNo) {
    this.pageNo = pageNo;
    this.search();
    this.setPage()
  }

  // search() {
    // this.pageSize = $('#pageSize').val()
    // this.fetchDetails()
  // }

  search() {

    // this.sortBy = $('#sortBy').val()
    this.pageSize = $('#pageSize').val()
    this.fetchDetails()
  }

  resetsearch() {
    this.pageNo = 1;
    this.searchParam = "";
    this.search();
    this.isCrossEnable = false;
  }

  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
  }



  previous() {
    this.pageNo = this.pageNo - 1;
    this.search()
    this.setPage()


  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.search()
    this.setPage()
  }

  setPage() {

    $('.pitem').removeClass('active');
    $(`.pitem:eq(${(this.pageNo - 1)})`).addClass('active');
    if (this.pageNo == 1 && this.pageCount == 1) {

      $('.previous').addClass('disabled');
      $('.next').addClass('disabled');

    }
    if (this.pageNo == 1 && this.pageCount > 1) {
      $('.previous').addClass('disabled');
      $('.next').removeClass('disabled');

    }
    if (this.pageNo > 1 && this.pageCount > 1) {
      $('.previous').removeClass('disabled');
      $('.next').removeClass('disabled');

    }
    if (this.pageNo > 1 && this.pageNo == this.pageCount) {

      $('.previous').removeClass('disabled');
      $('.next').addClass('disabled');

    }
  }
  arrayOne(n: number): any[] {
    if (n == undefined)
      n = 0
    n = Math.ceil(n)
    if (n != undefined)
      return Array(n);
  }

  // checkClicked() {

  //   this.checkAll = !this.checkAll;
  //   setTimeout(() => {
  //     $('#selectOption').selectpicker('refresh')

  //   }, 100);
  // }

  innercheckClicked(id) {

    if (this.messageIds.indexOf(id) == -1) {
      this.messageIds.push(id)
    } else {
      this.messageIds.splice(this.messageIds.indexOf(id), 1)
    }
    setTimeout(() => {
      $('#selectOption').selectpicker('refresh')

    }, 100);

    // console.log(this.messageIds)
  }


  selectChanged() {

    if ($('#selectOption').val() == 'Delete')
      $('#deleteEmail').modal('show')
  }

  getmessageId(id){
    this.messageId = id;
    // console.log(this.messageId)
  }
  onDelete() {
    this.checkAll = false;
    if(this.messageId){
      this._ss.deleteMessage(this.messageId, this.organization_id).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.ts.pop("success", "", "Message deleted successfully");
          this.messageId = ""
          this.fetchDetails()
        } else {
          // console.log("error ", res);
          this.messageId = "";
          this.ts.pop("error", "", "Something wrong, try again");
        }
      },err=>{
        this.messageId="";
        this.ts.pop("error", "", "Something wrong, try again");
      })
      
    }
    else {

      this._ss.deleteSelectedMessages(this.organization_id, this.messageIds).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.ts.pop("success", "", "Deleted successfully")
          this.fetchDetails();
          this.messageIds = [];
          // $('#deleteEmail').modal('close')
          $('#selectOption').val('');
          $('#selectOption').selectpicker('refresh')
        } else {
          // console.log("error ", res);
        }
      }, err => {
        this.messageIds = [];
        // $('#deleteEmail').modal('close')
        $('#selectOption').val('');
        $('#selectOption').selectpicker('refresh')      
        
      })

    }
  
}
refreshPicker() {
  // $('#deleteEmail').modal('close')
  $('#selectOption').val('');
  $('#selectOption').selectpicker('refresh') 
}

}
