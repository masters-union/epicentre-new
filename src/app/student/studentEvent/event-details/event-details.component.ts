import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
import { client } from "src/app/common/service/config";
import { OwlOptions } from 'ngx-owl-carousel-o';
import * as moment from 'moment';
declare var $;
@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.css']
})
export class EventDetailsComponent implements OnInit {

  loading: any;
  createEventForm: any;
  student_id: string;
  organizationId: string;
  eventId: any;
  eventData: any;
  pastEventData: any;
  upcommingEventData: any;
  eventCommData: any;
  isStuPresident: boolean;
  reviewText: any;
  stars: number[] = [1, 2, 3, 4, 5];
  selectedValue: number = 0;
  startValue: number;
  eventReview: any = [];
  startArray: any = [];
  eventstartArr = [1, 2, 3, 4, 5]
  eventGalleryData = [];
  customOptions: OwlOptions;
  customReviewOptions: OwlOptions;
  viewMoreList: any = [];
  eventComments: any = [];
  responseText: any = "";
  clubId: any = "";
  dayofweek = "";

  constructor(
    private stuService: StudentService, private fb: FormBuilder, private cs: CookieServiceProvider, private ts: ToasterService,
    private router: Router, private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.loading = true;
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['eventId']
    });
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    this.getEventDetail();
    this.getEventReview();
    this.getEventGallery();
    this.getEventComments();
  }

  getEventDetail() {
    this.stuService.getEventById(this.student_id, this.eventId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        this.eventData = data.Data;
        if (this.eventData.pastEvent.length > 0) {
          // this.pastEventData = this.eventData.pastEvent[0]
          this.eventCommData = this.eventData.pastEvent[0]
          this.clubId = this.eventCommData.clubId
          this.dayofweek = moment(this.eventData.pastEvent[0].eventDate).format("ddd");
        } else {
          // this.upcommingEventData = this.eventData.upcommingEvent[0]
          this.eventCommData = this.eventData.upcommingEvent[0]
          this.clubId = this.eventCommData.clubId
          this.dayofweek = moment(this.eventData.upcommingEvent[0].eventDate).format("ddd");
        }
        let eventDate = new Date(this.eventCommData.eventDate);
        let timeArr = this.eventCommData.eventTime.split(":");
        // moment(eventDate).set({h: +timeArr[0], m: +timeArr[1]});
        eventDate.setHours(+timeArr[0],+timeArr[1], 0, 0)
        if(moment(eventDate) > moment()){
          this.upcommingEventData = this.eventCommData
        }
        else {
          this.pastEventData = this.eventCommData
        }
        if (this.eventCommData.ClubMaster.ClubIntrestedStudents[0].Student.id == this.student_id) this.isStuPresident = true;
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    });
  }
  registerNow() {
    this.loading = true;
    let data = {
      eventId: this.eventId
    }
    // let timeArr = this.eventData.upcommingEvent[0].eventTime.split(":")
    // let eventStartTime = new Date(this.eventData.upcommingEvent[0].eventDate);
    // eventStartTime.setHours(+timeArr[0],+timeArr[1], 0, 0)

    this.stuService.eventRegisration(this.student_id, data).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        if (data.Message == "User already registered") this.ts.pop("error", data.Message);
        else this.ts.pop("success", data.Message);
        // this.getEventDetail();
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    });


  }

  addGalleryImg() {
    const options = {
      accept: ["image/*"],
      maxFiles: 10,
      storeTo: {
        location: "s3",
        path: "/eventClub/",
        region: "ap-south-1",
        access: "public"
      },
      onFileUploadFinished: (file) => {
        const data = {
          imgUrl: file.url
        }
        this.loading = true;
        this.stuService.addEventGallery(this.student_id, this.eventId, data).subscribe((data: any) => {
          if (data.IsSuccess) {
            this.loading = false;
            this.ts.pop("success", "Image successfully uploaded");
            this.getEventGallery();
          } else {
            this.loading = false;
            this.ts.pop("error", "Something Went Wrong");
          }
        });
      },
    };
    client.picker(options).open();
  }

  getEventGallery() {
    this.loading = true;
    this.stuService.getEventGallery(this.student_id, this.eventId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.eventGalleryData = data.Data;
        setTimeout(() => {
          this.customOptions = {
            loop: true,
            autoplay: true,
            center: true,
            dots: false,
            margin: 10,
            navSpeed: 700,
            autoHeight: true,
            autoWidth: true,
            responsive: {
              0: {
                items: 1,
              },
              600: {
                items: 4,
              },
              1000: {
                items: 6,
              },
              1200: {
                items: 6,
              }
            }
          }
          this.loading = false;
        }, 2000);
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    });
  }
  openReviewModel() {
    $("#exampleModal").modal("show");
  }

  onStar() {
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars li').on('mouseover', function () {
      var onStar = parseInt($(this).data('value'), 10);
      // The star currently mouse on     
      // Now highlight all the stars that's not after the current hovered star 
      $(this).parent().children('li.star').each(function (e) {
        if (e < onStar) {
          $(this).addClass('hover');
        }
        else {
          $(this).removeClass('hover');
        }
      });
    }).on('mouseout', function () {
      $(this).parent().children('li.star').each(function (e) {
        $(this).removeClass('hover');
      });
    });
    /* 2. Action to perform on click */
    // let jQueryInstance = this;
    $('#stars li').on('click', (event) => {
      var $this = $(event.currentTarget);
      var onStar = parseInt($this.data('value'), 10);
      this.startValue = onStar

      // The star currently selected   
      var stars = $this.parent().children('li.star');
      for (let i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }
      for (let i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }
    });

  }
  submitReview() {
    let data = {
      comment: this.reviewText,
      rating: this.startValue
    }
    if(this.reviewText.trim().length>0){
      this.loading = true;
      this.stuService.eventReview(this.student_id, this.eventId, data).subscribe((data: any) => {
        if (data.IsSuccess) {
          this.loading = false;
          this.ts.pop("success", data.Message);
          $("#exampleModal").modal("hide");
          this.getEventReview();
        } else {
          this.loading = false;
          this.ts.pop("error", "Something Went Wrong");
        }
      });
    }
    else{
      this.ts.pop("error", "Please write something");
    }
    
  }

  getEventReview() {
    this.loading = true;
    this.stuService.getEventReviews(this.organizationId, this.eventId).subscribe((data: any) => {
      if (data.IsSuccess) {
        // this.loading = false;
        this.eventReview = data.Data;
        // if (this.eventReview.length > 0) {
        //   for (let i = 0; i < this.eventReview[0].rating; i++) {
        //     this.startArray.push('fill');
        //   }
        // }
        setTimeout(() => {
          this.customReviewOptions = {
            loop: true,
            autoplay: true,
            center: true,
            dots: false,
            margin: 10,
            autoHeight: true,
            autoWidth: true,
            responsive: {
              0: {
                items: 1,
              },
              600: {
                items: 4,
              },
              1000: {
                items: 6,
              },
              1200: {
                items: 6,
              }
            }
          }
          this.loading = false;
        }, 2000);
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    });
  }

  viewMore(index) {
    let i = this.viewMoreList.indexOf(index);
    if (i >= 0) {
      this.viewMoreList.pop(i);
    } else {
      this.viewMoreList.push(index);
    }
  }

  getEventComments() {
    // this.loading = true;
    this.stuService.getEventComments(this.organizationId, this.student_id, this.eventId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let data = res.Data;
        data.forEach((e) => {
          e.createdAt = new Date(e.createdAt);
        })
        data.sort((a, b) => a.createdAt - b.createdAt);
        this.eventComments = data;
        // console.error(this.eventComments)
      }
      else {
        this.loading = false;
      }
    })
  }

  sendMessage() {
    let obj = {
      eventId: this.eventId,
      clubId: this.clubId,
      studentId: this.student_id,
      userResponseId: null,
      userComment: null,
      userCommentDate: null,
      studentComment: this.responseText,
      studentCommentDate: moment(),
    }
    if(this.responseText.trim().length>0){
      this.loading = true;
      this.stuService.createEventComment(this.organizationId, this.student_id, obj).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          this.responseText = "";
          this.ts.pop("success", "", "Sent")
          this.getEventComments();
  
        }
        else {
          this.loading = false;
        }
      })
    }
    else {
      this.ts.pop("warning", "", "Please write something")
    }
    
  }
}

