import { Component, OnInit, AfterViewInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { StudentService } from "../../service/student.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { client } from "src/app/common/service/config";
import * as moment from "moment";
declare var $: any;
@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css'],
  providers: [LoadscriptsService],
})
export class CreateEventComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  eventForm: FormGroup;
  createEventForm: FormGroup;
  organizationId: string;
  adminId: string;
  userId: string;
  loading: any;
  clubsList = [];
  departmentList = [];
  value: any;
  minDate: Date;
  showPaidAmount: boolean = false;
  programNames: any = [];
  proName: any;
  studentData: any;
  isCrossEnable: boolean;
  orgData = [];
  isSelected: boolean = false;
  prof: Object;
  searchParam: String = "";
  pageNo: number = 1;
  pageSize: any = 50;
  sortBy: any = "officialEmail";
  pageCount: number = 1;
  recordCount: number = 0;
  allCount: number = 0;
  selectAll: boolean = false;
  checkAll: boolean = false;
  studentIds: any = [];
  orgProgramsData = [];
  programs: any;
  mentorList = [];
  Programs: any;
  showStudentList: boolean = false;
  batchShow: boolean = false;
  formD: FormData;
  hideStudents: boolean = false;
  student_id: string;
  isPublished: boolean;
  clubStatus: string;
  EventType: any = 'internal';
  todayDate: string;
  clubId: any;

  constructor(
    private stuService: StudentService,
    private fb: FormBuilder,
    private loadscriptsService: LoadscriptsService,
    private cs: CookieServiceProvider,
    private ts: ToasterService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    var today = new Date();
    this.todayDate = moment(today).format("YYYY-MM-DD");
    this.initForm();
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);
    this.route.params.subscribe(param => {
      this.clubId = param.clubId;
    })
    this.getAllClubs();
  }

  ngAfterViewInit() {
    $(".multi-select").selectize({
      plugins: ["remove_button"],
      delimiter: ",",
      persist: false,
      create: function (input) {
        return {
          value: input,
          text: input,
        };
      },
    });
  }

  initForm() {
    this.createEventForm = this.fb.group({
      title: ["", Validators.required],
      description: ["", Validators.required],
      clubId: ["", Validators.required],
      amountNeeded: ["", Validators.required],
      eventTime: ["", Validators.required],
      eventDate: ["", Validators.required],
      eventType: ["internal"],
      organizationId: this.organizationId,
      adminId: this.adminId,
      userId: this.userId,
      isPublished: [false],
      isAutoApproved: [false],
      isPublic: [false],
      resourcesRequired: ["", Validators.required],
      isRegisterableEvent: [true],
      enableRating: [false],
      supportRequired: ["", Validators.required],
      fundingArray: ["", Validators.required],
      coverPicUrl: [""],
      eventStatus: ['pending']
    });
  }

  getAllClubs() {
    this.loading = true;
    this.isPublished = true;
    this.clubStatus = 'Approved'
    this.stuService.getClubs(this.student_id, this.isPublished, this.clubStatus).subscribe((data: any) => {
      if (data.IsSuccess) {
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
        this.loading = false;

        this.clubsList = data.Data;
        if (this.clubId) {
          this.createEventForm.controls.clubId.patchValue(this.clubId);
        }
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    });
  }

  uploadCoverImage() {
    const options = {
      accept: ["image/*"],
      storeTo: {
        location: "s3",
        path: "/eventClub/",
        region: "ap-south-1",
        access: "public"
      },
      onFileUploadFinished: (file) => {
        this.createEventForm.patchValue({
          coverPicUrl: file.url,
        });
      },
    };
    client.picker(options).open();
  }

  submitEventDetail() {
    var sdsd = $(".multi-select").val();
    this.createEventForm.get("fundingArray").setValue(sdsd);
    if(this.createEventForm.controls.title.value.trim().length>0 || this.createEventForm.controls.resourcesRequired.value.trim().length>0
    || this.createEventForm.controls.description.value.trim().length>0 || this.createEventForm.controls.supportRequired.value.trim().length>0){
      if (this.createEventForm.valid) {
        if((this.createEventForm.controls.eventTime.value < moment().format('HH:mm')) &&
            (this.createEventForm.controls.eventDate.value == moment().format('YYYY-MM-DD'))){
          this.ts.pop("error", "", "Time must be greater than or equal to current time")
        }
        else{
          this.loading = true;
          this.stuService.createEvent(this.student_id, this.createEventForm.value).subscribe((data: any) => {
            if (data.IsSuccess) {
              this.loading = false;
              this.router.navigate(["/student/event-list"]);
            } else {
              this.loading = false;
              this.ts.pop("error", "Something Went Wrong");
            }
          });
        }
      } else {
        this.ts.pop("error", "", "Please fill in the required fields")
      }
    }else {
      this.ts.pop("error", "", "Event name is empty")
    }
    
  }

  clubSearch() {
    let value = $("#clubId").val();
    this.createEventForm.get("clubId").setValue(value);
  }

  onTypeChange(event) {
    this.EventType = event;
  }
}

