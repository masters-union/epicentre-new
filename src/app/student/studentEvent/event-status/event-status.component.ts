import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl, Form } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { StudentService } from '../../service/student.service';
import { client } from 'src/app/common/service/config';
import * as moment from 'moment';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
declare var $: any;

@Component({
  selector: 'app-event-status',
  templateUrl: './event-status.component.html',
  styleUrls: ['./event-status.component.css'],
  providers: [LoadscriptsService]
})
export class EventStatusComponent implements OnInit {
  clubSearch: any

  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  student_id: string;
  organizationId: string;
  loading: boolean = true;
  eventId: any;
  eventData: any;
  pendingEventData: any;
  approvedEventData: any;
  rejectedEventData: any;
  eventComments: any = [];
  status: any = "";
  eventForm: FormGroup;
  formD: FormData;
  file: File;
  fileName: any;
  responseText: any = "";
  clubsList: any = [];
  eventFundings: any = [];
  arr: any = [];
  clubId: any = "";
  objArr: { value: any, text: any };
  EventType: any = 'internal';
  rejectedReason: any = "";
  todayDate: string;


  constructor(
    private _ss: StudentService, private cs: CookieServiceProvider, private ts: ToasterService,
    private route: ActivatedRoute, private router: Router, private fb: FormBuilder,
    private loadscriptsService: LoadscriptsService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    var today = new Date();
    this.todayDate = moment(today).format("YYYY-MM-DD");
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['eventId']
    });
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);

    this.eventInitForm();
    this.getAllClub();
    this.getEventDataById();
    this.getEventComments();


  }


  setMultiSelect() {
    setTimeout(() => {
      let $select = $(".multi-select").selectize({
        plugins: ["remove_button"],
        delimiter: ",",
        persist: false,
        create: function (input) {
          return {
            value: input,
            text: input,
          };
        },
      });

      let control = $select[0].selectize;
      // console.error(this.objArr)
      control.addOption(this.objArr);
      control.setValue(this.arr);
      // control.refreshOptions();
    }, 2000)


  }

  eventInitForm() {
    this.eventForm = this.fb.group({
      id: ["", Validators.required],
      title: ["", Validators.required],
      description: ["", Validators.required],
      clubId: ["", Validators.required],
      amountNeeded: ["", Validators.required],
      eventTime: ["", Validators.required],
      eventDate: ["", Validators.required],
      eventType: ["", Validators.required],
      // isAutoApproved: [""],
      // isPublic: [""],
      resourcesRequired: ["", Validators.required],
      isRegisterableEvent: [true],
      enableRating: [false],
      supportRequired: ["", Validators.required],
      fundingArray: ["", Validators.required],
      coverPicUrl: [""],
      eventStatus: ["", Validators.required],
      isPublic: [false],
    })
  }

  getAllClub() {
    // this.loading = true;
    this._ss.getAllPresidentPublishedClub(this.organizationId, this.student_id).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);

        this.clubsList = res.Data;
      }
      else {
        this.loading = false;
      }
    })
  }

  getEventDataById() {
    this.loading = true;
    this._ss.getUnpublishedEventById(this.student_id, this.eventId).subscribe((res: any) => {
      if (res.IsSuccess) {

        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
        this.eventData = res.Data;
        this.status = this.eventData.eventStatus;
        this.clubId = this.eventData.clubId
        this.eventData.EventFundings.forEach((element) => {
          this.arr.push(element.name);
        });
        this.objArr = this.eventData.EventFundings.map((element) => ({
          value: element.name,
          text: element.name,
        }));
        this.setMultiSelect();
        this.rejectedReason = this.eventData.rejectedReason
        this.eventForm.controls.id.patchValue(this.eventData.id);
        this.eventForm.controls.title.patchValue(this.eventData.title);
        this.eventForm.controls.description.patchValue(this.eventData.description);
        this.eventForm.controls.clubId.patchValue(this.eventData.clubId);
        this.eventForm.controls.amountNeeded.patchValue(this.eventData.amountNeeded);
        this.eventForm.controls.coverPicUrl.patchValue(this.eventData.coverPicUrl);
        this.eventForm.controls.enableRating.patchValue(this.eventData.enableRating);
        this.eventForm.controls.eventTime.patchValue(this.eventData.eventTime);
        this.eventForm.controls.eventDate.patchValue(moment(this.eventData.eventDate).format(
          "YYYY-MM-DD"
        ));
        this.eventForm.controls.resourcesRequired.patchValue(this.eventData.resourcesRequired);
        this.eventForm.controls.supportRequired.patchValue(this.eventData.supportRequired);
        this.eventForm.controls.eventStatus.patchValue(this.eventData.eventStatus);
        this.eventForm.controls.eventType.patchValue(this.eventData.eventType);
        this.eventForm.controls.isRegisterableEvent.patchValue(this.eventData.isRegisterableEvent);
        this.loading = false;
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
      }
      else {
        this.loading = false;
      }
    })
  }



  clubChange() {
    let value = $("#clubId").val();
    this.eventForm.get("clubId").setValue(value);

  }

  eventChange() {
    let value = $("#eventId").val();
    this.eventForm.get("eventType").setValue(value);
  }

  onUploadCoverPic() {

    const options = {
      accept: ["image/*"],
      storeTo: {
        location: "s3",
        path: "/eventClub/",
        region: "ap-south-1",
        access: "public"
      },
      onFileSelected(file) {
        // if (file.size > 10485760) {
        //     alert('Video size is too big, select something smaller than 10 MB');
        // }
      },
      // maxSize: 10485760,
      onFileUploadFinished: file => {
        this.eventForm.patchValue({
          coverPicUrl: file.url
        })

      }
    };
    client.picker(options).open();


  }

  editApprovedEventRequest() {

    var fund = $(".multi-select").val();
    this.eventForm.get("fundingArray").setValue(fund);
    // console.error(this.eventForm.value)
    if(this.eventForm.controls.title.value.trim().length>0){
      if (this.eventForm.valid) {
        this.loading = true;
        this._ss.editApprovedEventRequest(this.organizationId, this.student_id, this.eventForm.value).subscribe((data: any) => {
          if (data.IsSuccess) {
            this.loading = false;
            this.router.navigate(["/student/event-list"]);
          } else {
            this.loading = false;
            this.ts.pop("error", "Something Went Wrong");
          }
        });
      } else {
        this.ts.pop("error", "", "Please fill in the required fields")
      }
    }else {
      this.ts.pop("error", "", "Event name is empty")
    }
    

  }

  editEventRequest() {

    var fund = $(".multi-select").val();
    this.eventForm.get("fundingArray").setValue(fund);
    if(this.eventForm.controls.title.value.trim().length>0){
      if (this.eventForm.valid) {
        this.loading = true;
        this._ss.editEventRequest(this.organizationId, this.student_id, this.eventForm.value).subscribe((data: any) => {
          if (data.IsSuccess) {
            this.loading = false;
            this.router.navigate(["/student/event-list"]);
          } else {
            this.loading = false;
            this.ts.pop("error", "Something Went Wrong");
          }
        });
      } else {
        this.ts.pop("error", "", "Please fill in the required fields")
      }
    }else {
      this.ts.pop("error", "", "Event name is empty")
    }
    

  }
  getEventComments() {
    // this.loading = true;
    this._ss.getEventComments(this.organizationId, this.student_id, this.eventId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let data = res.Data;
        data.forEach((e) => {
          e.createdAt = new Date(e.createdAt);
        })
        data.sort((a, b) => a.createdAt - b.createdAt);
        this.eventComments = data;
        // console.error(this.eventComments)
      }
      else {
        this.loading = false;
      }
    })
  }

  sendMessage() {
    let obj = {
      eventId: this.eventId,
      clubId: this.clubId,
      studentId: this.student_id,
      userResponseId: null,
      userComment: null,
      userCommentDate: null,
      studentComment: this.responseText,
      studentCommentDate: moment(),
    }
    if(this.responseText.trim().length>0){
      this.loading = true;
      this._ss.createEventComment(this.organizationId, this.student_id, obj).subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          this.responseText = "";
          this.ts.pop("success", "", "Sent")
          this.getEventComments();
  
        }
        else {
          this.loading = false;
        }
      })
    }
    else {
      this.ts.pop("warning", "", "Please write something")
    }
    
  }

  goBack() {
    this.router.navigate(["/student/event-list"]);
  }
  onTypeChange(event) {
    this.EventType = event;
  }
}
