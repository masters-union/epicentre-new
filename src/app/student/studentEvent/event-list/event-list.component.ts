import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { CalendarOptions } from '@fullcalendar/angular';
import { StudentService } from '../../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { LoadscriptsService } from 'src/app/common/service/loadscripts.service';
import * as moment from 'moment';

declare var $: any;

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css'],
  providers: [LoadscriptsService]
})
export class EventListComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: any;
  student_id: string;
  organizationId: string;

  unPublishedRow: any;
  deleteCourse: any;
  clubList: any;
  createEventForm: any;
  clubsList: any;
  clubSearch: any;
  unPublished: any;

  eventStatus: any = '';
  searchParams1: String = '';
  searchParams2: String = '';
  isCrossEnable1: boolean;
  isCrossEnable2: boolean;
  eventType: any = '';
  publishedEvents: any = [];
  unPublishedEvents: any = [];
  pastE: any = []; //for list
  upcomingE: any = []; //for list
  showPast: Boolean = false;
  showUpcoming: Boolean = true;
  today = new Date();
  calendarOptions: CalendarOptions;
  eventsArray: any = []; //for calendar;
  showCalendar: Boolean = false;
  eventDetailId: any;
  eventTitle: any;
  clubName: any;
  eventDate: any;
  eventCoverPicUrl: any;
  eventTime: any;
  isPresident: Boolean = false;
  isAnyClubPresident: Boolean = false;

  constructor(
    private _ss: StudentService, private cs: CookieServiceProvider, private ts: ToasterService, private router: Router, private loadscriptsService: LoadscriptsService
  ) { }

  ngOnInit() {
    this.student_id = this.cs.getItem('id');
    this.organizationId = this.cs.getItem('orgId');
    this.allJoinedList();
    this.allPublishedEvents();
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);
  }

  allJoinedList(){
    // this.loading = true;
    let clubType = '';
    this._ss.getAllClubs(this.organizationId, this.student_id, this.searchParams1, clubType).subscribe((res: any) => {
      if (res.IsSuccess) {
        // this.loading = false;
        let orgClubs = res.Data;
        orgClubs.forEach((o) => {
          // o.isJoinedClub = o.ClubIntrestedStudents.filter((e)=> e.studentId == this.student_id)
          o.isPresident = o.ClubIntrestedStudents.filter((e)=> e.studentId == this.student_id && e.isPresident)
          if(o.isPresident.length>0){
            this.isAnyClubPresident = true;
          }
        })
        // orgClubs.forEach((o) => {
        //   if(o.isPresident.length>0){
        //     this.isAnyClubPresident = true;
        //   }
        // })
      }
      else {
        this.loading = false;
      }
    })
  }


  allPublishedEvents() {
    this.loading = true;
    this._ss.getAllPublishedEvents(this.organizationId, this.student_id, this.searchParams1, this.eventType).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let arr = res.Data;
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
        this.publishedEvents = [];
        arr.forEach((e) => {
          if (e.ClubMaster && e.ClubMaster.EventMasters) {
            e.ClubMaster.EventMasters.forEach(element => {
              this.publishedEvents.push(element)
            });
          }
        })
        // console.error(this.publishedEvents)
        this.publishedEvents.sort((a,b)=> new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime())
        this.setEvents();
      }
      else {
        this.loading = false;
      }
    })
  }

  setEvents() {
    this.publishedEvents.forEach((o) => {
      o.clubName = o.ClubMaster.name;
      o.isPresident = o.ClubMaster.ClubIntrestedStudents[0].isPresident,
      Object.keys(o).forEach((k) => {
        let eventDate = new Date(o[k])
        // eventDate.setHours(23, 59, 0, 0)
        let timeArr = o.eventTime.split(":")
        eventDate.setHours(+timeArr[0],+timeArr[1], 0, 0)
        if (k == 'eventDate') {
          if (this.today > eventDate) {
            o.status = "past"
            setTimeout(() => {
              // $(".fc-daygrid-event-harness").parents("td.fc-day-past").addClass("past-bg-pink");
            }, 100);
          } else {
            o.status = "upcoming"
            setTimeout(() => {
              // $(".fc-daygrid-event-harness").parents("td.fc-day-today").addClass("active-bg-green");
            }, 100);
          }
          // o.date = moment(o[k]).format('MMM, DD, YYYY');
          o.date = o[k]
          o.eventDate = moment(o[k]).format('MMM, DD, YYYY');
          o.eventTime = moment(o.eventTime, "HH:mm").format("hh:mm A");
        }
      });

    });

    // this.eventsArray = arr.map(({ title, date, id, clubName, status}) => ({ title, date, id, clubName, status}))
    this.pastE = [];
    this.upcomingE = [];
    this.publishedEvents.forEach(element => {
      if (element.status == "upcoming") {
        this.upcomingE.push(element)
      }
      else if (element.status == "past") {
        this.pastE.push(element)
      }
    });
    // this.pastEvents();
    this.upcomingEvents();
  }

  initializeCalendar(e) {
    this.calendarOptions = {
      initialView: 'dayGridMonth',
      // dateClick: this.handleDateClick.bind(this),
      events: e,
      eventClick: this.showModal.bind(this),
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridDay,dayGridMonth,dayGridWeek'
      },
      eventBackgroundColor: "transparent",
      eventBorderColor: "transparent",
      eventContent: this.renderEventContent,
    };
    setTimeout(() => {
      $(".fc-header-toolbar.fc-toolbar .fc-toolbar-title").appendTo("fc-header-toolbar.fc-toolbar .fc-toolbar-chunk:nth-child(1) .fc-button-group");
    }, 100);

  }
  renderEventContent(eventInfo, createElement) {
    let innerHtml;
    innerHtml =
      "<h4 class=' text-capitalize'>" +
      eventInfo.event.title +
      "</h4>" +
      "<span class=' text-capitalize '>" +
      eventInfo.event._def.extendedProps.clubName +
      "</span>";
    //Event with rendering html
    return (createElement = {
      html: "<div class='event-tag'>" + innerHtml + "</div>",
    });
    // }
  }

  search1() {
    // this.sortBy = $('#sortBy').val()
    this.allPublishedEvents();
  }

  resetsearch1() {
    this.searchParams1 = "";
    this.search1();
    this.isCrossEnable1 = false;
  }

  isSearchType1(value) {
    if (value) {
      this.isCrossEnable1 = true;
    } else {
      this.isCrossEnable1 = false;
    }
  }

  search2() {
    // this.sortBy = $('#sortBy').val()
    this.getAllUnPublishedEvents();
  }

  resetsearch2() {
    this.searchParams2 = "";
    this.search2();
    this.isCrossEnable2 = false;
  }

  isSearchType2(value) {
    if (value) {
      this.isCrossEnable2 = true;
    } else {
      this.isCrossEnable2 = false;
    }
  }

  sortByEventType() {
    this.eventType = $("#eventType").val();
    this.allPublishedEvents();
  }

  showModal(arg) {

    this.eventDetailId = arg.event.id;
    this.eventTitle = arg.event.title;
    this.clubName = arg.event.extendedProps.clubName;
    this.eventDate = arg.event.extendedProps.eventDate;
    this.eventTime = arg.event.extendedProps.eventTime;
    this.eventCoverPicUrl = arg.event.extendedProps.coverPicUrl;
    this.isPresident = arg.event.extendedProps.isPresident;
    $('#exampleModal').modal('show')
  }

  pastEvents() {
    this.showPast = true;
    this.showUpcoming = false;
    this.initializeCalendar(this.pastE)
  }

  upcomingEvents() {
    this.showPast = false;
    this.showUpcoming = true;
    this.initializeCalendar(this.upcomingE)
  }

  showCalendarFunc() {
    this.showCalendar = !this.showCalendar;
  }

  getAllUnPublishedEvents() {
    this.loading = true;
    this._ss.getAllUnPublishedEvents(this.organizationId, this.student_id, this.searchParams2, this.eventStatus).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        let arr = res.Data;
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
        this.unPublishedEvents = [];
        arr.forEach((e) => {
          if (e.ClubMaster && e.ClubMaster.EventMasters) {
            e.ClubMaster.EventMasters.forEach(element => {
              this.unPublishedEvents.push(element)
            });
          }
        })
        this.unPublishedEvents.sort((a,b)=> new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime())
      }
      else {
        this.loading = false;
      }
    })
  }


  getByStatus(){
    this.eventStatus = $('#eventStatus').val();
    this.getAllUnPublishedEvents()
  }

  goToStatusPage(eventId) {
    this.router.navigate(["/student/event-status", eventId]);
  }

  goToDetailsPage() {
    $('#exampleModal').modal('hide')
    this.router.navigate(['/student/event-details/', this.eventDetailId]);
  }
  eventDelete(eventId) {
    this.loading = true;
    this._ss.deletEvent(this.student_id, eventId).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.allPublishedEvents();
        this.getAllUnPublishedEvents();
        this.ts.pop("success", "Deleted successfully");
      }
      else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    })
  }

  copyLink(evId) {
    var text = "";
    text = "https://fee-test-mu.herokuapp.com/external-event/" + evId;
    var TempText = document.createElement("input");
    TempText.value = text;
    document.body.appendChild(TempText);
    TempText.select();
    document.execCommand("copy");
    this.ts.pop("success", "", "Link Copied Successfully");
    document.body.removeChild(TempText);
  }
}
