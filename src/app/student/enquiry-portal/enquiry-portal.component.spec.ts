import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiryPortalComponent } from './enquiry-portal.component';

describe('EnquiryPortalComponent', () => {
  let component: EnquiryPortalComponent;
  let fixture: ComponentFixture<EnquiryPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiryPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiryPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
