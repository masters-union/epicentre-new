import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StudentService } from "../service/student.service";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { client } from 'src/app/common/service/config';

declare var $;
@Component({
  selector: "app-enquiry-portal",
  templateUrl: "./enquiry-portal.component.html",
  styleUrls: ["./enquiry-portal.component.css"],
  providers: [LoadscriptsService],
})
export class EnquiryPortalComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  data = [];
  addForm: FormGroup;
  student_id: any;
  object: {};
  categoryArray: any[];
  subCategoryArray: any = [];
  categoryId;
  loading: boolean;
  org_id: any;
  email: any;
  newData: any;
  reply: any;
  student_email: any;
  pageSize: any = 50;
  pageCount: number = 1;
  recordCount: number = 0;
  allCount: number = 0;
  fetchedData = [];
  messageId: any;
  searchParam: String = "";
  isCrossEnable: boolean;
  sortBy = "dsc";
  pageNo: number = 1;
  statuslist: any;
  statusId: any;
  status: any;
  showSub: boolean = false;
  statusParam: String = "";
  subjectData: any;
  categoryName = "";
  viewMoreList: any = [];
  totalTextCount = 0;
  fileToUpload: any;
  fileArray=[]
fileName=[]
  constructor(
    private studentService: StudentService,
    private fb: FormBuilder,
    private cookie: CookieServiceProvider,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
    private router: Router
  ) {}

  ngOnInit() {
    window.console.error = function () { };
    this.initForm();
    this.student_id = this.cookie.getItem("id");
    this.org_id = this.cookie.getItem("orgId");
    this.email = this.cookie.getItem("email");
    console.error(this.org_id);
    this.object = {
      student_id: this.student_id,
    };
    this.solvedTicketRecord();

    this.setCategoryArray();
    this.getStudentDetails();
  }

  viewMore(index) {
    let i = this.viewMoreList.indexOf(index);
    if (i >= 0) {
      this.viewMoreList.pop(i);
    } else {
      this.viewMoreList.push(index);
    }
  }

  initForm() {
    this.addForm = this.fb.group({
      // message_type: ["Enquiry", Validators.required],
      student_email: [""],
      category_name: ["", Validators.required],
      subCategory_name: ["", Validators.required],
      description: ["", Validators.required],
      subject: ["", Validators.required],
      // contact_number: [
      //   ,
      //   [
      //     Validators.required,
      //     Validators.pattern(/^[6-9]\d{9}$/),
      //     Validators.maxLength(10),
      //   ],
      // ],
    });
  }
  setCategoryArray() {
    this.loading = true;
    this.studentService.getCategory(this.org_id).subscribe((res: any) => {
      //console.error(res);
      if (res.IsSuccess) {
        this.loading = false;
        this.categoryArray = res.Data;
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
      }
    });
  }

  categoryChange(e) {
    this.categoryName = $("#myselect option:selected").text();
    this.categoryId = e;
    let d = [];
    this.loading = true;

    if (this.categoryName != "Others" && this.categoryName != "Founders’ Office") {
      this.studentService
        .getSubCategory(e, this.org_id)
        .subscribe((res: any) => {
          if (res.IsSuccess) {
            this.subCategoryArray = res.Data;
            this.loading = false;
            setTimeout(() => {
              this.loadscriptsService.loadStuff();
            }, 100);
            this.showSub = true;
          }
        });
    } else {
      this.loading = false;
      this.showSub = false;
    }
  }

  solvedTicketRecord() {
    this.loading = true;
    this.studentService
      .fetchAllStudentEnquiry(
        this.org_id,
        this.student_id,
        this.searchParam,
        this.pageNo,
        this.pageSize,
        this.statusParam
      )
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          this.data = res.Data;
          // console.error(this.data);
          let d = res.Data;
          if (this.sortBy == "dsc") {
            d.sort(
              (a, b) =>
                new Date(b.createdAt).getTime() -
                new Date(a.createdAt).getTime()
            );
            this.fetchedData = d;
          } else if (this.sortBy == "asc") {
            d.sort(
              (a, b) =>
                new Date(a.createdAt).getTime() -
                new Date(b.createdAt).getTime()
            );
            this.fetchedData = d;
          }

          // console.error(res)
          setTimeout(() => {
            this.loadscriptsService.loadStuff();
          }, 100);
          this.pageCount = Math.ceil(res.count / this.pageSize);
          // console.error("count", this.pageCount);
          this.recordCount = this.fetchedData.length;
          this.allCount = res.count;
          this.setPage();
        } else {
          this.loading = false;
        }
      });
  }

  fromJsonDate(jDate) {
    const bDate: Date = new Date(jDate);
    let eDate = bDate.toISOString().substring(0, 10).replace(/\-/g, "/");
    return eDate;
  }

  get fval(): any {
    return this.addForm["controls"];
  }

  onSubmit(val) {
    // console.error(val)
    if (this.categoryName == "Others" || this.categoryName == "Founders’ Office") {
      if (val.subject == "" || val.description == "") {
        this.ts.pop("error", "", "*These fields are mandatory.");
      } else {
        this.loading = true;
        let addData = {
          enquiryType: val.message_type,
          organizationId: this.org_id,
          category_id: val.category_name,
          // sub_category_id: val.subCategory_name,
          organizationStudentId: this.student_id,
          subject: val.subject,
          // contact_number: val.contact_number,
          email: this.student_email,
          description: val.description,
          fileArray: this.fileArray
        };
        // console.error(addData);
        this.studentService
          .addNewEnquiry(this.org_id, this.student_id, addData)
          .subscribe((res: any) => {
            this.loading = false;
            if (res.IsSuccess) {
              // console.error("Submitted data: ", res);
              this.solvedTicketRecord();
              this.addForm.reset();
              this.fileArray=[]
              this.initForm();
              this.getStudentDetails();
              // this.showSub = false;
              this.ts.pop("success", "", "Submitted Successfully");
              this.totalTextCount=0
            } else {
              this.ts.pop("error", "", "Opps some error please try again");
            }
          });
      }
    } else {
      // console.error("else");
      if (this.addForm.valid) {
        this.loading = true;
        let addData = {
          enquiryType: val.message_type,
          organizationId: this.org_id,
          category_id: val.category_name,
          sub_category_id: val.subCategory_name,
          organizationStudentId: this.student_id,
          subject: val.subject,
          // contact_number: val.contact_number,
          email: this.student_email,
          description: val.description,
          fileArray: this.fileArray
        };
        // console.error(addData);
        this.studentService
          .addNewEnquiry(this.org_id, this.student_id, addData)
          .subscribe((res: any) => {
            this.loading = false;
            if (res.IsSuccess) {
              // console.error("Submitted data: ", res);
              this.solvedTicketRecord();
              this.addForm.reset();
              this.fileArray=[]
              this.initForm();
              this.getStudentDetails();
              this.showSub = false;
              this.totalTextCount =0
              this.ts.pop("success", "", "Submitted Successfully");
            } else {
              this.ts.pop("error", "", "Opps some error please try again");
            }
          });
      } else {
        this.ts.pop("error", "", "*These fields are mandatory.");
      }
    }
  }
  patchEmail() {
    this.addForm.patchValue({
      student_email: this.student_email,
    });
  }
  viewDescription(index) {
    // console.error(index);
    // $("#exampleModalCenter1").modal('show');
    this.newData = this.data[index].description;
    // console.error(this.newData);
  }
  viewSubject(index) {
    // console.error(index);
    // $("#exampleModalCenter1").modal('show');
    this.subjectData = this.data[index].subject;
  }
  viewReply(index) {
    this.reply = this.data[index].userResponse;
  }
  getStudentDetails() {
    this.loading = true;
    this.studentService
      .getStudentProfileDetails(this.student_id)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          // console.error(data.Data)
          this.student_email = data.Data.officialEmail;
          this.patchEmail();
          this.loading = false;
        }
      });
  }
  sortByDate() {
    this.sortBy = $("#sortBy").val();
    this.solvedTicketRecord();
  }
  sortByStatus() {
    this.statusParam = $("#sortByStatus").val();
    this.solvedTicketRecord();
  }

  nextMessage(pageNo) {
    this.pageNo = pageNo;
    this.search();
    this.setPage();
  }

  // search() {
  // this.pageSize = $('#pageSize').val()
  // this.fetchDetails()
  // }

  search() {
    // this.sortBy = $('#sortBy').val()
    this.pageSize = $("#pageSize").val();
    this.solvedTicketRecord();
  }

  resetsearch() {
    this.pageNo = 1;
    this.searchParam = "";
    this.search();
    this.isCrossEnable = false;
  }

  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
  }

  previous() {
    this.pageNo = this.pageNo - 1;
    this.search();
    this.setPage();
  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.search();
    this.setPage();
  }

  setPage() {
    $(".pitem").removeClass("active");
    $(`.pitem:eq(${this.pageNo - 1})`).addClass("active");
    if (this.pageNo == 1 && this.pageCount == 1) {
      $(".previous").addClass("disabled");
      $(".next").addClass("disabled");
    }
    if (this.pageNo == 1 && this.pageCount > 1) {
      $(".previous").addClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageCount > 1) {
      $(".previous").removeClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageNo == this.pageCount) {
      $(".previous").removeClass("disabled");
      $(".next").addClass("disabled");
    }
  }
  arrayOne(n: number): any[] {
    if (n == undefined) n = 0;
    n = Math.ceil(n);
    if (n != undefined) return Array(n);
  }

  viewStatus(index, id) {
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);
    this.statuslist = this.data[index].status;
    $("#exampleModalCenter3").modal("show");
    this.statusId = id;
    // console.error(this.statusId);
  }
  patchCheckStatus(value) {
    $("#exampleModalCenter3").modal("hide");
    this.loading = true;
    this.status = value;
    // console.error(this.status);
    let object = {
      status: this.status,
      solvedAt: null,
    };
    this.studentService
      .editEnquiry(this.org_id, this.student_id, this.statusId, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Status Updated");
          this.solvedTicketRecord();
          this.statuslist = "";
          this.loading = false;
        } else {
          this.loading = false;
        }
      });
  }

  onClickReply(enquiryId, orgStudentId) {
    // console.error("fefeeffe", enquiryId);
    this.loading = true;
    // this.studentService
    //   .getspecificStudentResponse(paramId)
    //   .subscribe((res: any) => {
    //     if (res.IsSuccess) {
    //       console.error("check data", res);
    // this.ts.pop("success", "Updated Successfully");
    this.router.navigate([
      "/student/enquiryConversation",
      enquiryId,
      orgStudentId,
    ]);
    this.loading = false;
    //   }
    // });
  }
  positiveFeedback(id) {
    let object = {
      postiveFeedback: true,
      negativeFeedback: false,
    };
    this.loading = true;
    this.studentService
      .studentFeedback(this.student_id, id, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Status Updated");
          this.solvedTicketRecord();
          this.loading = false;
        } else {
          this.loading = false;
        }
      });
  }
  negativeFeedback(id) {
    let object = {
      negativeFeedback: true,
      postiveFeedback: false,
    };
    this.loading = true;
    this.studentService
      .studentFeedback(this.student_id, id, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Status Updated");
          this.solvedTicketRecord();
          this.loading = false;
        } else {
          this.loading = false;
        }
      });
  }

  onChangeTextArea(text) {
    this.totalTextCount = text.length;
  }
  handleFileInput(files: FileList) {
    console.error("files",files);
    
    const options = {
      // storeTo: {
      //   location: "s3",
      //   path: "/leave/",
      //   region: "ap-south-1",
      //   access: "public",
      // },
      onFileUploadFinished: (file) => {
        console.error(file);
        
        this.fileToUpload = file.filename
        console.error("files",this.fileToUpload);
        this.fileArray.push(file.url)
        this.fileName.push(this.fileToUpload)
        // this.leaveForm.patchValue({
        //   uploadDocument: file.url,
        // });
      },
    };
    client.picker(options).open();

  }
}
