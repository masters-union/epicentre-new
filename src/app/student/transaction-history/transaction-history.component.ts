import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { NUMBER_TYPE, THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { parse } from 'querystring';

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.component.html',
  styleUrls: ['./transaction-history.component.css']
})
export class TransactionHistoryComponent implements OnInit {
  loading: boolean = false;
  student_id: String = "";
  details: any = []
  hidePdf: boolean;
  isCrossEnable: boolean;
  searchParam: String = "";
    constructor(private _ss: StudentService, private cookie: CookieServiceProvider) { }

  ngOnInit() {
    window.console.log = function () { };
    this.hidePdf = true;
    this.loading = true
    this.student_id = this.cookie.getItem('id')

    this.getStudentTransationDetails();

  }

  getStudentTransationDetails() {
    console.log()
    this.loading=true
    this._ss.getStudentTransationDetails(this.student_id,this.searchParam).subscribe((data: any) => {
      if (data.IsSuccess) {
        console.log(data.Data)
        this.loading = false
        this.details = data.Data
        console.log(this.details)


      }
    })
  }
  resetsearch() {
    this.searchParam = "";
    this.search()
    this.isCrossEnable = false;
  }
  
  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
    console.log("yes typing...........", value)

  }
  search() {
    this.getStudentTransationDetails()
  }

}
