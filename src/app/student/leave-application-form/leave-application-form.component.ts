import { Component, OnInit } from "@angular/core";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { Location } from "@angular/common";
import { StudentService } from "../service/student.service";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import * as moment from "moment";
import { client } from 'src/app/common/service/config';

declare var $: any;

@Component({
  selector: "app-leave-application-form",
  templateUrl: "./leave-application-form.component.html",
  styleUrls: ["./leave-application-form.component.css"],
  providers: [LoadscriptsService],
})
export class LeaveApplicationFormComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  leaveForm: FormGroup;
  organizationId: string;
  showDateView: boolean = false;
  loading: boolean;
  studentId: any;
  fileToUpload: any;
  formD: FormData;
  isMedical: boolean;
  isTerm: boolean;
  batchValue: any;
  courseList = [];
  todayDate: String;
  tommorowDate: String;
  leaveDays: number;
  dropdownSettings: IDropdownSettings;
  arrayLength = [];
  courseObj = [];

  constructor(
    private studentService: StudentService,
    private cs: CookieServiceProvider,
    private route: ActivatedRoute,
    private ts: ToasterService,
    private router: Router,
    private loadscriptsService: LoadscriptsService,
    private location: Location,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    var today = new Date();
    var tommorow = new Date();
    tommorow.setDate(today.getDate() + 1);
    this.todayDate = moment(today).format("YYYY-MM-DD");
    this.tommorowDate = moment(tommorow).format("YYYY-MM-DD");
    this.organizationId = this.cs.getItem("orgId");
    this.studentId = this.cs.getItem("id");
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      this.batchValue = this.cs.getItem("batch");
    }, 2000);

    this.initForm();
    this.dropdownSettings = {
      singleSelection: false,
      idField: "id",
      textField: "courseName",
      itemsShowLimit: 7,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      allowSearchFilter: true,
      enableCheckAll: false,
    };
    this.loadscriptsService.loadStuff();
  }

  initForm() {
    this.leaveForm = this.fb.group({
      Reason: ["", Validators.required],
      leaveFromDate: ["", Validators.required],
      leaveToDate: ["", Validators.required],
      organizationId: this.organizationId,
      leaveType: ["Casual Leave"],
      studentId: this.studentId,
      uploadDocument: [""],
      dayType: ["Single Day"],
      term: ["", Validators.required],
      courseArray: ["", Validators.required],
      numberOfDays:[""],

    });
  }

  isRadioType(value) {
    if (value == "Single Day") {
      this.showDateView = false;
    } else {
      this.showDateView = true;
    }
  }
  medicalSelect() {
    this.isMedical = true;
    this.leaveForm.controls["leaveFromDate"].reset();
    this.leaveForm.controls["leaveToDate"].reset();
    this.leaveForm.controls['uploadDocument'].setValidators([Validators.required]);
    this.leaveForm.controls['uploadDocument'].updateValueAndValidity();

    // this.leaveForm.get('uploadDocument').setValidators([
    //       Validators.required]);
  
  }
  casualSelect() {
    this.isMedical = false;
    this.leaveForm.controls["leaveFromDate"].reset();
    this.leaveForm.controls["leaveToDate"].reset();
    // this.leaveForm.get('uploadDocument').clearValidators();
    this.leaveForm.controls['uploadDocument'].clearValidators();
    this.leaveForm.controls['uploadDocument'].updateValueAndValidity();

  
  }

  submitLeaveApplication() {
    // console.log("form data", this.leaveForm.value);

    var values = $("input[name='pname[]']")
      .map(function () {
        return $(this).val();
      })
      .get();

    var check = values.includes("0");
    var newCheck = values.includes("");

    if (check) {
      this.ts.pop("error", "Number of lecture missing must be greater than 0");
    } else if (newCheck) {
      this.ts.pop("error", "Number of lecture missing must not be empty");
    } else {
      var startDate = this.leaveForm.get("leaveFromDate").value;
      var now = new Date();
      var currentDate = moment(now).format("YYYY-MM-DD");
      var endDate = this.leaveForm.get("leaveToDate").value;
      var duration = this.leaveForm.get("dayType").value;

      var sDate = moment(startDate);
      var eDate = moment(endDate);

      if (duration == "Single Day") {
        if (!this.showDateView) {
          var data = $("#datePicker").val();
          this.leaveForm.get("leaveToDate").setValue(data);
        }
        this.leaveDays = 1;
        this.leaveForm.get("numberOfDays").setValue(this.leaveDays);

        console.log(
          "arrlen: " + this.arrayLength.length,
          "courseObj " + this.courseObj.length
        );
        // if (this.arrayLength.length === this.courseObj.length) {
        this.submitLeave();
        // } else {
        //   this.ts.pop("error", "Please enter number of missing lectures");
        // }
      } else {
        if (endDate == "" || endDate < startDate) {
          this.ts.pop("error", "", "Please select appropriate date");
        } else {
          this.leaveDays = eDate.diff(sDate, "days") + 1;
          console.log(
            "arrlen: " + this.arrayLength.length,
            "courseObj " + this.courseObj.length
          );
          this.leaveForm.get("numberOfDays").setValue(this.leaveDays);


          // if (this.arrayLength.length === this.courseObj.length) {
          this.submitLeave();
          // } else {
          //   this.ts.pop("error", "Please enter number of missing lectures");
          // }
        }
      }
    }
  }

  submitLeave() {
    if (this.leaveForm.valid && !!this.leaveForm.value.Reason.trim()) {
      // this.formD = new FormData();
      // this.formD.append("Reason", this.leaveForm.get("Reason").value);
      // this.formD.append(
      //   "leaveFromDate",
      //   this.leaveForm.get("leaveFromDate").value
      // );
      // this.formD.append("leaveToDate", this.leaveForm.get("leaveToDate").value);
      // this.formD.append("numberOfDays", this.leaveDays.toString());
      // this.formD.append(
      //   "organizationId",
      //   this.leaveForm.get("organizationId").value
      // );
      // this.formD.append("leaveType", this.leaveForm.get("leaveType").value);
      // this.formD.append("studentId", this.leaveForm.get("studentId").value);
      // this.formD.append("dayType", this.leaveForm.get("dayType").value);
      // this.formD.append("courseArray", JSON.stringify(this.courseObj));
      // this.formD.append("uploadDocument", this.leaveForm.get("uploadDocument").value);

      this.loading = true;
      // console.error("dsds",this.leaveForm.value);
      
      this.studentService
        .submitApplicationForm(this.organizationId, this.studentId, this.leaveForm.value)
        .subscribe((data: any) => {
          if (data.IsSuccess) {
            console.log("data added");
            this.loading = false;
            $("#thankyou-modal").modal("show");
            // this.leaveForm.reset();

            // this.router.navigate(["/org/surveyList"]);
          } else {
            this.loading = false;
            this.ts.pop("error", "Something Went Wrong");
          }
        });
    } else {
      this.ts.pop("error", "", "Please fill the required fields");
    }
  }

  handleFileInput(files: FileList) {
    console.error("files",files);
    
    const options = {
      storeTo: {
        location: "s3",
        path: "/leave/",
        region: "ap-south-1",
        access: "public",
      },
      onFileUploadFinished: (file) => {
        this.fileToUpload = file.filename
        console.error("files",this.fileToUpload);

        this.leaveForm.patchValue({
          uploadDocument: file.url,
        });
        // $('#file-upload-content').css('display', 'block')
      },
    };
    client.picker(options).open();

  }

	onTermSelect(term) {
		this.isTerm = true;
		this.leaveForm.controls["courseArray"].reset();
		this.arrayLength = [];
		this.getCourseRosterList(this.batchValue, term);
	}

  getCourseRosterList(batch, term) {
    this.loading = true;
    this.studentService
      .getCourseByTermList(this.studentId, batch, term)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.loading = false;
          this.loadscriptsService.loadStuff();

          this.courseList = data.Data;
          if (this.courseList.length == 0) {
            this.isTerm = false;
            this.ts.pop("error", "", "No courses found in this term");
          }
          console.log("check data", data);
        }
      });
  }
  closeModal() {
    $("#thankyou-modal").modal("hide");
    this.router.navigate(["/student/leaveList"]);
  }

  onItemSelect(item: any) {
    console.log(item);

    // if (this.arrayLength.indexOf(item.id)) this.arrayLength.push(item);

    if (this.arrayLength.indexOf(item.id) == -1) {
      this.arrayLength.push(item);
	  this.courseObj.push({ id: item.id, courseMissed: 1 });
    this.leaveForm.get("courseArray").setValue( this.courseObj);

	} else {		
      	this.arrayLength.splice(this.arrayLength.indexOf(item.id), 1);
		if(this.courseObj.indexOf(item.id) != -1){
        	this.courseObj.splice(this.courseObj.indexOf(item.id), 1);
          this.leaveForm.get("courseArray").setValue( this.courseObj);

		}
    }

    console.log("checking array", this.arrayLength);
	console.log("checking courseObj",this.courseObj);

  }

  onItemDeSelect(item: any) {
    console.log(item);
    const removeIndex = this.arrayLength.findIndex(
      (element) => element.id === item.id
    );
    // remove object
    this.arrayLength.splice(removeIndex, 1);
    console.log("checking array", this.arrayLength);
  }

  onSelectAll(item: any) {
    console.log(item);
    // this.arrayLength.push(items);

    if (this.arrayLength.indexOf(item.id) == -1) {
      this.arrayLength.push(item);
    } else {
      this.arrayLength.splice(this.arrayLength.indexOf(item.id), 1);
    }
  }

  onSearchChange(searchValue: string, id: number): void {
    if (!!searchValue) {
      if (this.courseObj.length !== 0) {
        var foundId = this.courseObj.findIndex((x) => x.id == id);
        if (foundId == -1) {
          this.courseObj.push({ id: id, courseMissed: searchValue });
          this.leaveForm.get("courseArray").setValue( this.courseObj);

        } else {
          this.courseObj[foundId] = { id: id, courseMissed: searchValue };
          this.leaveForm.get("courseArray").setValue( this.courseObj);

        }
      } else {
        this.courseObj.push({ id: id, courseMissed: searchValue });
        this.leaveForm.get("courseArray").setValue( this.courseObj);

      }
    } else {
      if (this.courseObj.length !== 0) {
		  if(this.courseObj.indexOf(id) != -1){
        	this.courseObj.splice(this.courseObj.indexOf(id), 1);
          this.leaveForm.get("courseArray").setValue( this.courseObj);

		  }
      }
    }
    console.log(this.courseObj);
  }
}
