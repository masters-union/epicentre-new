import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { StudentService } from '../service/student.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { client } from 'src/app/common/service/config';
import * as moment from 'moment';
declare var $: any;
@Component({
  selector: 'app-survey-detail',
  templateUrl: './survey-detail.component.html',
  styleUrls: ['./survey-detail.component.css']
})
export class SurveyDetailComponent implements OnInit {

  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean;
  studentName: any;
  organizationId: string;
  mentorId: string;
  questionData: any;
  surveyForm: FormGroup;
  studentId: any;
  surveyId: any;
  surveyData: any;
  sq: any;
  surveyAnswers: any;
  goals: any[];
  surveyjQueryQuesInd: any;
  sId: any;
  ques: any;
  sQuesId: any;
  selectedItems: any[];
  checkboxSelectedArray: any = [];
  arraytwo: any = [];
  formInvalid: boolean = true
  surveyUserId: any;
  hideSubmit : boolean = false;
  surveyIsAnonymous: boolean = false;
  surveyNotAssigned: boolean = false;
  formD: FormData;
  fileName: any;
  fileArray: any[] = [];
  studentRegNumber: any;

  constructor(private route: ActivatedRoute, private cs: CookieServiceProvider,
    private _ss: StudentService, private fb: FormBuilder, private router: Router,
    private ts: ToasterService) { }

  ngOnInit() {
    window.console.log = function () { };
    this.route.params.subscribe((params: Params) => {
      // this.mentorName = params['mentorName'];
      // this.mentorId = params['id'];
      this.surveyId = params['surveyId'];
    })
    // this.surveyId = "160ca972-b63d-4087-ab11-e7c56d01d11b"
    this.organizationId = this.cs.getItem('orgId')
    this.studentId = this.cs.getItem('id')
    // this.surveyInitForm();
    this.getSurveyQuestion();
    this.surveyAnswers = [];
    this.goals = []
    this.selectedItems = new Array<string>();
  }

  surveyInitForm(){
    this.surveyForm = this.fb.group({
      // id: ['', Validators.required],
      // studentId: ['', Validators.required],
      // mentorId: '',
      // surveyId: ['', Validators.required],
      // userType: ['', Validators.required],
      // isAttempted
      surveyQuestions: this.fb.array([this.fb.group({
        id: ['', Validators.required],
        question: ['', Validators.required],
        questionType: ['', Validators.required],
        displayOrder: ['', Validators.required],
        surveyQuestionOptions: this.fb.array([this.fb.group({
            optionName: ['', Validators.required],
          })
        ])
      })
    ])
    })
  }

  getSurveyQuestion(){
    this.loading = true
    this._ss.getSurveyById(this.studentId, this.surveyId).subscribe((data:any) => {
      this.loading = false
      if(data.IsSuccess){
        this.surveyData = data.Data;
        this.studentRegNumber = this.surveyData.Student.registrationNumber || 0;
        if(this.surveyData == null){
         // this.router.navigate(["/student/survey-list"]);
          this.surveyNotAssigned = true;
        }
        else {
          this.surveyUserId = this.surveyData.id
          let arr= this.surveyData.SurveyMaster.SurveyQuestions
          this.surveyIsAnonymous = this.surveyData.SurveyMaster.isAnonymous
          if(this.surveyIsAnonymous){
            this.surveyNotAssigned = true;
          }
          this.sq = arr.sort((a,b)=>{return parseFloat(a.displayOrder) - parseFloat(b.displayOrder)})
          this.sq.forEach(element => {
            let obj = {
              surveyUserId: this.surveyUserId,
              surveyId: element.surveyId,
              surveyQuestionId: element.id,
              answer: "",
              questionType: element.questionType,
              studentId:this.studentId,
              mentorId: "",
              userType: "student",
              question: element.question,
              optionId: null,
              isRequired: element.isRequired
            }
              
              
            this.surveyAnswers.push(obj)
            if(element.questionType == "checkBox" && element.isRequired){
              this.checkboxSelectedArray.push(element.id)
            } 
            });
            this.surveyData.SurveyMaster.expiryDate = new Date(this.surveyData.SurveyMaster.expiryDate)
            this.surveyData.SurveyMaster.expiryDate.setHours(23,59, 0, 0)
            if(this.surveyData.SurveyMaster.expiryDate < new Date()){
                this.hideSubmit = true;
            }
              // this.surveyForm.setControl('surveyQuestions', this.fb.array(this.setSurveyQuestion(this.sq)));
        }
      }
      else{
        this.surveyNotAssigned = true;
      }
        
    })
  }

  // setSurveyQuestion(arr: any): Array<any>{
  //   const array = []
  //   // let c = this.fb.control(null, Validators.required)
  //   arr.forEach(element => {
  //     let a = this.fb.group({
  //       id: element.id,
  //       question: element.question,
  //       questionType: element.questionType,
  //       displayOrder: element.displayOrder,
  //       surveyQuestionOptions: this.fb.array(this.surveyQuesoptions(element.surveyQuestionOptions))
  //     })
  //     array.push(a);
      
  //   });
  //   const sortedArray = array.sort((a,b)=>{return parseFloat(a.value.displayOrder) - parseFloat(b.value.displayOrder)})
  //   return sortedArray
  // }

  // surveyQuesoptions(element){
  //   let arr = []
  //   element.forEach(op => {
  //     let c = this.fb.group({
  //       optionName: op.optionName
  //     })
  //     arr.push(c)
  //   })
  //   return arr
  // }


  onRateQus(quesIndex, quesId) {
    this.surveyjQueryQuesInd = quesIndex
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars'+quesIndex+' li').on('mouseover', function () {
      var onStar = parseInt($(this).data('value'), 10);
      // The star currently mouse on     
      // Now highlight all the stars that's not after the current hovered star 
      $(this).parent().children('li.star').each(function (e) {
        if (e < onStar) {
          $(this).addClass('hover');
        }
        else {
          $(this).removeClass('hover');
        }
      });
    }).on('mouseout', function () {
      $(this).parent().children('li.star').each(function (e) {
        $(this).removeClass('hover');
      });
    });
    /* 2. Action to perform on click */
    // let jQueryInstance = this;
    $('#stars'+quesIndex+' li').on('click', (event) => {
      var $this = $(event.currentTarget);
      var onStar = parseInt($this.data('value'), 10);
      let ind = this.surveyAnswers.findIndex(e=> e.surveyQuestionId == quesId)
      this.surveyAnswers[ind].answer = onStar
      // this.surveyAnswers[this.surveyjQueryQuesInd].answer = onStar

      // The star currently selected   
      var stars = $this.parent().children('li.star');
      for (let i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }
      for (let i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }
    });

  }

  onRadioButtonClick(ques, quesId, value, quesIndex){
    this.surveyjQueryQuesInd = quesIndex
    this.surveyAnswers[this.surveyjQueryQuesInd].answer = value
    
  }

  updateCheckBoxOption(ques, questionId, value, event, optionNameId, isRequired){
    if(event.target.checked){
      // this.selectedItems.push(value)
      let obj = {
        surveyUserId: this.surveyUserId,
        surveyId: this.surveyId,
        surveyQuestionId: questionId,
        answer: value,
        studentId:this.studentId,
        mentorId: "",
        userType: "student",
        question: ques,
        optionId: optionNameId,
        questionType: "checkBox",
        isRequired: isRequired
      }
      
        this.surveyAnswers.push(obj)
        this.selectedItems.push(obj)
    }
    else{
      this.selectedItems = this.selectedItems.filter(m=>{
        if(m.surveyQuestionId == questionId){
          if(m.answer!=value){
            return true;
          }
          else {
            return false;
          }
        }
        else {
          return true;
        }
      })


      this.surveyAnswers = this.surveyAnswers.filter(m=>{
        if(m.surveyQuestionId == questionId){
          if(m.answer!=value){
            return true;
          }
          else {
            return false;
          }
        }
        else {
          return true;
        }
      })
      
    }
    this.arraytwo = [];
    this.selectedItems.forEach(e=>{
      this.arraytwo.push(e.surveyQuestionId)
    })
  }

  onTextAreaChanged(val, quesIndex){
    this.surveyjQueryQuesInd = quesIndex
    this.surveyAnswers[this.surveyjQueryQuesInd].answer = val
  }

  onUploadFile(event, quesIndex) {
    // if (event.target.files) {
    // this.formD = new FormData();
    // const file: File = event.target.files[0];
    // this.fileArray.push(file.name)
    // // this.fileName = file.name;
    // $('#'+quesIndex).text(file.name);
    // this.surveyjQueryQuesInd = quesIndex;
    // // this.surveyAnswers[this.surveyjQueryQuesInd].answer = this.fileName;

    // // let appendFileName = 'file#' + quesIndex;
    // // this.formD.append(appendFileName, file, file.name);
    // this.formD.append('file', file, file.name)
    // this.loading = true;
    // this._ss.uploadSurveyFile(this.organizationId, this.formD).subscribe((res: any) => {
    //   if (res.IsSuccess) {
    //     this.fileName = res.Data
    //     this.surveyAnswers[this.surveyjQueryQuesInd].answer = this.fileName;
    //     this.loading = false
    //     this.ts.pop("success", "", "Uploaded Successfully");
    //   } else {
    //     this.loading = false
    //     this.ts.pop("error", "", res.Message)
    //   }
    // })
    // }
    let temp = "";
    let stuRegNo = this.studentRegNumber
    const options = {
      accept: ["video/*"],
      onFileSelected(file) {
        if (file.size > 52428800) {
          alert('Video size is too big, select something smaller than 50 MB');
        }
        temp = file.filename;
        let min = file.mimetype.split('video/')[1];
        let exten = file.filename.split('.').pop();
        return { ...file, name: stuRegNo + "_" + moment() + "." + min }
      },
      storeTo: {
        location: "s3",
        path: "/MU-videos/",
        region: "ap-south-1",
        access: "public"
      },
      maxSize: 52428800,
      onFileUploadFinished: file => {
        this.fileName = file.url;
        this.surveyjQueryQuesInd = quesIndex;
        this.surveyAnswers[this.surveyjQueryQuesInd].answer = this.fileName;
        $('#' + quesIndex).text(temp);
      }
    };
    client.picker(options).open();
  }

  submitSurveyForm(){
    // if(this.selectedItems.length==0 && this.checkboxSelectedArray.lenght!=0){
    //   this.formInvalid = true;
    // }
    // else{
      
        // this.formInvalid = !(this.checkboxSelectedArray.every(e=> this.arraytwo.includes(e)))
  // }  
  
  // if(this.checkboxSelectedArray.lenght!=0){
  //   this.formInvalid = !(this.checkboxSelectedArray.every(e=> this.arraytwo.includes(e)))
  // }
  // else if(this.selectedItems.length==0){
  //   this.formInvalid = true;
  // }
      
  //   this.surveyAnswers.forEach((e)=>{
  //     if(e.answer=="" && e.questionType!="checkBox"){
  //       this.formInvalid = true
  //     }
  //   })
  //  if(this.formInvalid){
  //     this.ts.pop("error", "", "Please give response to all required question");
  //   }
  let checkboxValidation = this.checkboxSelectedArray.every(e=> this.arraytwo.includes(e))
  let mandatoryArr = this.surveyAnswers.filter(element => element.answer =="" && element.isRequired && element.questionType !="checkBox");
  let arr = this.surveyAnswers.filter(element => element.answer =="");
   if(arr.length == this.surveyAnswers.length){
      this.ts.pop("error", "", "Empty form can not be submitted!");
    }
    else if(!checkboxValidation){
      this.ts.pop("error", "", "Please fill in the mandatory questions");
    }
    else if(mandatoryArr.length > 0){
      this.ts.pop("error", "", "Please fill in the mandatory questions");
    }
   else{ 
      this.loading = true
      this.surveyAnswers = this.surveyAnswers.filter(m=>m.answer!="")
      if(this.surveyData.SurveyMaster.isAnonymous){
        this.surveyAnswers.forEach((e)=>{
          delete e.studentId
        })

        let obj = {
          surveyAnswers: this.surveyAnswers,
          Goals : this.goals,
          surveyId: this.surveyId,
        }
        // this.formD.append('surveyAnswers', JSON.stringify(this.surveyAnswers));
        // this.formD.append('Goals', JSON.stringify(this.goals));
        // this.formD.append('surveyId', this.surveyId);
        this._ss.createAnonymousSurveyAnswers(obj).subscribe((data:any) => {
        
          if(data.IsSuccess){
            this.loading = false
                $('#thankyou-modal').modal({backdrop: 'static', keyboard: false});
            // this.surveyForm.setControl('surveyQuestions', this.fb.array(this.setSurveyQuestion(this.sq)));
          }
          else {
            this.loading = false
            this.ts.pop("error", "", "Something went wrong. Please try again.");
          }
        })
      }else {
      
      let obj = {
        surveyAnswers: this.surveyAnswers,
        Goals : this.goals,
        surveyId: this.surveyId,
      }
      // this.formD.append('surveyAnswers', JSON.stringify(this.surveyAnswers));
      // this.formD.append('Goals', JSON.stringify(this.goals));
      // this.formD.append('surveyId', this.surveyId);
      this._ss.createSurveyAnswers(this.studentId, obj).subscribe((data:any) => {
        
        if(data.IsSuccess){
          this.loading = false
              $('#thankyou-modal').modal({backdrop: 'static', keyboard: false});
          // this.surveyForm.setControl('surveyQuestions', this.fb.array(this.setSurveyQuestion(this.sq)));
        }
        else {
          this.loading = false
          this.ts.pop("error", "", "Something went wrong. Please try again.");
        }
      })
      }
   }
  }

  closeModal(){
    $('#thankyou-modal').modal("hide");
    this.loading = true;
   setTimeout(() => {
      this.loading = false;
      this.router.navigate(["/student/survey-list"]);
    }, 1000);
    
  }

}
