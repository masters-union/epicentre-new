import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { HeaderComponent } from './common/commHeader/header/header.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from './common/service/auth.guard';
import { myInterceptor } from './common/service/interceptor.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AuthServiceL } from './authenticate/services/auth.service';
import { AnonymousSurveyComponent } from './anonymous/anonymous-survey/anonymous-survey.component';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { ExternalEventComponent } from './event/external-event/external-event.component';
import { EventFeedbackComponent } from './event/event-feedback/event-feedback.component';
// import { CarouselModule } from 'ngx-owl-carousel-o';

@NgModule({
  declarations: [
    AppComponent,
    AnonymousSurveyComponent,
    ExternalEventComponent,
    EventFeedbackComponent,
    // HeaderComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ToasterModule,
    // CarouselModule,

  ],
  providers: [AuthGuard, AuthServiceL, { provide: HTTP_INTERCEPTORS, useClass: myInterceptor, multi: true }, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
