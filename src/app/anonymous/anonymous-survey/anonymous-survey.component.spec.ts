import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnonymousSurveyComponent } from './anonymous-survey.component';

describe('AnonymousSurveyComponent', () => {
  let component: AnonymousSurveyComponent;
  let fixture: ComponentFixture<AnonymousSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnonymousSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnonymousSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
