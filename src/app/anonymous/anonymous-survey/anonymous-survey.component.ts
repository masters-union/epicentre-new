import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { CommonServiceService } from 'src/app/common/service/common-service.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { client } from 'src/app/common/service/config';
import * as moment from 'moment';
declare var $: any;
@Component({
  selector: 'app-anonymous-survey',
  templateUrl: './anonymous-survey.component.html',
  styleUrls: ['./anonymous-survey.component.css']
})
export class AnonymousSurveyComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean;
  studentName: any;
  organizationId: string;
  mentorId: string;
  questionData: any;
  surveyForm: FormGroup;
  studentId: any;
  surveyId: any;
  surveyData: any;
  sq: any;
  surveyAnswers: any;
  goals: any[];
  surveyjQueryQuesInd: any;
  sId: any;
  ques: any;
  sQuesId: any;
  selectedItems: any[];
  checkboxSelectedArray: any = [];
  arraytwo: any = [];
  formInvalid: boolean = true
  surveyUserId: any;
  hideSubmit: boolean = false;
  surveyIsAnonymous: boolean = false;
  orgId: any;
  isSuccessfullSubmit: boolean = false;
  noData: boolean = false;
  formD: FormData;
  fileName: any;


  constructor(private route: ActivatedRoute, private cs: CookieServiceProvider,
    private _ss: CommonServiceService, private fb: FormBuilder, private router: Router,
    private ts: ToasterService
  ) { }

  ngOnInit() {
    // window.console.log = function () { };
    this.route.params.subscribe((params: Params) => {
      this.surveyId = params['surveyId'];
      this.orgId = params['orgId'];
    })
    this.organizationId = this.cs.getItem('orgId')
    // this.studentId = this.cs.getItem('id')
    // this.surveyInitForm();
    this.getSurveyQuestion();
    this.surveyAnswers = [];
    this.goals = []
    this.selectedItems = new Array<string>();
  }

  surveyInitForm() {
    this.surveyForm = this.fb.group({
      surveyQuestions: this.fb.array([this.fb.group({
        id: ['', Validators.required],
        question: ['', Validators.required],
        questionType: ['', Validators.required],
        displayOrder: ['', Validators.required],
        surveyQuestionOptions: this.fb.array([this.fb.group({
          optionName: ['', Validators.required],
        })
        ])
      })
      ])
    })
  }

  getSurveyQuestion() {
    this.loading = true
    this._ss.getAnonymousSurveyQuestions(this.surveyId).subscribe((data: any) => {
      this.loading = false
      if (data.IsSuccess) {
        this.surveyData = data.Data;
        if (this.surveyData == null) {
          this.noData = true;
        }
        else {
          this.surveyUserId = this.surveyData.id
          let arr = this.surveyData.SurveyQuestions
          this.surveyIsAnonymous = this.surveyData.isAnonymous
          this.sq = arr.sort((a, b) => { return parseFloat(a.displayOrder) - parseFloat(b.displayOrder) })
          this.sq.forEach(element => {
            let obj = {
              surveyUserId: this.surveyUserId,
              surveyId: element.surveyId,
              surveyQuestionId: element.id,
              answer: "",
              questionType: element.questionType,
              // studentId: this.studentId,
              mentorId: "",
              userType: "student",
              question: element.question,
              optionId: null,
              isRequired: element.isRequired
            }
            // console.log(this.sq);

            this.surveyAnswers.push(obj)
            if (element.questionType == "checkBox" && element.isRequired) {
              this.checkboxSelectedArray.push(element.id)
            }

          });
          this.surveyData.expiryDate = new Date(this.surveyData.expiryDate)
          this.surveyData.expiryDate.setHours(23, 59, 0, 0)
          if (this.surveyData.expiryDate < new Date()) {
            this.hideSubmit = true;
          }
          // this.surveyForm.setControl('surveyQuestions', this.fb.array(this.setSurveyQuestion(this.sq)));
        }
      }
      else {
        this.noData = true;
      }
    })
  }

  // setSurveyQuestion(arr: any): Array<any>{
  //   const array = []
  //   // let c = this.fb.control(null, Validators.required)
  //   arr.forEach(element => {
  //     let a = this.fb.group({
  //       id: element.id,
  //       question: element.question,
  //       questionType: element.questionType,
  //       displayOrder: element.displayOrder,
  //       surveyQuestionOptions: this.fb.array(this.surveyQuesoptions(element.surveyQuestionOptions))
  //     })
  //     array.push(a);

  //   });
  //   const sortedArray = array.sort((a,b)=>{return parseFloat(a.value.displayOrder) - parseFloat(b.value.displayOrder)})
  //   return sortedArray
  // }

  // surveyQuesoptions(element){
  //   let arr = []
  //   element.forEach(op => {
  //     let c = this.fb.group({
  //       optionName: op.optionName
  //     })
  //     arr.push(c)
  //   })
  //   return arr
  // }


  onRateQus(quesIndex, quesId) {
    this.surveyjQueryQuesInd = quesIndex
    /* 1. Visualizing things on Hover - See next part for action on click */
    $('#stars'+quesIndex+' li').on('mouseover', function () {
      var onStar = parseInt($(this).data('value'), 10);
      // The star currently mouse on     
      // Now highlight all the stars that's not after the current hovered star 
      $(this).parent().children('li.star').each(function (e) {
        if (e < onStar) {
          $(this).addClass('hover');
        }
        else {
          $(this).removeClass('hover');
        }
      });
    }).on('mouseout', function () {
      $(this).parent().children('li.star').each(function (e) {
        $(this).removeClass('hover');
      });
    });
    /* 2. Action to perform on click */
    // let jQueryInstance = this;
    $('#stars'+quesIndex+' li').on('click', (event) => {
      var $this = $(event.currentTarget);
      var onStar = parseInt($this.data('value'), 10);
      let ind = this.surveyAnswers.findIndex(e=> e.surveyQuestionId == quesId)
      this.surveyAnswers[ind].answer = onStar
      
      // console.log(this.surveyAnswers)

      // The star currently selected   
      var stars = $this.parent().children('li.star');
      for (let i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }
      for (let i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }
    });

  }

  onRadioButtonClick(ques, quesId, value, quesIndex) {
    this.surveyjQueryQuesInd = quesIndex
    this.surveyAnswers[this.surveyjQueryQuesInd].answer = value

  }

  updateCheckBoxOption(ques, questionId, value, event, optionNameId, isRequired) {
    if (event.target.checked) {
      // this.selectedItems.push(value)
      let obj = {
        surveyUserId: this.surveyUserId,
        surveyId: this.surveyId,
        surveyQuestionId: questionId,
        answer: value,
        // studentId: this.studentId,
        mentorId: "",
        userType: "student",
        question: ques,
        optionId: optionNameId,
        questionType: "checkBox",
        isRequired: isRequired
      }

      this.surveyAnswers.push(obj)
      this.selectedItems.push(obj)
    }
    else {
      this.selectedItems = this.selectedItems.filter(m => {
        if (m.surveyQuestionId == questionId) {
          if (m.answer != value) {
            return true;
          }
          else {
            return false;
          }
        }
        else {
          return true;
        }
      })


      this.surveyAnswers = this.surveyAnswers.filter(m => {
        if (m.surveyQuestionId == questionId) {
          if (m.answer != value) {
            return true;
          }
          else {
            return false;
          }
        }
        else {
          return true;
        }
      })

    }
    this.arraytwo = [];
    this.selectedItems.forEach(e => {
      this.arraytwo.push(e.surveyQuestionId)
    })
  }

  onTextAreaChanged(val, quesIndex) {
    this.surveyjQueryQuesInd = quesIndex
    this.surveyAnswers[this.surveyjQueryQuesInd].answer = val
  }

  onUploadFile(event, quesIndex) {
    // if (event.target.files) {
    //   this.formD = new FormData();
    //   const file: File = event.target.files[0];
    //   // this.fileArray.push(file.name)
    //   // this.fileName = file.name;
    //   $('#' + quesIndex).text(file.name);
    //   this.surveyjQueryQuesInd = quesIndex;
    //   // this.surveyAnswers[this.surveyjQueryQuesInd].answer = this.fileName;

    //   // let appendFileName = 'file#' + quesIndex;
    //   // this.formD.append(appendFileName, file, file.name);
    //   this.formD.append('file', file, file.name)
    //   this.loading = true;
    //   this._ss.uploadSurveyFile(this.organizationId, this.formD).subscribe((res: any) => {
    //     if (res.IsSuccess) {
    //       this.fileName = res.Data
    //       this.surveyAnswers[this.surveyjQueryQuesInd].answer = this.fileName;
    //       this.loading = false
    //       this.ts.pop("success", "", "Uploaded Successfully");
    //     } else {
    //       this.loading = false
    //       this.ts.pop("error", "", res.Message)
    //     }
    //   })
    // }
    // const options = {
    //   accept: ["video/*", "application/pdf"],
    //   onFileSelected(file) { 
    //       if (file.size > 10485760) {
    //           alert('Video size is too big, select something smaller than 10 MB');
    //       }
    //     })
    // }
    let temp = "";
    let surveyname = this.surveyData.title
    const options = {
      accept: ["video/*"],
      onFileSelected(file) {
        if (file.size > 52428800) {
          alert('Video size is too big, select something smaller than 50 MB');
        }
        temp = file.filename;
        let min = file.mimetype.split('video/')[1];
        let exten = file.filename.split('.').pop();
        return { ...file, name: surveyname + "_" + moment() + "." + min }
      },
      maxSize: 52428800,
      storeTo: {
        location: "s3",
        path: "/MU-videos/",
        region: "ap-south-1",
        access: "public"
      },
      onFileUploadFinished: file => {
        this.fileName = file.url;
        this.surveyjQueryQuesInd = quesIndex;
        this.surveyAnswers[this.surveyjQueryQuesInd].answer = this.fileName;
        $('#' + quesIndex).text(temp);
      }
    };
    client.picker(options).open();
  }


  submitSurveyForm() {
    // if(this.selectedItems.length==0 && this.checkboxSelectedArray.lenght!=0){
    //   this.formInvalid = true;
    // }
    // else{

    // this.formInvalid = !(this.checkboxSelectedArray.every(e=> this.arraytwo.includes(e)))
    // }  

    // if(this.checkboxSelectedArray.lenght!=0){
    //   this.formInvalid = !(this.checkboxSelectedArray.every(e=> this.arraytwo.includes(e)))
    // }
    // else if(this.selectedItems.length==0){
    //   this.formInvalid = true;
    // }

    //   this.surveyAnswers.forEach((e)=>{
    //     if(e.answer=="" && e.questionType!="checkBox"){
    //       this.formInvalid = true
    //     }
    //   })
    //  if(this.formInvalid){
    //     this.ts.pop("error", "", "Please give response to all required question");
    //   }
    let checkboxValidation = this.checkboxSelectedArray.every(e => this.arraytwo.includes(e))
    let mandatoryArr = this.surveyAnswers.filter(element => element.answer == "" && element.isRequired && element.questionType != "checkBox");
    // console.error(mandatoryArr)
    let arr = this.surveyAnswers.filter(element => element.answer == "");
    if (arr.length == this.surveyAnswers.length) {
      this.ts.pop("error", "", "Empty form can not be submitted!");
    }
    else if (!checkboxValidation) {
      this.ts.pop("error", "", "Please fill in the mandatory questions");
    }
    else if (mandatoryArr.length > 0) {
      this.ts.pop("error", "", "Please fill in the mandatory questions");
    }
    else {
      this.loading = true
      this.surveyAnswers = this.surveyAnswers.filter(m => m.answer != "")
      if (this.surveyData.isAnonymous) {
        // this.surveyAnswers.forEach((e) => {
        //   delete e.studentId
        // })

        let obj = {
          surveyAnswers: this.surveyAnswers,
          Goals: this.goals,
          surveyId: this.surveyId,
        }
        this._ss.createAnonymousSurveyAnswers(this.orgId, obj).subscribe((data: any) => {

          if (data.IsSuccess) {
            this.loading = false;
            $('#thankyou-modal').modal({ backdrop: 'static', keyboard: false });

            // this.surveyForm.setControl('surveyQuestions', this.fb.array(this.setSurveyQuestion(this.sq)));
          }
          else {
            this.loading = false
            this.ts.pop("error", "", "Something went wrong. Please try again.");
          }
        })
      }
    }
  }

  closeModal() {
    $('#thankyou-modal').modal("hide");
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      $('.fees-container').addClass('hide');
      this.isSuccessfullSubmit = true;
      $('.submit-sucessful').removeClass('hide');

      // this.router.navigate(["/student/survey-list"]);
    }, 1000);

  }

}
