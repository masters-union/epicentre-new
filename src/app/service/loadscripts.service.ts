import { Injectable } from '@angular/core';
declare let $: any;
@Injectable()
export class LoadscriptsService {

    constructor() {
    }
    loadStuff() {

        $(document).ready(function () {
            $('.selectpicker').selectpicker();
            $('.selectpicker').selectpicker('refresh');
        });
    }
    loadNotePad() {
        $(document).ready(function () {
            $(".notepad-btn").click(function () {
                $(".notepad-outer").toggleClass("open-notpad");
            });
        });
    }
    menuSlideUp() {
        $(document).ready(function () {
            $(".submenu").hide();
        });
    }
}
