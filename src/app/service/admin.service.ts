import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { CookieServiceProvider } from './cookie.service';

export interface AdminDetails {
  _id: string,
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  phoneNumber: number,
  exp: number,
  iat: number
}
interface TokenResponse {
  token: string
}
export interface TokenPayload {
  _id: string,
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  phoneNumber: number
}

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private token: string;
  constructor(
    private http: HttpClient,
    private router: Router,
    private cs: CookieServiceProvider
  ) { }

  private saveToken(token: string): void {
    localStorage.setItem('usertoken', token);
    this.token = token;
  }

  private getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('usertoken')
    }
    return this.token
  }

  public getAdminDetails() {
    const token = this.getToken()
    let payload
    if (token) {
      payload = token.split('.')[1]
      payload = window.atob(payload)
      return JSON.parse(payload)
    } else {
      return null
    }
  }

  public isLoggedIn(): boolean {
    const user = this.getAdminDetails()
    if (user) {
      return user.exp > Date.now() / 1000
    } else {
      return false
    }
  }

  adminLogin(obj) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post(environment.baseURL + "admin/signin", obj)
  }
  createOrganizations(obj) {
    let id = this.cs.getItem('id')
    let token = this.cs.getItem('token')

    let headers = new Headers();
    var headers_object = new HttpHeaders().set("Authorization", token);

    return this.http.post(environment.baseURL + "admin/" + id + '/organization', obj)
  }


  public logout(): void {
    this.token = ''
    window.localStorage.removeItem('usertoken');
    this.router.navigateByUrl('/');
  }
}
