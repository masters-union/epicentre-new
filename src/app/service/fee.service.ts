import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';

import { environment } from "../../environments/environment";






@Injectable({
  providedIn: 'root'
})
export class FeeService {

  constructor(
    private http: HttpClient

  ) { }

  getFeeDetails(){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get<any>(environment.baseURL + 'feeDetails/').pipe(
      catchError(this.handleError)
    );
  }
  getSpecificFeeRecord(id){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post<any>(environment.baseURL + 'getSpecificFeeRecord/', id).pipe(
      catchError(this.handleError)
    );
  }

  getFeeDetailById(id: number) {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get<any>(environment.baseURL + 'editStuFee/' + id).pipe(
      catchError(this.handleError)
    );
  }
  getStuDetails() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get<any>(environment.baseURL + '/fetchStudents').pipe(
      catchError(this.handleError)
    );
  }

  // postFeeDetail(obj){
  //   let headers = new Headers();
  //   headers.append("Content-Type", "application/json");
  //   return this.http.post<any>(environment.baseURL + 'addFeeDetail/', obj).pipe(
  //     catchError(this.handleError)
  //   );
  // }

  editStuFeeDetils(obj){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post<any>(environment.baseURL + 'editStuFeeDetils', obj).pipe(
      catchError(this.handleError)
    );
  }

  //hostel
  editStuHostelDetails(obj){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post<any>(environment.baseURL + 'editStuHostalDetils', obj).pipe(
      catchError(this.handleError)
    );
  }

//food
editStuFoodDetails(obj){
  let headers = new Headers();
  headers.append("Content-Type", "application/json");
  return this.http.post<any>(environment.baseURL + 'editStuFoodDetils', obj).pipe(
    catchError(this.handleError)
  );
}

  getScholarship() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL + 'getScholarship/').pipe(
      catchError(this.handleError)
    );
  }
  studentBulkUpload(obj){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post<any>(environment.baseURL + 'studentBulkUpload/', obj).pipe(
      catchError(this.handleError)
    );
  }
  addFeeDetails(object){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.post(environment.baseURL+'addFeeDetail',object).pipe(catchError(this.handleError))
      
  }
  fetchAllStudents(){
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL+'fetchAllStudents').pipe(catchError(this.handleError))

  }

  private handleError(errorResponse: HttpErrorResponse) {
    if(errorResponse.error instanceof ErrorEvent){
      console.error('Client side error: ', errorResponse.error)
    }
    else{
      console.error('Server side error: ', errorResponse)
    }
    return throwError('There is problem with the service')
  }
  fileDownload() {
    let headers = new Headers();
    headers.append("Content-Type", "application/json");
    return this.http.get(environment.baseURL + "/downloadfeecsv", { responseType: 'blob' })
  }


  //download csv

  downloadFile(data, filename='data') {
    let csvData = this.ConvertToCSV(data, ['issue','email']);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
        dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
}

ConvertToCSV(objArray, headerList) {
     let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
     let str = '';
     let row = 'S.No,';

     for (let index in headerList) {
         row += headerList[index] + ',';
     }
     row = row.slice(0, -1);
     str += row + '\r\n';
     for (let i = 0; i < array.length; i++) {
         let line = (i+1)+'';
         for (let index in headerList) {
            let head = headerList[index];

             line += ',' + array[i][head];
         }
         str += line + '\r\n';
     }
     return str;
 }

}
