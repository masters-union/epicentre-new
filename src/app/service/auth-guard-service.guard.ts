import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CommonServiceService } from 'src/app/service/common-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardServiceGuard implements CanActivate {
  constructor(private orgadminSerivice: CommonServiceService, private router: Router) { }
  canActivate(): boolean {
      if (this.orgadminSerivice.checkLoginStatus()) {
        return true
      }else{
        this.router.navigateByUrl('/')
      }
      
      return false
    }
  }