import { Injectable } from '@angular/core';
import { CookieServiceProvider } from './cookie.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  constructor(private http: HttpClient,
    private router: Router,
    private cs: CookieServiceProvider) { }
  checkLoginStatus() {
    if (this.cs.hasCookies()) {
        return true
    } else {
        return false;
    }
}
}
