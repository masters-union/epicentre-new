import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "../common/service/auth.guard";
import { LoadscriptsService } from "../common/service/loadscripts.service";
import { LeadConversationComponent } from "./lead-conversation/lead-conversation.component";
import { LeadEnquiryComponent } from "./lead-enquiry/lead-enquiry.component";
import { LeadPortalComponent } from "./lead-portal/lead-portal.component";
const routes: Routes = [
{path:"",component:LeadPortalComponent},
{path:"leadPortal",component:LeadPortalComponent},
{path:"leadEnquiry",component:LeadEnquiryComponent},
{
  path: "enquiryLeadConversation/:enqId/:orgLeadId",
  component: LeadConversationComponent
},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeadRoutingModule {}
