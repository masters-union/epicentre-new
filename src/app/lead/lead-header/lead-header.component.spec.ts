import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadHeaderComponent } from './lead-header.component';

describe('LeadHeaderComponent', () => {
  let component: LeadHeaderComponent;
  let fixture: ComponentFixture<LeadHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
