import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import * as moment from 'moment';
import { StudentService } from 'src/app/student/service/student.service';
@Component({
  selector: 'app-lead-conversation',
  templateUrl: './lead-conversation.component.html',
  styleUrls: ['./lead-conversation.component.css'],
  providers: [LoadscriptsService],

})
export class LeadConversationComponent implements OnInit {

  public config: ToasterConfig = new ToasterConfig({ limit: 1 });

  enquiryId: String = "";
  orgLeadId: String = "";
  loading: boolean;
  enquiryList = [];
  org_id:any
  reply:any

  constructor(    private studentService: StudentService,
    private fb: FormBuilder,
    private cookie: CookieServiceProvider,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    window.console.log = function () { };
    this.org_id = this.cookie.getItem("orgId");

    this.route.params.subscribe((params: Params) => {
      this.enquiryId = params["enqId"];
      this.orgLeadId = params["orgLeadId"];
    });
    this.getStudentConversationData()
    
    
  }
  
  getStudentConversationData() {
    // console.log("fefeeffe", enquiryId);
    this.loading = true;
    this.studentService
      .getspecificLeadResponse(this.enquiryId, this.orgLeadId)
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          //console.log("check data", res);

          this.enquiryList = res.Data;
          // this.ts.pop("success", "Updated Successfully");
          // this.router.navigate([
          //   "/student/enquiryConversation",
          //   enquiryId,
          //   orgStudentId,
          // ]);
          this.loading = false;
        }
      });
  }
  sendReply(){
    let rep = this.reply.charCodeAt(0)
    if (rep == 32 || this.reply == "") {
      this.ts.pop("error", "", "Please Enter Something");
  }
  else{
    let object={
      organizationId: this.org_id,
      organizationLeadId: this.orgLeadId,
    // userResponseId: this.userId,
    enquiryId:this.enquiryId,
    studentReply: this.reply,
    studentReplyDate:moment()
    }
    //console.log("ob",object)
    this.loading=true 
    this.studentService.userResponseToLead(this.orgLeadId,object).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.loading = false;
        this.reply=""
        this.ts.pop("success", "", "Reply Updated");
        this.getStudentConversationData()
      } else {
        //console.log("error ", res);
      }
    })   
  } 
  }

}
