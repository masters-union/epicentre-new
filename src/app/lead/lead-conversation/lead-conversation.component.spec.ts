import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadConversationComponent } from './lead-conversation.component';

describe('LeadConversationComponent', () => {
  let component: LeadConversationComponent;
  let fixture: ComponentFixture<LeadConversationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadConversationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadConversationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
