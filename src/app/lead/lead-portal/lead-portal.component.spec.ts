import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadPortalComponent } from './lead-portal.component';

describe('LeadPortalComponent', () => {
  let component: LeadPortalComponent;
  let fixture: ComponentFixture<LeadPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
