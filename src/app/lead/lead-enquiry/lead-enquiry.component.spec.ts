import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadEnquiryComponent } from './lead-enquiry.component';

describe('LeadEnquiryComponent', () => {
  let component: LeadEnquiryComponent;
  let fixture: ComponentFixture<LeadEnquiryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadEnquiryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadEnquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
