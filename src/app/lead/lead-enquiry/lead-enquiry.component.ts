import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from "@angular/forms";
import { CookieServiceProvider } from "src/app/common/service/cookie.service";
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { LoadscriptsService } from "src/app/common/service/loadscripts.service";
import { StudentService } from 'src/app/student/service/student.service';
declare var $;

@Component({
  selector: 'app-lead-enquiry',
  templateUrl: './lead-enquiry.component.html',
  styleUrls: ['./lead-enquiry.component.css'],
  providers:[LoadscriptsService]
})
export class LeadEnquiryComponent implements OnInit {
  addForm: FormGroup;
  leadId:any
  org_id:any
  email:any
  loading:boolean
  categoryArray=[]
  subCategoryArray: any = [];
  categoryId:any
  data = [];
  object: {};
  newData: any;
  reply: any;
  student_email: any;
  pageSize: any = 50;
  pageCount: number = 1;
  recordCount: number = 0;
  allCount: number = 0;
  fetchedData = [];
  messageId: any;
  searchParam: String = "";
  isCrossEnable: boolean;
  sortBy = "dsc";
  pageNo: number = 1;
  statuslist: any;
  statusId: any;
  status: any;

  constructor(    private studentService: StudentService,
    private fb: FormBuilder,
    private cookie: CookieServiceProvider,
    private ts: ToasterService,
    private loadscriptsService: LoadscriptsService,
    private router: Router) { }

  ngOnInit() {
    window.console.log = function () { };
    this.leadId = this.cookie.getItem("id");
    this.org_id = this.cookie.getItem("orgId");
    this.email = this.cookie.getItem("email");

    this.initForm();
    this.getLeadDetails()
    this.setCategoryArray()
    this.solvedTicketRecord()
  }
  initForm() {
    this.addForm = this.fb.group({
      message_type: ["Enquiry", Validators.required],
      student_email: ["", [Validators.required, Validators.email]],
      category_name: ["", Validators.required],
      subCategory_name: ["", Validators.required],
      description: ["", Validators.required],
      contact_number: [
        ,
        [
          Validators.required,
          Validators.pattern(/^[6-9]\d{9}$/),
          Validators.maxLength(10),
        ],
      ],
    });
  }
  get fval(): any {
    return this.addForm["controls"];
  }
  setCategoryArray() {
    this.loading = true;
    this.studentService.getCategory(this.org_id).subscribe((res: any) => {
      //console.log(res);
      if (res.IsSuccess) {
        this.loading = false;
        this.categoryArray = res.Data;
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
      }
    });
  }

  categoryChange(e) {
    this.categoryId = e;
    let d = [];
    // this.subCategoryArray.splice(0, this.subCategoryArray.length);
    this.studentService.getSubCategory(e, this.org_id).subscribe((res: any) => {
      if (res.IsSuccess) {
        this.subCategoryArray = res.Data;
        setTimeout(() => {
          this.loadscriptsService.loadStuff();
        }, 100);
      }
    });
  }
 
  solvedTicketRecord() {
    this.loading = true;
    this.studentService
      .fetchAllLeadEnquiry(
        this.org_id,
        this.leadId,
        this.searchParam,
        this.pageNo,
        this.pageSize
      )
      .subscribe((res: any) => {
        if (res.IsSuccess) {
          this.loading = false;
          this.data = res.Data;
          //console.log(this.data);
          let d = res.Data;
          if (this.sortBy == "dsc") {
            d.sort(
              (a, b) =>
                new Date(b.createdAt).getTime() -
                new Date(a.createdAt).getTime()
            );
            this.fetchedData = d;
          } else if (this.sortBy == "asc") {
            d.sort(
              (a, b) =>
                new Date(a.createdAt).getTime() -
                new Date(b.createdAt).getTime()
            );
            this.fetchedData = d;
          }

          // console.log(res)
          setTimeout(() => {
            this.loadscriptsService.loadStuff();
          }, 100);
          this.pageCount = Math.ceil(res.count / this.pageSize);
          //console.log("count", this.pageCount);
          this.recordCount = this.fetchedData.length;
          this.allCount = res.count;
          this.setPage();
        } else {
          this.loading = false;
        }
      });
  }

  viewDescription(index) {
    //console.log(index);
    // $("#exampleModalCenter1").modal('show');
    this.newData = this.data[index].description;
    //console.log(this.newData);
  }
  viewReply(index) {
    this.reply = this.data[index].userResponse;
  }
  sortByDate() {
    this.sortBy = $("#sortBy").val();
    this.solvedTicketRecord();
  }

  nextMessage(pageNo) {
    this.pageNo = pageNo;
    this.search();
    this.setPage();
  }

  // search() {
  // this.pageSize = $('#pageSize').val()
  // this.fetchDetails()
  // }

  search() {
    // this.sortBy = $('#sortBy').val()
    this.pageSize = $("#pageSize").val();
    this.solvedTicketRecord();
  }

  resetsearch() {
    this.pageNo = 1;
    this.searchParam = "";
    this.search();
    this.isCrossEnable = false;
  }

  isSearchType(value) {
    if (value) {
      this.isCrossEnable = true;
    } else {
      this.isCrossEnable = false;
    }
  }

  previous() {
    this.pageNo = this.pageNo - 1;
    this.search();
    this.setPage();
  }
  next() {
    this.pageNo = this.pageNo + 1;
    this.search();
    this.setPage();
  }

  setPage() {
    $(".pitem").removeClass("active");
    $(`.pitem:eq(${this.pageNo - 1})`).addClass("active");
    if (this.pageNo == 1 && this.pageCount == 1) {
      $(".previous").addClass("disabled");
      $(".next").addClass("disabled");
    }
    if (this.pageNo == 1 && this.pageCount > 1) {
      $(".previous").addClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageCount > 1) {
      $(".previous").removeClass("disabled");
      $(".next").removeClass("disabled");
    }
    if (this.pageNo > 1 && this.pageNo == this.pageCount) {
      $(".previous").removeClass("disabled");
      $(".next").addClass("disabled");
    }
  }
  arrayOne(n: number): any[] {
    if (n == undefined) n = 0;
    n = Math.ceil(n);
    if (n != undefined) return Array(n);
  }

  viewStatus(index, id) {
    setTimeout(() => {
      this.loadscriptsService.loadStuff();
    }, 100);
    this.statuslist = this.data[index].status;
    $("#exampleModalCenter3").modal("show");
    this.statusId = id;
    //console.log(this.statusId);
  }
  patchCheckStatus(value) {
    $("#exampleModalCenter3").modal("hide");
    this.loading = true;
    this.status = value;
    //console.log(this.status);
    let object = {
      status: this.status,
      solvedAt: null,
    };
    this.studentService
      .editLeadEnquiry(this.org_id, this.leadId, this.statusId, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Status Updated");
          this.solvedTicketRecord();
          this.statuslist = "";
          this.loading = false;
        } else {
          this.loading = false;
        }
      });
  }

  onClickReply(enquiryId, organizationLeadId) {
    // console.log("fefeeffe", enquiryId);
    this.loading = true;
    // this.studentService
    //   .getspecificStudentResponse(paramId)
    //   .subscribe((res: any) => {
    //     if (res.IsSuccess) {
    //       console.log("check data", res);
    // this.ts.pop("success", "Updated Successfully");
    this.router.navigate([
      "/lead/enquiryLeadConversation",
      enquiryId,
      organizationLeadId,
    ]);
    this.loading = false;
    //   }
    // });
  }
  positiveFeedback(id){
    let object={
      postiveFeedback:true,
      negativeFeedback:false,

    }
    this.loading = true;
    this.studentService
      .leadFeedback(this.leadId, id, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Status Updated");
          this.solvedTicketRecord();
          this.loading = false;
        } else {
          this.loading = false;
        }
      });

  }
  negativeFeedback(id){
    let object={
      negativeFeedback:true,
      postiveFeedback:false

    }
    this.loading = true;
    this.studentService
      .leadFeedback(this.leadId, id, object)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Status Updated");
          this.solvedTicketRecord();
          this.loading = false;
        } else {
          this.loading = false;
        }
      });

    
  }
  getLeadDetails() {
    this.loading = true;
    this.studentService
      .leadEmail(this.leadId)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          // console.log(data.Data)
          this.student_email = data.Data.email;
          this.patchEmail();
          this.loading = false;
        }
      });
  }
  patchEmail() {
    this.addForm.patchValue({
      student_email: this.student_email,
    });
}
onSubmit(val) {
  // console.log(val)
  this.loading = true;
  let addData = {
    enquiryType: val.message_type,
    organizationId: this.org_id,
    category_id: val.category_name,
    sub_category_id: val.subCategory_name,
    organizationLeadId: this.leadId,
    contact_number: val.contact_number,
    email: this.student_email,
    description: val.description,
    isLead:true
  };
  //console.log(addData);
  this.studentService
    .addNewLeadEnquiry(this.org_id, this.leadId, addData)
    .subscribe((res: any) => {
      this.loading = false;
      if (res.IsSuccess) {
        //console.log("Submitted data: ", res);
        this.solvedTicketRecord();
        this.addForm.reset();
        this.initForm();
        this.getLeadDetails();
        this.ts.pop("success", "", "Submitted Successfully");
      } else {
        this.ts.pop("error", "", "Opps some error please try again");
      }
    });
}
}