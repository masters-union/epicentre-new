import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeadRoutingModule } from './lead.routing.module';
import { LeadPortalComponent } from './lead-portal/lead-portal.component';
import { LeadHeaderComponent } from './lead-header/lead-header.component';
import { LeadMenuComponent } from './lead-menu/lead-menu.component';
import { LeadEnquiryComponent } from './lead-enquiry/lead-enquiry.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToasterService, ToasterModule } from "angular2-toaster";
import { LeadConversationComponent } from './lead-conversation/lead-conversation.component';
@NgModule({
  declarations: [LeadPortalComponent, LeadHeaderComponent, LeadMenuComponent, LeadEnquiryComponent, LeadConversationComponent],
  imports: [
    CommonModule,
    LeadRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
    ToasterModule
  ],
  providers:[ToasterService]
})
export class LeadModule { }
