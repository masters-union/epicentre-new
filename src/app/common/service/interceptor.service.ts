import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieServiceProvider } from './cookie.service';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class myInterceptor implements HttpInterceptor {
  constructor(private cs: CookieServiceProvider) {
  }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const req = {
      setHeaders: {
        'Authorization': `${this.cs.getItem("token")}`,
        'benutzerin': `${this.cs.getItem("id")}`,
      }
    };
    let student_id = this.cs.getItem('id')
    if (request.url != `${environment.baseURL}/${environment.orgId}/stu/${student_id}/onlineFeePaymentByStudent` && request.url != `${environment.baseURL}/${environment.orgId}/stu/${student_id}/uploadResume` 
    && request.url != `${environment.baseURL}/${environment.orgId}/stu/${student_id}/club` && request.url != `${environment.baseURL}/${environment.orgId}/uploadFile`
    && request.url != `${environment.baseURL}/${environment.orgId}/stu/${student_id}/addClubReport` 
    && request.url != `${environment.baseURL}/${environment.orgId}/stu/${student_id}/editClubRequest` 
    && request.url != `${environment.baseURL}/${environment.orgId}/stu/${student_id}/editApprovedClubRequest`) {
      // console.log("contant type json", `${environment.baseURL}/${environment.orgId}/stu/${student_id}/onlineFeePaymentByStudent`)
      // console.log("request.url",request.url)
      req.setHeaders['Content-Type'] = 'application/json';
    }
    if (request.url == `${environment.baseURL}/${environment.orgId}/stu/${student_id}/onlineFeePaymentByStudent`) {
      // console.log("cashfee url hai", request.url)
      // req.setHeaders['Content-Type'] = 'application/x-www-form-urlencoded';
      req.setHeaders['Content-Type'] = 'application/json';
      req.setHeaders['Access-Control-Allow-Headers'] = 'application/x-www-form-urlencoded';
      req.setHeaders['Access-Control-Allow-Origin'] = 'https://test.cashfree.com/api/v1/order/create';
      req.setHeaders['Access-Control-Allow-Methods'] = 'OPTIONS,POST,GET';
    }

    if (request.url == `${environment.baseURL}/${environment.orgId}/stu/${student_id}/feePaymentByStatus`) {
      // console.log("cashfee url hai", request.url)
      // req.setHeaders['Content-Type'] = 'application/x-www-form-urlencoded';
      req.setHeaders['Content-Type'] = 'application/json';
      req.setHeaders['Access-Control-Allow-Headers'] = 'application/x-www-form-urlencoded';
      req.setHeaders['Access-Control-Allow-Origin'] = 'https://test.cashfree.com/api/v1/order/create';
      req.setHeaders['Access-Control-Allow-Methods'] = 'OPTIONS,POST,GET';
    }
    // if(request.url == `${environment.baseURL}/${environment.orgId}/stu/${student_id}/club`){
    //   console.error("request.url",request.url)
    //   req.setHeaders['Content-Type'] = 'multipart/form-data';
    // }
    // if (request.url == `${environment.baseURL}/${environment.orgId}/uploadFile`) {
    //   console.error("${environment.baseURL}/${environment.orgId}/uploadFile")
    //   req.setHeaders['Content-Type'] = 'multipart/form-data'
    //   // req.setHeaders['Access-Control-Allow-Headers'] = "*";
    // }
    request = request.clone(req);
    return next.handle(request);
  }
}

