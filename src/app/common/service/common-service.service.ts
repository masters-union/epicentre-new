import { ErrorHandler, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from "../../../environments/environment";
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  constructor(private http: HttpClient,
    private cookieService: CookieServiceProvider) { }
  //survey
  getAnonymousSurveyQuestions(surveyId) {
    return this.http.get(environment.baseURL + "/survey/" + surveyId + "/anonymousSurveyQues");
  }
  createAnonymousSurveyAnswers(orgId, obj) {
    return this.http.post(environment.baseURL + '/' + orgId + "/anonymousSurveyAnswers", obj);
  }
  uploadSurveyFile(orgId, data) {
    let headers = new Headers();
    headers.append("Content-Type", "multipart/form-data");
    headers.append('Accept', 'application/json');
    return this.http.post<any>(environment.baseURL + '/' + orgId + '/uploadFile', data)
  }
  getEventById(eventId) {
    let orgId = environment.orgId
    return this.http.get(environment.baseURL + '/' + orgId + "/event/" + eventId + '/externalEvent')
  }
  eventRegisration(eventId, obj) {
    let orgId = environment.orgId
    return this.http.post(environment.baseURL + "/event/" + eventId + '/externalEventRegisration', obj)
  }
  externalEventFeedback(eventId, obj) {
    let orgId = environment.orgId
    return this.http.post(environment.baseURL + '/' + orgId + "/event/" + eventId + '/feedback', obj)
  }
}
