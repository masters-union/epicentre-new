import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthServiceL } from 'src/app/authenticate/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor (private authservice: AuthServiceL) {}
  
  canActivate(): boolean {
    if (this.authservice.checkLoginStatus()) {
      return true
    } else {
      return false;
    }
  }
  
}
