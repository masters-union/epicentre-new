import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnonymousSurveyComponent } from './anonymous/anonymous-survey/anonymous-survey.component';
import { EventFeedbackComponent } from './event/event-feedback/event-feedback.component';
import { ExternalEventComponent } from './event/external-event/external-event.component';


const routes: Routes = [
  // {
  //   path:'',component:UnderMaintainenceComponent
  // }
  {
     path: '', loadChildren: () => import('./authenticate/authenticate.module').then(m => m.AuthenticateModule) 
  },
  { path: 'student', loadChildren: () => import('./student/student.module').then(m => m.StudentModule) },
  { path: 'auth', loadChildren: () => import('./authenticate/authenticate.module').then(m => m.AuthenticateModule) },
  { path: 'student', loadChildren: () => import('./student/student.module').then(m => m.StudentModule) },
  { path: 'anonymous/:orgId/:surveyId', component: AnonymousSurveyComponent },
  { path: 'external-event/:eventId', component: ExternalEventComponent },
  { path: 'external-eventFeedback/:eventId', component: EventFeedbackComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
