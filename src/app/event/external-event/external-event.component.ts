import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService } from 'angular2-toaster';
import { CommonServiceService } from 'src/app/common/service/common-service.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
@Component({
  selector: 'app-external-event',
  templateUrl: './external-event.component.html',
  styleUrls: ['./external-event.component.css']
})
export class ExternalEventComponent implements OnInit {
  loading: any;
  registerEventForm: FormGroup;
  organizationId: string;
  eventId: any;
  eventData: any;
  nameValidationError: Boolean = false;
  emailValidationError: Boolean = false;
  numberValidationError: Boolean = false;

  constructor(
    private stuService: CommonServiceService, private fb: FormBuilder, private cs: CookieServiceProvider, private ts: ToasterService,
    private router: Router, private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.loading = true;
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['eventId']
    });
    this.organizationId = this.cs.getItem('orgId');
    this.initForm();
    this.getEventDetail();
  }

  initForm() {
    this.registerEventForm = this.fb.group(
      {
        name: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        mobile: ["", [Validators.required, Validators.pattern('[123456789][0-9]{9}')]],
        isExternal: "true",
        eventId: this.eventId,
        slug: ""
      });
  }
  getEventDetail() {
    this.stuService.getEventById(this.eventId)
      .subscribe((data: any) => {
        if (data.IsSuccess) {
          this.loading = false;
          this.eventData = data.Data;
          this.registerEventForm.controls.slug.patchValue(this.eventData.slug);
        } else {
          this.loading = false;
          this.ts.pop("error", "Something Went Wrong");
        }
      });
  }
  registerNow() {
    if(this.registerEventForm.controls.name.invalid){
      this.nameValidationError = true;
    }
    else {
      this.nameValidationError = false;
    }
    if(this.registerEventForm.controls.email.invalid){
      this.emailValidationError = true;
    }
    else {
      this.emailValidationError = false;
    } 
    if(this.registerEventForm.controls.mobile.invalid){
      this.numberValidationError = true;
    }
    else {
      this.numberValidationError = false;
    }
    if (this.registerEventForm.valid) {
      this.loading = true;
      this.stuService.eventRegisration(this.eventId, this.registerEventForm.value)
        .subscribe((data: any) => {
          if (data.IsSuccess) {
            this.loading = false;
            if (data.Message == "User already registered") this.ts.pop("error", data.Message);
            else this.ts.pop("success", data.Message);
            // this.getEventDetail();
          } else {
            this.loading = false;
            this.ts.pop("error", "Something Went Wrong");
          }
        });
    } else {
      this.ts.pop("error", "Please fill in required fields(*) in correct format");
    }
    
  }
}
