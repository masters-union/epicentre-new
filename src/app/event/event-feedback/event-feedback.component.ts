import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { CommonServiceService } from 'src/app/common/service/common-service.service';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
declare var $: any;

@Component({
  selector: 'app-event-feedback',
  templateUrl: './event-feedback.component.html',
  styleUrls: ['./event-feedback.component.css']
})
export class EventFeedbackComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loading: boolean;
  eventData: any;
  eventId: any;
  feedbackForm: FormGroup;
  SuccessFullMeg: boolean = false;

  constructor(
    private stuService: CommonServiceService, private fb: FormBuilder, private cs: CookieServiceProvider, private ts: ToasterService,
    private router: Router, private route: ActivatedRoute,
  ) { }
  ngOnInit() {
    this.loading = true;
    this.route.params.subscribe((params: Params) => {
      this.eventId = params['eventId']
    });
    this.initForm();
    this.eventDetails();
    this.feedBackDetails()
  }

  initForm() {
    this.feedbackForm = this.fb.group({
      feedback1: ["", Validators.required],
      feedback2: ["", Validators.required],
      email: ["", Validators.required],
      isExternal: "true",
      eventId: this.eventId,
    });
  }

  eventDetails() {
    this.stuService.getEventById(this.eventId).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.loading = false;
        this.eventData = data.Data;
      } else {
        this.loading = false;
        this.ts.pop("error", "Something Went Wrong");
      }
    });
  }
  feedBackDetails() {

  }
  onSubmit() {
    if (this.feedbackForm.valid) {
      this.loading = true;
      this.stuService.externalEventFeedback(this.eventId, this.feedbackForm.value).subscribe((data: any) => {
        if (data.IsSuccess) {
          this.SuccessFullMeg = true;
          this.loading = false;
          this.ts.pop("success", "Submitted Successfully");
        } else {
          this.loading = false;
          this.ts.pop("error", "You have already submitted the feedback");
        }
      });
    } else {
      this.ts.pop("error", "Please fill all required field (*)");
    }
  }
  closeModal() {
    $('#thankyou-modal').modal("hide");
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      $('.fees-container').addClass('hide');
      $('.submit-sucessful').removeClass('hide');

      // this.router.navigate(["/student/survey-list"]);
    }, 1000);

  }
}
