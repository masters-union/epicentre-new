import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthServiceL } from "../services/auth.service";
import { CookieServiceProvider } from "../../common/service/cookie.service";
import { ActivatedRoute, Router } from "@angular/router";
// import { AuthService, SocialUser } from "angularx-social-login";
// import { GoogleLoginProvider } from "angularx-social-login";


import { ToasterService, ToasterConfig } from "angular2-toaster";
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
  providers: [AuthServiceL],
})
export class LoginComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loginForm: FormGroup;
  emailRequiredError: boolean = false;
  passwordRequiredError: boolean = false;
  loading: boolean = false;
  redirectURL = null;
  loginURL = null;
  password = null;
  decryptedValue=null
  // private user: SocialUser;
  private loggedIn: boolean;
  constructor(
    private fb: FormBuilder,
    private ts: ToasterService,
    private router: Router,
    private loginService: AuthServiceL,
    private cookieService: CookieServiceProvider,
    // private authService: AuthService,
    private route: ActivatedRoute
  ) {
    this.initLoginForm();
  }

  ngOnInit() {
    // window.console.log = function () {};

    // this.route.params.subscribe((param) => {
    // this.redirectURL =
    //   "http://localhost:4200?redirectUrl=student/survey-detail/1f1fca95-664d-46ea-b882-105665590def";

    //   queryParams: { redirectUrl: 'popular' }
    // });
    this.route.queryParams.subscribe((params) => {
 
      this.redirectURL = params["redirectUrl"];
      this.loginURL = params["loginURL"];
      this.password = params["password"];
//       var encrypted = CryptoJS.AES.encrypt(JSON.stringify('defaultMusb@321'), "Secret Passphrase").toString();
// console.log(encrypted);
// let pass = JSON.stringify(encrypted)
// console.log(encrypted);
// console.log(this.password);

// if(this.password){
  // const decrypted = CryptoJS.AES.decrypt(this.password, "Secret Passphrase");
  // console.log(decrypted.toString(CryptoJS.enc.Utf8), "decrypted");
  // this.decryptedValue=decrypted.toString(CryptoJS.enc.Utf8)
 

// }

    });

    if (
      this.cookieService.getItem("token") != null &&
      this.cookieService.getItem("id") != null
    ) {
      console.log("inside if condition");

      this.router.navigate(["/student/survey-list"]);
      if(this.redirectURL != null){
        this.router.navigate([this.redirectURL]);
      }
    }
    if(this.loginURL != null){
      this.loginForm.get("password").setValue(this.password);
      this.loginForm.get("email").setValue(this.loginURL);
      this.login()
      this.loading=true
    }
     else if (this.redirectURL != null) {
      console.log("inside else if condition");

      // this.signInWithGoogle();
    }

    // this.authService.authState.subscribe((user) => {
    //   console.log("usefe", user);

    //   this.user = user;
    //   this.loggedIn = user != null;
    // });
    
  }



  signOut(): void {
    // this.authService.signOut();
  }

  initLoginForm() {
    this.loginForm = this.fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
    });
    this.loginForm.get("email").valueChanges.subscribe((event) => {
      this.loginForm
        .get("email")
        .setValue(event.toLowerCase(), { emitEvent: false });
    });
  }

  login() {    
    if (this.loginForm.valid) {
      this.loading = true;      
      this.loginService.login(this.loginForm.value).subscribe(
        (res) => {
          this.loading = false;
          // console.log(res)
          if (res.IsSuccess) {
            // console.log(res)
            this.loading = false;
            if (this.cookieService.hasItem("id"))
              this.cookieService.removeItem("id", null, null);
            if (this.cookieService.hasItem("token"))
              this.cookieService.removeItem("token", null, null);
            if (this.cookieService.hasItem("email"))
              this.cookieService.removeItem("email", null, null);
            if (this.cookieService.hasItem("orgId"))
              this.cookieService.removeItem("orgId", null, null);
              if (this.cookieService.hasItem("programBatchId"))
              this.cookieService.removeItem("programBatchId", null, null);

            this.cookieService.setItem(
              "id",
              res.Data.id,
              24 * 3600,
              "/",
              null,
              null
            );
            this.cookieService.setItem(
              "token",
              res.Token,
              24 * 3600,
              "/",
              null,
              null
            );
            this.cookieService.setItem(
              "email",
              res.Data.officialEmail,
              24 * 3600,
              "/",
              null,
              null
            );
            this.cookieService.setItem(
              "orgId",
              res.Data.organizationId,
              24 * 3600,
              "/",
              null,
              null
            );
            this.cookieService.setItem(
              "programBatchId",
              res.Data.programId,
              24 * 3600,
              "/",
              null,
              null
            );
            this.cookieService.setItem(
              "password",
              this.loginForm.get('password').value,
              24 * 3600,
              "/",
              null,
              null
            );
            if (this.redirectURL != null) {
              this.router.navigate([this.redirectURL]);
              this.updateStatus()
            } else {
              this.updateStatus()

              this.router.navigate(["/student/survey-list"]);
            }
            this.ts.pop("success", "", "Logged in");
            // this.initLoginForm();
          } else {
            this.loading = false;
            //console.log("error invalid ");
            this.ts.pop("error", "", "Invalid email/password");
          }
        },
        (error) => {
          this.loading = false;
          //console.log(error);
          this.ts.pop("error", "", "Invalid email/password");
        }
      );
    } else {
      this.emailRequiredError = true;
      this.passwordRequiredError = true;
    }
  }
  updateStatus(){
    this.loginService.loginUpdateStatus(this.loginForm.value).subscribe(data=>{
      if(data.IsSuccess){
        console.log("updated successfully")
      }else{
        this.ts.pop("error", "", "Something Went Wrong");

      }

    })
  }
}
