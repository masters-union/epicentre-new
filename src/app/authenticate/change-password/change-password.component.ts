import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { AuthServiceL } from '../services/auth.service';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup
  submited: boolean;
  passwordError: boolean;
  userParamToken: any;
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  rePasswordError: boolean;
  passwordMatch: boolean;
  tokenMatch: boolean = true
  splitEmailToken: any;
  userParamEmail: any;
  loading: boolean = true;
  userParam: any;
  constructor(private fb: FormBuilder, private _ls: AuthServiceL, private cookie: CookieServiceProvider, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router) { }

  ngOnInit() {
    // window.console.log = function () { };
    this.submited = false;
    this.passwordMatch = true
    this.initForm();
    this.route.params.subscribe(param => {
      this.userParam = param.user;
      this.userParamEmail = param['email'];
      this.userParamToken = param['token'];
    })
    this.IsValidStudent();
  }
  IsValidStudent() {
    this.loading = true
    let data = {
      officialEmail: this.userParamEmail,
      token: this.userParamToken
    }
    this._ls.findToken(data).subscribe((data: any) => {
      if (data.IsSuccess) {
        this.tokenMatch = true;
        this.loading = false;
      }
      else {
        this.tokenMatch = false;
        this.loading = false;
      }
    })
  }

  initForm() {
    this.changePasswordForm = this.fb.group({
      password: ['', Validators.required],
      confirmPassword: [null, Validators.required]
    },
      { validators: this.passwordConfirming('password', 'confirmPassword') })
  }
  //Confirm Password
  passwordConfirming(password: string, confirmPassword: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let pass = group.controls[password];
      let cnfpass = group.controls[confirmPassword];
      if (pass.value !== cnfpass.value) {
        this.passwordMatch = false
        return {
          passwordConfirming: true
        };
      }
      this.passwordMatch = true
      return null;
    }
  }

  submitbtn() {
    this.submited = true;
    this.passwordError = this.changePasswordForm.controls.password.invalid;
    this.rePasswordError = this.changePasswordForm.controls.confirmPassword.invalid;
    let data = {
      officialEmail: this.userParamEmail,
      password: this.changePasswordForm.value.password
    }
    if (!this.passwordError && this.passwordMatch) {
      this.loading = true;
      this._ls.changeForgotPassword(data).subscribe((data: any) => {
        if (data.IsSuccess) {
          this.ts.pop("success", "", "Your Password Successfully changed");
          this.loading = false
          this.router.navigate(['/auth/login']);
        }
        else {
          this.loading = false
          this.ts.pop("error", "", "Something wrong, try again")
        }
      })
    }
  }
}


