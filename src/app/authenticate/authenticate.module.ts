import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticateRoutingModule } from './authenticate-routing.module';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CookieServiceProvider } from '../common/service/cookie.service';
import { AuthServiceL } from './services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { ToasterService, ToasterModule } from "angular2-toaster";
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
// import { SocialLoginModule, AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';
import { LeadLoginComponent } from './lead-login/lead-login.component';


const google_oauth_client_id: string = '762205953238-gjf9ju2l5le6p46cbcr492vt01o272v9.apps.googleusercontent.com';
// let config = new AuthServiceConfig([
//   {
//     id: GoogleLoginProvider.PROVIDER_ID,
//     provider: new GoogleLoginProvider(google_oauth_client_id)
//   }
// ]);

export function provideConfig() {
  // return config;
}

@NgModule({
  declarations: [LoginComponent, HeaderComponent, ForgotPasswordComponent, ChangePasswordComponent, LeadLoginComponent],
  imports: [
    CommonModule,
    AuthenticateRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToasterModule,
    // SocialLoginModule
  ],
  providers: [ToasterService, AuthServiceL, CookieServiceProvider,
    // {
    //   provide: AuthServiceConfig,
    //   useFactory: provideConfig
    // } 
  ]
  // exports: [HeaderComponent]
})
export class AuthenticateModule { }
