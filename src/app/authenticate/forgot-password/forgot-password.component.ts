import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from "angular2-toaster";
import { AuthServiceL } from '../services/auth.service';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup
  emailError: boolean;
  submited: boolean;
  loading:boolean
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  paramUser: any;
  constructor(private fb: FormBuilder, private _ls: AuthServiceL, private cookie: CookieServiceProvider, private route: ActivatedRoute,
    private ts: ToasterService, private router: Router) { }

  ngOnInit() {
    this.submited = false;
    this.initForm(); 
    
  }
  initForm() {
    this.forgotPasswordForm = this.fb.group({
      officialEmail: ['', Validators.required]
    })
  }
  submitbtn() {
    this.submited = true;
    this.emailError = this.forgotPasswordForm.controls.officialEmail.invalid;
    if (!this.emailError) {
      this.loading=true
        this._ls.forgotPassword(this.forgotPasswordForm.value).subscribe((data: any) => {
          if (data.IsSuccess) {
            this.ts.pop("success", "", "We've sent you an email with password reset instructions");
            this.loading=false
            setTimeout(() => {
              this.router.navigate(["/"])
            }, 2000);
          }
          else {
            this.ts.pop("error", "", "Invalid Email")
            this.loading=false
          }
        })
      }
    }
  
}

