import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadLoginComponent } from './lead-login.component';

describe('LeadLoginComponent', () => {
  let component: LeadLoginComponent;
  let fixture: ComponentFixture<LeadLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
