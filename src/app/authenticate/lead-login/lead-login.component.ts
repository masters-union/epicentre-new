import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthServiceL } from "../services/auth.service";
import { CookieServiceProvider } from "../../common/service/cookie.service";
import { Router } from "@angular/router";
// import { AuthService, SocialUser } from "angularx-social-login";

import { ToasterService, ToasterConfig } from "angular2-toaster";

@Component({
  selector: 'app-lead-login',
  templateUrl: './lead-login.component.html',
  styleUrls: ['./lead-login.component.css']
})
export class LeadLoginComponent implements OnInit {
  public config: ToasterConfig = new ToasterConfig({ limit: 1 });
  loginForm: FormGroup;
  emailRequiredError: boolean = false;
  passwordRequiredError: boolean = false;
  loading: boolean = false;
  // private user: SocialUser;
  private loggedIn: boolean;
  id:any
  constructor(private fb: FormBuilder,
    private ts: ToasterService,
    private router: Router,
    private loginService: AuthServiceL,
    private cookieService: CookieServiceProvider,
    ) {    this.initLoginForm();
  }

  ngOnInit() {
    window.console.log = function () { };
    if (this.cookieService.getItem('token') != null && this.cookieService.getItem('id') != null) {
      this.router.navigate(["/lead/leadEnquiry"]);
    }
    // this.authService.authState.subscribe((user) => {
    //   this.user = user;
    //   this.loggedIn = (user != null);
    // });

  }
  initLoginForm() {
    this.loginForm = this.fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required],

    });
    this.loginForm.get('email').valueChanges.subscribe(event => {
      this.loginForm.get('email').setValue(event.toLowerCase(), { emitEvent: false });
    });
  }
  login() {

    if (this.loginForm.valid) {
      this.loading = true
      this.loginService.leadLogin(this.loginForm.value).subscribe(res => {
        this.loading = false;
        // console.log(res)
        if (res.IsSuccess) {
          // console.log(res)
          this.loading = false;
          this.id=res.Data.id
          if (this.cookieService.hasItem('id')) this.cookieService.removeItem('id', null, null);
          if (this.cookieService.hasItem('token')) this.cookieService.removeItem('token', null, null);
          if (this.cookieService.hasItem('email')) this.cookieService.removeItem('email', null, null);
          if (this.cookieService.hasItem('orgId')) this.cookieService.removeItem('orgId', null, null);

          this.cookieService.setItem('id', res.Data.id, 24 * 3600, "/", null, null)
          this.cookieService.setItem('token', res.Token, 24 * 3600, "/", null, null)
          this.cookieService.setItem('email', res.Data.email, 24 * 3600, "/", null, null)
          this.cookieService.setItem('orgId', res.Data.organizationId, 24 * 3600, "/", null, null)

          // this.cookieService.setItem('firstName', res.Data.firstName, 24 * 3600, "/", null, null)

          // this.cookieService.setItem('lastName', res.Data.lastName, 24 * 3600, "/", null, null)
          //console.log("login succesfully ")
          this.router.navigate(["/lead/leadEnquiry"]);
          this.ts.pop("success", "", "Logged in");
          this.updateLeadStatus()
          // this.initLoginForm();
        } else {
          this.loading = false;
          //console.log("error invalid ")
          this.ts.pop("error", "", "Invalid email/password");

        }
      },
        error => {
          this.loading = false;
          //console.log(error);
          this.ts.pop("error", "", "Invalid email/password");
        })
    } else {
      this.emailRequiredError = true;
      this.passwordRequiredError = true;

}
}
updateLeadStatus(){
  let obj={}
  this.loginService.updateleadLogin(this.id,obj).subscribe(res => {
    if(res.IsSuccess){
      //console.log("status Updated")
    }
  })
}
}