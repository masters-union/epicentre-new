import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { map } from 'rxjs/operators';
// import { catchError } from 'rxjs/operators';
import { Router, ActivatedRoute, Params } from "@angular/router";
import { environment } from "./../../../environments/environment";
import { CookieServiceProvider } from 'src/app/common/service/cookie.service';
@Injectable({
  providedIn: 'root'
})

export class AuthServiceL {

  constructor(
    private router: Router,
    private http: HttpClient,
    private cookie: CookieServiceProvider
  ) { }

  login(loginDetails) {
    return this.http.post<any>(environment.baseURL + '/' + environment.orgId + '/stu/auth', loginDetails)
  }
  loginWithGoogle(email) {
    return this.http.post<any>(environment.baseURL + '/' + environment.orgId + '/stu/authWithGoogle', email)
  }
  loginUpdateStatus(loginDetails) {
    return this.http.put<any>(environment.baseURL + '/' + environment.orgId + '/stu/updateStudentLoginStatus', loginDetails)
  }
  forgotPassword(object) {
    return this.http.post<any>(environment.baseURL + "/stu/forgotPassword", object);
  }
  changeForgotPassword(object) {
    return this.http.put<any>(environment.baseURL + '/stu/changeForgotpassword', object);
  }
  findToken(object) {
    return this.http.post<any>(environment.baseURL + '/stu/findToken', object);
  }
  loggedIn(): Boolean {
    if (this.cookie.hasCookies()) {
      return true
    } else {
      this.router.navigate(["/"]);
      return false;
    }

  }
  checkLoginStatus() {
    if (this.cookie.hasCookies()) {
      return true
    } else {
      console.log('no cookies')
      return false;
    }
  }
  leadLogin(loginDetails) {
    return this.http.post<any>(environment.baseURL + '/' + environment.orgId + '/lead/leadAuth', loginDetails)
  }
  updateleadLogin(id,obj) {
    return this.http.put<any>(environment.baseURL + '/' + environment.orgId + '/lead/'+id+'/updateLeadStatus', obj)
  }
}
