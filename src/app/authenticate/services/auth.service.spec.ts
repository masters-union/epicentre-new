import { TestBed } from '@angular/core/testing';

import { AuthServiceL } from './auth.service';

describe('AuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthServiceL = TestBed.get(AuthServiceL);
    expect(service).toBeTruthy();
  });
});
